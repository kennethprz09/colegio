<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Descuentos extends Model
{
    protected $fillable = [
		'servicio', 'porcentaje', 'codigo', 'uso', 'codigo_estudiante', 'status',
	];

	protected $casts = [
		'inicia_at' => 'datetime',
		'termina_at' => 'datetime'
	];
}
