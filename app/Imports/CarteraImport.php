<?php

namespace App\Imports;

use App\User;
use App\HistoricoCartera;
use Illuminate\Validation\Rule;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithValidation;

class CarteraImport implements ToModel, WithValidation {
    use Importable;
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row) {
        if (!isset($row[0])) {
            return null;
        }

        $acudiente = User::where('roll', 'acudiente')->where('codigo', $row[0])->first();
        if ($acudiente) {
            return new HistoricoCartera([
                'servicio' => 'Otro',
                'valor' => $row[2],
                'otro' => 'Si',
                'descripcion' => $row[3],
                'status' => 'activa',
                'acudiente_id' => $acudiente->id,
            ]);
        } else {
            return new HistoricoCartera([
                'servicio' => 'Otro',
                'valor' => $row[2],
                'otro' => 'Si',
                'descripcion' => $row[3],
                'status' => 'activa',
                'acudiente_id' => 'Z-'. $row[0],
            ]);
            return null;
        }

    }

    public function rules(): array {
        return [
            '0' => 'required',
            '2' => 'required',
            '3' => 'required',
        ];
    }
}

