<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Extracurriculares extends Model
{
    protected $fillable = [
		'actividad', 'valor', 'docente',  'docente_id', 'url_informacion', 'url_horarios', 'imagen',
	];

	protected $casts = [
		'inicia_at' => 'datetime',
		'termina_at' => 'datetime'
	];
}
