<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InformePago extends Model
{
    protected $fillable = [
		'transaccionId',
		'uuid',
		'aprobado',
		'transaccionConvenioId',
		'ref1',
		'valor',
		'valor_subtotal',
		'descuento',
		'cupon',
		'_uid',
		'_tramite',
		'acudiente_id',
		'tipo',
		'meses',
		'servicio_id',
		'tipo_pago',
		'certificado_estado',
		'certificado_motivo',
		'certificado_archivo',
		'matricula_estado',
		'matricula_pagare',
		'matricula_contrato',
		'matricula_acta',
		'matricula_paz',
		'factura_pdf',
	];
}