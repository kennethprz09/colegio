<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HistoricoCartera extends Model
{
    protected $fillable = [
		'servicio', 'valor', 'mora', 'otro', 'descripcion', 'acudiente_id', 'matricula_id', 'status', 'acuerdo_id',
	];
}
