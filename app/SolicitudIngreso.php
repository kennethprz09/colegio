<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SolicitudIngreso extends Model
{
    protected $fillable = [
		// Paso 1
			'step1_nombre_completo', 'step1_primer_apellido', 'step1_segundo_apellido', 'step1_genero', 'step1_ide', 'step1_identificacion', 'step1_direccion_1', 'step1_direccion_1_1',
			'step1_direccion_2', 'step1_direccion_3', 'step1_barrio', 'step1_nacimiento_at', 'step1_lugar_nacimiento', 'step1_telefono', 'step1_email',
			'step1_institucion_previa', 'step1_grado', 'step1_motivo_cambio',
		// Paso 2
			'step2_estado_padre', 'step2_nombre_padre', 'step2_ide_padre', 'step2_identificacion_padre', 'step2_expedicion_padre', 'step2_email_padre',
			'step2_profesion_padre', 'step2_empresa_padre', 'step2_cargo_padre', 'step2_ingresos_padre', 'step2_telefono_padre', 'step2_direccion_padre',
			'step2_estado_madre','step2_nombre_madre', 'step2_ide_madre', 'step2_identificacion_madre', 'step2_expedicion_madre', 'step2_email_madre',
			'step2_profesion_madre', 'step2_empresa_madre', 'step2_cargo_madre', 'step2_ingresos_madre', 'step2_telefono_madre', 'step2_direccion_madre',
		// Paso 3
			'step3_hermanos', 'step3_cantidad', 'step3_lugar_ocupado', 'step3_responsable', 'step3_medio_conocido', 'step3_estado_otro', 'step3_parentesco_otro',
			'step3_nombre_otro', 'step3_ide_otro', 'step3_identificacion_otro', 'step3_expedicion_otro', 'step3_email_otro', 'step3_profesion_otro',
			'step3_empresa_otro', 'step3_cargo_otro', 'step3_ingresos_otro', 'step3_telefono_otro', 'step3_direccion_1_otro', 'step3_direccion_1_1_otro',
			'step3_direccion_2_otro', 'step3_direccion_3_otro', 'step3_factura', 'step3_medio_conocido_mas',
			// Factura electronica
			'step3_tipo_persona_factura', 'step3_nombre_factura', 'step3_tipo_id_factura', 'step3_tipo_id_cual_factura', 'step3_identificacion_factura',
			'step3_email_factura', 'step3_tel_fijo_factura', 'step3_tel_celular_factura', 'step3_pais_factura', 'step3_ciudad_factura', 'step3_departamento_factura', 'step3_direccion_factura',
			
			// Generales
			'codigo_estudiante',
			'status',
			'nivelacion',
			'status_nivelacion',
			'compromiso',
	];
}
