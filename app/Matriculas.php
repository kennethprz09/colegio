<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Matriculas extends Model
{
	protected $fillable = [
		'desde', 'hasta',
		'inicia_at_or', 'termina_at_or', 'valor_or',
		'inicia_at_ex', 'termina_at_ex', 'valor_ex',
	];

	protected $casts = [
		'inicia_at_or' => 'datetime',
		'inicia_at_ex' => 'datetime',
		'termina_at_or' => 'datetime',
		'termina_at_ex' => 'datetime',
	];
}