<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AcuerdoCuotas extends Model
{
    protected $fillable = [
        'mes', 'valor', 'estatus', 'acuerdo_id',
  ];
}
