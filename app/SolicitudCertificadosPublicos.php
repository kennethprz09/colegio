<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SolicitudCertificadosPublicos extends Model {
  protected $fillable = [
    'nombres', 'apellidos', 'tipo_doc', 'numero', 'direccion', 'ciudad', 'telefono', 'correo', 'responsabilidad_fiscal', 'actividad_economica', 'otro_cual',
    'nombres_estudiante', 'apellidos_estudiante', 'tipo_doc_estudiente', 'numero_estudiente', 'telefono_estudiante', 'correo_estudiante', 'tipo_documento', 'cantidad', 'año_promocion',
    'motivo', 'datos_certificacion', 'canal_envio', 'certificado_publico', 'valor_pagar', 'codigounico', 'status',
	];
}
