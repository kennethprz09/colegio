<?php

namespace App\Externos;

use Illuminate\Database\Eloquent\Model;

class Padre extends Model
{
	protected $connection = 'mysql2';
	protected $table = "padres";
    protected $primaryKey = 'id';
    public $timestamps = false;
}