<?php

namespace App\Externos;

use Illuminate\Database\Eloquent\Model;

class Acudiente extends Model
{
	protected $connection = 'mysql2';
	protected $table = "acudientes";
    protected $primaryKey = 'id';
    public $timestamps = false;
}