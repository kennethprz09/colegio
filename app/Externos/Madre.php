<?php

namespace App\Externos;

use Illuminate\Database\Eloquent\Model;

class Madre extends Model
{
	protected $connection = 'mysql2';
	protected $table = "madres";
    protected $primaryKey = 'id';
    public $timestamps = false;
}
