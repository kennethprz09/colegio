<?php

namespace App\Externos;

use Illuminate\Database\Eloquent\Model;

class Estudiante extends Model {
	protected $connection = 'mysql2';
	protected $table = "estudiantes";
    protected $primaryKey = 'id';
    public $timestamps = false;
}