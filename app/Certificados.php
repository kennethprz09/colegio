<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Certificados extends Model
{
	protected $fillable = [
		'concepto', 'entrega', 'canal', 'valor', 'status',
	];
}