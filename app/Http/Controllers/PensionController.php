<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\User;
use App\Pension;
use App\InformePago;
use App\HistoricoCartera;
use Carbon\Carbon;
use Auth, NumberFormatter;

class PensionController extends Controller {
	protected function ListDetalles($items) {
		foreach ($items as $item) {
			$item->valor = preg_replace('/[.]/', '', $item->valor);
		}
	}

	public function List(Request $request) {
		$columns = [ 'id' ];

		$length = $request->input('length');
		$column = $request->input('column'); //Index
		$dir = $request->input('dir');
		$searchValue = $request->input('search');

		$query = Pension::orderBy($columns[$column], $dir);

		if($searchValue) {
			$query->where(function($query) use ($searchValue) {
				$query->where('desde', 'LIKE', '%' . $searchValue . '%')
				->orWhere('hasta', 'LIKE', '%' . $searchValue . '%')
				->orWhere('valor', 'LIKE', '%' . $searchValue . '%')
				->orWhere('porcentaje', 'LIKE', '%' . $searchValue . '%');
			});
		}

		$projects = $query->paginate($length);
		return ['data' => $projects, 'draw' => $request->input('draw')];
	}

	public function Save(Request $request) {
		Validator::make($request->all(), [
			'desde' => [
				'required',
				'unique:pensions'
			],
			'valor' => 'required',
			'porcentaje' => 'required'
		])->validate();

		Pension::create($request->all());
	}

	public function Update(Request $request) {
		Validator::make($request->all(), [
			'id' => 'required',
			'desde' => [
				'required',
				Rule::unique('pensions')->ignore($request->id, 'id')
			],
			'valor' => 'required',
			'porcentaje' => 'required'
		])->validate();

		Pension::find($request->id)->update($request->all());
	}

	public function ConfiguracionPension(Request $request) {
		$user = User::find(Auth::User()->id);
		$user->pension = $request->pension;
		$user->save();
	}

	public function Delete($id) {
		Pension::find($id)->delete();
	}

	public function ListPagos(Request $request) {
		$columns = [ 'id', 'desde', 'hasta', 'valor' ];

		$length = $request->input('length');
		$column = $request->input('column'); //Index
		$dir = $request->input('dir');
		$searchValue = $request->input('search');

		$pagados = InformePago::where('_tramite', 'Pension')->where('acudiente_id', Auth::user()->id)->where('aprobado', 'A')->select('_uid')->get();
		$query = Pension::whereIn('id', $pagados)->orderBy($columns[$column], $dir);

		if($searchValue) {
			$query->where(function($query) use ($searchValue) {
				$query->where('desde', 'LIKE', '%' . $searchValue . '%')
				->orWhere('hasta', 'LIKE', '%' . $searchValue . '%')
				->orWhere('valor', 'LIKE', '%' . $searchValue . '%');
			});
		}

		$projects = $query->paginate($length);
		$this->ListDetalles($projects);
		return ['data' => $projects, 'draw' => $request->input('draw')];
	}

	public function Tarifa(Request $request) {
		$acudiente_grado = Auth::User()->grado;
		$matricula_activa = InformePago::orderBy('id', 'desc')->where('aprobado', 'A')->where('acudiente_id', Auth::User()->id)
		->where('_tramite', 'Matricula')->where('matricula_estado', 'Activo')->first();
		// Cargo pensiones en forma de cartera inactiva
		$semejantes = null;
		
		if ($acudiente_grado == 'PJ' || $acudiente_grado == 'JD' || $acudiente_grado == 'TR') {
			if ($acudiente_grado == 'PJ') {
				$grado = 'Pre. Jardin';
			}
			if ($acudiente_grado == 'JD') {
				$grado = 'Jardin';
			}
			if ($acudiente_grado == 'TR') {
				$grado = 'Transicion';
			}
			$pension_asignada = Pension::where('desde', $grado)->first();
			$pension_asignada->valor = preg_replace('/[.]/', '', $pension_asignada->valor);
			$semejantes = $pension_asignada;
		} else {
			$pension_asignada = Pension::where('desde', $acudiente_grado)->first();
			$pension_asignada->valor = preg_replace('/[.]/', '', $pension_asignada->valor);
			$semejantes = $pension_asignada;
		}

		$deudas = HistoricoCartera::where('acudiente_id', Auth::User()->id)->where('servicio', 'Pension')->where('matricula_id', $matricula_activa->id)->get();

		return [
			'deudas' => $deudas,
			'matricula_activa' => $matricula_activa,
			'semejantes' => $semejantes,
		];
	}

	public function Pagado(Request $request) {
		$informe = InformePago::create($request->all());
		if($informe->aprobado == 'A') {
			$pension = HistoricoCartera::find($request->_uid);
			$pension->status = "Pagada";
			$pension->save();

			$activarpension = HistoricoCartera::where('servicio', 'Pension')->where('acudiente_id', $informe->acudiente_id)
			->where('status', 'inactiva')->first();
			$pensionesActivas = HistoricoCartera::where('servicio', 'Pension')->where('acudiente_id', $informe->acudiente_id)
			->where('status', 'activa')->count();
			if ($activarpension) {
				if ($pensionesActivas == 0) {
					$activarpension->status = 'preactiva';
					$activarpension->save();
				}
			}
		}
		return $request;
	}

	public function PagadoMultiple(Request $request) {
		$informe = InformePago::create($request->all());
		if($informe->aprobado == 'A') {
			$acudiente = User::find($informe->acudiente_id);
			$pensiones = HistoricoCartera::where('servicio', 'Pension')->where('acudiente_id', $acudiente->id)
			->where(function($query) {
				$query->where('status', '=', "inactiva")
					->orWhere('status', '=', "preactiva")
					->orWhere('status', '=', "activa");
			})->limit($request->meses)->get();

			foreach ($pensiones as $pension) {
				$pension->status = "Pagada";
				$pension->save();
				setlocale(LC_ALL, 'es_ES');
				$fecha = Carbon::parse($pension->created_at);
				$fecha->format("F"); // Inglés.
				$pension->created_f = $fecha->formatLocalized('%B');// mes en idioma español
			}
			$informe->tipo = 'Web';
			$informe->factura_pdf = '/facturas/Factura-'.$informe->id.'-'.$acudiente->codigo.'.pdf';
			$informe->save();
			
			$informe->name_estudiante = $acudiente->name_estudiante;
			$informe->grado = $acudiente->grado;
			$informe->codigo = $acudiente->codigo;
			$informe->email = $acudiente->email;
			$formatterES = new NumberFormatter("es", NumberFormatter::SPELLOUT);
			$pdf = \PDF::loadView('pdf.facturaPensionMultiple', [ 
				'data' => $informe,
				'pagos' => $pensiones,
				'precio' => $formatterES->format($informe->valor)
			])->save(public_path().'/facturas/Factura-'.$informe->id.'-'.$acudiente->codigo.'.pdf');

			$activarpension = HistoricoCartera::where('servicio', 'Pension')->where('acudiente_id', $acudiente->id)
			->where('status', 'inactiva')->first();
			$pensionesActivas = HistoricoCartera::where('servicio', 'Pension')->where('acudiente_id', $acudiente->id)
			->where('status', 'activa')->count();
			if ($activarpension) {
				if ($pensionesActivas == 0) {
					$activarpension->status = 'preactiva';
					$activarpension->save();
				}
			}
		}
		return $request;
	}
}
