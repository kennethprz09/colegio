<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Docente;

class DocenteController extends Controller {
	public function __construct() {
		$this->middleware('auth');
	}

	public function List(Request $request) {
		$columns = [ 'id' ];

		$length = $request->input('length');
		$column = $request->input('column'); //Index
		$dir = $request->input('dir');
		$searchValue = $request->input('search');

		$query = Docente::orderBy($columns[$column], $dir);

		if($searchValue) {
			$query->where(function($query) use ($searchValue) {
				$query->where('nombre', 'LIKE', '%' . $searchValue . '%')
				->orWhere('asignatura', 'LIKE', '%' . $searchValue . '%')
				->orWhere('email', 'LIKE', '%' . $searchValue . '%')
				->orWhere('whatsapp', 'LIKE', '%' . $searchValue . '%');
			});
		}

		$projects = $query->paginate($length);
		return ['data' => $projects, 'draw' => $request->input('draw')];
	}

	public function Save(Request $request) {
		$this->validate($request, [
			'nombre' => 'required',
			'asignatura' => 'required',
			'email' => 'required',
			'whatsapp' => 'required'
		]);

		Docente::create($request->all());
	}

	public function Update(Request $request) {
		$this->validate($request, [
			'id' => 'required'
		]);

		Docente::find($request->id)->update($request->all());
	}

	public function Delete($id) {
		Docente::find($id)->delete();
	}
}
