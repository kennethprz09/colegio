<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Str;
use App\Matriculas;
use App\Pension;
use App\InformePago;
use App\HistoricoCartera;
use App\User;
use File, Auth;
use Carbon\Carbon;

class MatriculasController extends Controller {
	protected function ListDetalles($items) {
		foreach ($items as $item) {
			$item->inicia_at_or_0 = $item->inicia_at_or->format('Y-m-d');
			$item->termina_at_or_0 = $item->termina_at_or->format('Y-m-d');

			$item->inicia_at_ex_0 = $item->inicia_at_ex->format('Y-m-d');
			$item->termina_at_ex_0 = $item->termina_at_ex->format('Y-m-d');
		}
	}

	public function List(Request $request) {
		$columns = ['id'];

		$length = $request->input('length');
		$column = $request->input('column'); //Index
		$dir = $request->input('dir');
		$searchValue = $request->input('search');

		$query = Matriculas::orderBy($columns[$column], $dir);

		if($searchValue) {
			$query->where(function($query) use ($searchValue) {
				$query->where('desde', 'LIKE', '%' . $searchValue . '%')
				->orWhere('valor_or', 'LIKE', '%' . $searchValue . '%')
				->orWhere('valor_ex', 'LIKE', '%' . $searchValue . '%')
				->orWhere('hasta', 'LIKE', '%' . $searchValue . '%');
			});
		}

		$projects = $query->paginate($length);
		$this->ListDetalles($projects);
		return ['data' => $projects, 'draw' => $request->input('draw')];
	}

	public function Save(Request $request) {
		$messages = [
			'after' => 'La fecha de terminación debe ser mayor a la de iniciación.',
		];
		Validator::make($request->all(), [
			'desde' => [
				'required',
				'unique:matriculas'
			],
			'inicia_at_or' => 'required',
			'termina_at_or' => 'required|after:inicia_at_or',
			'valor_or' => 'required',
			'inicia_at_ex' => 'required',
			'termina_at_ex' => 'required|after:inicia_at_ex',
			'valor_ex' => 'required'
		], $messages)->validate();

		$objeto = new Matriculas;
		$objeto->desde = $request->desde;
		$objeto->hasta = 'N/A';

		$objeto->inicia_at_or = new \DateTime($request->inicia_at_or);
		$objeto->termina_at_or = new \DateTime($request->termina_at_or);
		$objeto->valor_or = $request->valor_or;

		$objeto->inicia_at_ex = new \DateTime($request->inicia_at_ex);
		$objeto->termina_at_ex = new \DateTime($request->termina_at_ex);
		$objeto->valor_ex = $request->valor_ex;
		$objeto->save();

		// Matriculas::create($request->all());
	}

	public function Update(Request $request) {
		$messages = [
			'after' => 'La fecha de terminación debe ser mayor a la de iniciación.',
		];
		Validator::make($request->all(), [
			'id' => 'required',
			'desde' => [
				'required',
				Rule::unique('matriculas')->ignore($request->id, 'id'),
			],
			'inicia_at_or' => 'required',
			'termina_at_or' => 'required|after:inicia_at_or',
			'valor_or' => 'required',
			'inicia_at_ex' => 'required',
			'termina_at_ex' => 'required|after:inicia_at_ex',
			'valor_ex' => 'required'
		], $messages)->validate();
		
		$objeto = Matriculas::find($request->id);
		$objeto->desde = $request->desde;
		$objeto->hasta = 'N/A';

		$objeto->inicia_at_or = new \DateTime($request->inicia_at_or);
		$objeto->termina_at_or = new \DateTime($request->termina_at_or);
		$objeto->valor_or = $request->valor_or;

		$objeto->inicia_at_ex = new \DateTime($request->inicia_at_ex);
		$objeto->termina_at_ex = new \DateTime($request->termina_at_ex);
		$objeto->valor_ex = $request->valor_ex;
		$objeto->save();
	}

	public function Delete($id) {
		Matriculas::find($id)->delete();
	}

	public function Tarifa(Request $request) {
		$acudiente_grado = Auth::User()->grado;
		if ($acudiente_grado == 'PJ' || $acudiente_grado == 'JD' || $acudiente_grado == 'TR') {
			if ($acudiente_grado == 'PJ') {
				$grado = 'Pre. Jardin';
			}
			if ($acudiente_grado == 'JD') {
				$grado = 'Jardin';
			}
			if ($acudiente_grado == 'TR') {
				$grado = 'Transicion';
			}
			$matricula = Matriculas::where('desde', $grado)->first();
			$matricula->valor_ex = preg_replace('/[.]/', '', $matricula->valor_ex);
			$matricula->valor_or = preg_replace('/[.]/', '', $matricula->valor_or);

			$pension = Pension::where('desde', $grado)->first();
			$pension->valor = preg_replace('/[.]/', '', $pension->valor);
			$pension_asignada = $pension;
		} else {
			$matricula = Matriculas::where('desde', $acudiente_grado)->first();
			$matricula->valor_ex = preg_replace('/[.]/', '', $matricula->valor_ex);
			$matricula->valor_or = preg_replace('/[.]/', '', $matricula->valor_or);

			$pension = Pension::where('desde', $acudiente_grado)->first();
			$pension->valor = preg_replace('/[.]/', '', $pension->valor);
			$pension_asignada = $pension;
		}
		$ordinaria = $matricula;
		$extraordinaria = $matricula;

		$historial = InformePago::orderBy('id', 'desc')->where('aprobado', 'A')
		->where('acudiente_id', Auth::User()->id)->where('_tramite', 'Matricula')->count();

		$matricula = InformePago::orderBy('id', 'desc')->where('aprobado', 'A')
		->where('acudiente_id', Auth::User()->id)->where('_tramite', 'Matricula')->where('matricula_estado', 'Activo')->first();
		
		$deudas = HistoricoCartera::where('acudiente_id', Auth::User()->id)->where('status', 'activa')->get();

		return [
			'ordinaria' => $ordinaria,
			'extraordinaria' => $extraordinaria,
			'historial' => $historial,
			'deudas' => $deudas,
			'matricula' => $matricula,
			'pension' => $pension_asignada,
			'pensionmeses' => Auth::User()->pension,
			'acudiente_grado' => $acudiente_grado,
		];
	}
	
	public function ListMatriculados(Request $request) {
		$columns = ['id'];

		$length = $request->input('length');
		$column = $request->input('column'); //Index
		$dir = $request->input('dir');
		$searchValue = $request->input('search');

		$query = InformePago::orderBy('id', 'desc')->where('aprobado', 'A')->where('_tramite', 'Matricula')
		->join('users', 'informe_pagos.acudiente_id', '=', 'users.id')
		->select('informe_pagos.*', 'users.codigo as codigo_estudiante', 'users.name_estudiante as name_estudiante', 'users.email as email_estudiante');

		if($searchValue) {
			$query->where(function($query) use ($searchValue) {
				
				$query->where('users.codigo', 'LIKE', '%' . $searchValue . '%')
				->orWhere('users.name_estudiante', 'LIKE', '%' . $searchValue . '%')
				->orWhere('users.email', 'LIKE', '%' . $searchValue . '%')
				->orWhere('informe_pagos.valor', 'LIKE', '%' . $searchValue . '%')
				->orWhere('informe_pagos.created_at', 'LIKE', '%' . $searchValue . '%');
			});
		}

		$projects = $query->paginate($length);
		return ['data' => $projects, 'draw' => $request->input('draw')];
	}

	public function ShowMatriculado($id) {
		$matricula = InformePago::find($id);
		if ($matricula) {
			$acudiente = User::find($matricula->acudiente_id);
			$matricula->user = $acudiente; 
		}

		return $matricula;
	}

	public function MatriculadoPdf(Request $request) {
		$matricula = InformePago::find($request->id);
		if ($matricula) {
			if ($request->status == 'matricula_pagare') {
				$file = $request->file('pdf'); // Manipulando imagen
				$file_veri = file_exists($file);
				if ($file_veri == 'true') {
					$destinationPath = public_path().'/matriculas-pdf';
					$filenameO = $file->getClientOriginalName();
					$filename = Str::random(12);
					$upload_success = $file->move($destinationPath, $filename . '-' . $filenameO);
					$matricula->matricula_pagare = $filename . '-' . $filenameO; // Guardando Imagen
				}
			}
			
			if ($request->status == 'matricula_contrato') {
				$file = $request->file('pdf'); // Manipulando imagen
				$file_veri = file_exists($file);
				if ($file_veri == 'true') {
					$destinationPath = public_path().'/matriculas-pdf';
					$filenameO = $file->getClientOriginalName();
					$filename = Str::random(12);
					$upload_success = $file->move($destinationPath, $filename . '-' . $filenameO);
					$matricula->matricula_contrato = $filename . '-' . $filenameO; // Guardando Imagen
				}
			}

			if ($request->status == 'matricula_acta') {
				$file = $request->file('pdf'); // Manipulando imagen
				$file_veri = file_exists($file);
				if ($file_veri == 'true') {
					$destinationPath = public_path().'/matriculas-pdf';
					$filenameO = $file->getClientOriginalName();
					$filename = Str::random(12);
					$upload_success = $file->move($destinationPath, $filename . '-' . $filenameO);
					$matricula->matricula_acta = $filename . '-' . $filenameO; // Guardando Imagen
				}
			}

			if ($request->status == 'matricula_paz') {
				$file = $request->file('pdf'); // Manipulando imagen
				$file_veri = file_exists($file);
				if ($file_veri == 'true') {
					$destinationPath = public_path().'/matriculas-pdf';
					$filenameO = $file->getClientOriginalName();
					$filename = Str::random(12);
					$upload_success = $file->move($destinationPath, $filename . '-' . $filenameO);
					$matricula->matricula_paz = $filename . '-' . $filenameO; // Guardando Imagen
				}
			}
			$matricula->save();
		}
	}

	public function Pagado(Request $request) {
		$informe = InformePago::create($request->all());
		if($informe->aprobado == 'A') {
			$informe->matricula_estado = 'Activo';
			$informe->save();

			// Cargo pensiones en forma de cartera inactiva
			$user = User::find($request->acudiente_id);
			$acudiente_grado = $user->grado;
			$semejantes = null;
			
			if ($acudiente_grado == 'PJ' || $acudiente_grado == 'JD' || $acudiente_grado == 'TR') {
				if ($acudiente_grado == 'PJ') {
					$grado = 'Pre. Jardin';
				}
				if ($acudiente_grado == 'JD') {
					$grado = 'Jardin';
				}
				if ($acudiente_grado == 'TR') {
					$grado = 'Transicion';
				}
				$pension_asignada = Pension::where('desde', $grado)->first();
				$pension_asignada->valor = preg_replace('/[.]/', '', $pension_asignada->valor);
				$semejantes = $pension_asignada;
			} else {
				$pension_asignada = Pension::where('desde', $acudiente_grado)->first();
				$pension_asignada->valor = preg_replace('/[.]/', '', $pension_asignada->valor);
				$semejantes = $pension_asignada;
			}

			$fecha = Carbon::now();
			$mes_actual = $fecha->format('m');
			$periodo1 = false;
			$periodo2 = false;
			if (($mes_actual == 5) || ($mes_actual == 6) || ($mes_actual == 7) || ($mes_actual == 8) || 
			($mes_actual == 9) || ($mes_actual == 10) || ($mes_actual == 11) || ($mes_actual == 12)) {
				$fecha_proxima = Carbon::now()->addYear();
				$periodo1 = true;
				$periodo2 = true;
			}
			if (($mes_actual == 1) || ($mes_actual == 2) || ($mes_actual == 3) || ($mes_actual == 4)) {
				$fecha_proxima = Carbon::now();
				$periodo2 = true;
			}
			$cuotas = $user->pension;
			$total = preg_replace('/[.]/', '', $semejantes->valor) * 10;
			$dosporciento = 0;
			if ($cuotas == 12) {
				$dosporciento = (2 / 100) * $total;
			}
			
			$valorbase = ($total + $dosporciento) / $cuotas;
			$valor = intval($valorbase);
			for ($i=1; $i <= 12; $i++) { 
				if ($periodo1) {
					if (($i == 1) && ($mes_actual < 9)) {
						$fecha_new = '01-09-' . $fecha->format('Y') . ' 05:00:00';
						$pensiones = new \App\HistoricoCartera;
						$pensiones->servicio = 'Pension';
						$pensiones->valor = $valor;
						$pensiones->otro = 'No';
						$pensiones->acudiente_id = $user->id;
						$pensiones->matricula_id = $informe->id;
						$pensiones->status = 'inactiva';
						$pensiones->created_at = new \DateTime($fecha_new);
						$pensiones->save();
					}
					if (($i == 2) && ($mes_actual < 10)) {
						$fecha_new = '01-10-' . $fecha->format('Y') . ' 05:00:00';
						$pensiones = new \App\HistoricoCartera;
						$pensiones->servicio = 'Pension';
						$pensiones->valor = $valor;
						$pensiones->otro = 'No';
						$pensiones->acudiente_id = $user->id;
						$pensiones->matricula_id = $informe->id;
						$pensiones->status = 'inactiva';
						$pensiones->created_at = new \DateTime($fecha_new);
						$pensiones->save();
					}
					if (($i == 3) && ($mes_actual < 11)) {
						$fecha_new = '01-11-' . $fecha->format('Y') . ' 05:00:00';
						$pensiones = new \App\HistoricoCartera;
						$pensiones->servicio = 'Pension';
						$pensiones->valor = $valor;
						$pensiones->otro = 'No';
						$pensiones->acudiente_id = $user->id;
						$pensiones->matricula_id = $informe->id;
						$pensiones->status = 'inactiva';
						$pensiones->created_at = new \DateTime($fecha_new);
						$pensiones->save();
					}
					if (($i == 4) && ($mes_actual < 12)) {
						$fecha_new = '01-12-' . $fecha->format('Y') . ' 05:00:00';
						$pensiones = new \App\HistoricoCartera;
						$pensiones->servicio = 'Pension';
						$pensiones->valor = $valor;
						$pensiones->otro = 'No';
						$pensiones->acudiente_id = $user->id;
						$pensiones->matricula_id = $informe->id;
						$pensiones->status = 'inactiva';
						$pensiones->created_at = new \DateTime($fecha_new);
						$pensiones->save();
					}
				}
				if ($periodo2) {
					if (($mes_actual == 5) || ($mes_actual == 6) || ($mes_actual == 7) || ($mes_actual == 8) || ($mes_actual == 9) || ($mes_actual == 10) || ($mes_actual == 11) || ($mes_actual == 12)) {
						if (($i == 5)) {
							$fecha_new = '01-01-' . $fecha_proxima->format('Y') . ' 05:00:00';
							$pensiones = new \App\HistoricoCartera;
							$pensiones->servicio = 'Pension';
							$pensiones->valor = $valor;
							$pensiones->otro = 'No';
							$pensiones->acudiente_id = $user->id;
							$pensiones->matricula_id = $informe->id;
							$pensiones->status = 'inactiva';
							$pensiones->created_at = new \DateTime($fecha_new);
							$pensiones->save();
						}
						if (($i == 6)) {
							$fecha_new = '01-02-' . $fecha_proxima->format('Y') . ' 05:00:00';
							$pensiones = new \App\HistoricoCartera;
							$pensiones->servicio = 'Pension';
							$pensiones->valor = $valor;
							$pensiones->otro = 'No';
							$pensiones->acudiente_id = $user->id;
							$pensiones->matricula_id = $informe->id;
							$pensiones->status = 'inactiva';
							$pensiones->created_at = new \DateTime($fecha_new);
							$pensiones->save();
						}
						if (($i == 7)) {
							$fecha_new = '01-03-' . $fecha_proxima->format('Y') . ' 05:00:00';
							$pensiones = new \App\HistoricoCartera;
							$pensiones->servicio = 'Pension';
							$pensiones->valor = $valor;
							$pensiones->otro = 'No';
							$pensiones->acudiente_id = $user->id;
							$pensiones->matricula_id = $informe->id;
							$pensiones->status = 'inactiva';
							$pensiones->created_at = new \DateTime($fecha_new);
							$pensiones->save();
						}
						if (($i == 8)) {
							$fecha_new = '01-04-' . $fecha_proxima->format('Y') . ' 05:00:00';
							$pensiones = new \App\HistoricoCartera;
							$pensiones->servicio = 'Pension';
							$pensiones->valor = $valor;
							$pensiones->otro = 'No';
							$pensiones->acudiente_id = $user->id;
							$pensiones->matricula_id = $informe->id;
							$pensiones->status = 'inactiva';
							$pensiones->created_at = new \DateTime($fecha_new);
							$pensiones->save();
						}
						if (($i == 9)) {
							$fecha_new = '01-05-' . $fecha_proxima->format('Y') . ' 05:00:00';
							$pensiones = new \App\HistoricoCartera;
							$pensiones->servicio = 'Pension';
							$pensiones->valor = $valor;
							$pensiones->otro = 'No';
							$pensiones->acudiente_id = $user->id;
							$pensiones->matricula_id = $informe->id;
							$pensiones->status = 'inactiva';
							$pensiones->created_at = new \DateTime($fecha_new);
							$pensiones->save();
						}
						if (($i == 10)) {
							$fecha_new = '01-06-' . $fecha_proxima->format('Y') . ' 05:00:00';
							$pensiones = new \App\HistoricoCartera;
							$pensiones->servicio = 'Pension';
							$pensiones->valor = $valor;
							$pensiones->otro = 'No';
							$pensiones->acudiente_id = $user->id;
							$pensiones->matricula_id = $informe->id;
							$pensiones->status = 'inactiva';
							$pensiones->created_at = new \DateTime($fecha_new);
							$pensiones->save();
						}
						if (($i == 11) && ($cuotas == 12)) {
							$fecha_new = '01-07-' . $fecha_proxima->format('Y') . ' 05:00:00';
							$pensiones = new \App\HistoricoCartera;
							$pensiones->servicio = 'Pension';
							$pensiones->valor = $valor;
							$pensiones->otro = 'No';
							$pensiones->acudiente_id = $user->id;
							$pensiones->matricula_id = $informe->id;
							$pensiones->status = 'inactiva';
							$pensiones->created_at = new \DateTime($fecha_new);
							$pensiones->save();
						}
						if (($i == 12) && ($cuotas == 12)) {
							$fecha_new = '01-08-' . $fecha_proxima->format('Y') . ' 05:00:00';
							$pensiones = new \App\HistoricoCartera;
							$pensiones->servicio = 'Pension';
							$pensiones->valor = $valor;
							$pensiones->otro = 'No';
							$pensiones->acudiente_id = $user->id;
							$pensiones->matricula_id = $informe->id;
							$pensiones->status = 'inactiva';
							$pensiones->created_at = new \DateTime($fecha_new);
							$pensiones->save();
						}
					} else {
						if (($i == 5) && ($mes_actual < 1)) {
							$fecha_new = '01-01-' . $fecha_proxima->format('Y') . ' 05:00:00';
							$pensiones = new \App\HistoricoCartera;
							$pensiones->servicio = 'Pension';
							$pensiones->valor = $valor;
							$pensiones->otro = 'No';
							$pensiones->acudiente_id = $user->id;
							$pensiones->matricula_id = $informe->id;
							$pensiones->status = 'inactiva';
							$pensiones->created_at = new \DateTime($fecha_new);
							$pensiones->save();
						}
						if (($i == 6) && ($mes_actual < 2)) {
							$fecha_new = '01-02-' . $fecha_proxima->format('Y') . ' 05:00:00';
							$pensiones = new \App\HistoricoCartera;
							$pensiones->servicio = 'Pension';
							$pensiones->valor = $valor;
							$pensiones->otro = 'No';
							$pensiones->acudiente_id = $user->id;
							$pensiones->matricula_id = $informe->id;
							$pensiones->status = 'inactiva';
							$pensiones->created_at = new \DateTime($fecha_new);
							$pensiones->save();
						}
						if (($i == 7) && ($mes_actual < 3)) {
							$fecha_new = '01-03-' . $fecha_proxima->format('Y') . ' 05:00:00';
							$pensiones = new \App\HistoricoCartera;
							$pensiones->servicio = 'Pension';
							$pensiones->valor = $valor;
							$pensiones->otro = 'No';
							$pensiones->acudiente_id = $user->id;
							$pensiones->matricula_id = $informe->id;
							$pensiones->status = 'inactiva';
							$pensiones->created_at = new \DateTime($fecha_new);
							$pensiones->save();
						}
						if (($i == 8) && ($mes_actual < 4)) {
							$fecha_new = '01-04-' . $fecha_proxima->format('Y') . ' 05:00:00';
							$pensiones = new \App\HistoricoCartera;
							$pensiones->servicio = 'Pension';
							$pensiones->valor = $valor;
							$pensiones->otro = 'No';
							$pensiones->acudiente_id = $user->id;
							$pensiones->matricula_id = $informe->id;
							$pensiones->status = 'inactiva';
							$pensiones->created_at = new \DateTime($fecha_new);
							$pensiones->save();
						}
						if (($i == 9) && ($mes_actual < 5)) {
							$fecha_new = '01-05-' . $fecha_proxima->format('Y') . ' 05:00:00';
							$pensiones = new \App\HistoricoCartera;
							$pensiones->servicio = 'Pension';
							$pensiones->valor = $valor;
							$pensiones->otro = 'No';
							$pensiones->acudiente_id = $user->id;
							$pensiones->matricula_id = $informe->id;
							$pensiones->status = 'inactiva';
							$pensiones->created_at = new \DateTime($fecha_new);
							$pensiones->save();
						}
						if (($i == 10) && ($mes_actual < 6)) {
							$fecha_new = '01-06-' . $fecha_proxima->format('Y') . ' 05:00:00';
							$pensiones = new \App\HistoricoCartera;
							$pensiones->servicio = 'Pension';
							$pensiones->valor = $valor;
							$pensiones->otro = 'No';
							$pensiones->acudiente_id = $user->id;
							$pensiones->matricula_id = $informe->id;
							$pensiones->status = 'inactiva';
							$pensiones->created_at = new \DateTime($fecha_new);
							$pensiones->save();
						}
						if (($i == 11) && ($mes_actual < 7) && ($cuotas == 12)) {
							$fecha_new = '01-07-' . $fecha_proxima->format('Y') . ' 05:00:00';
							$pensiones = new \App\HistoricoCartera;
							$pensiones->servicio = 'Pension';
							$pensiones->valor = $valor;
							$pensiones->otro = 'No';
							$pensiones->acudiente_id = $user->id;
							$pensiones->matricula_id = $informe->id;
							$pensiones->status = 'inactiva';
							$pensiones->created_at = new \DateTime($fecha_new);
							$pensiones->save();
						}
						if (($i == 12) && ($mes_actual < 8) && ($cuotas == 12)) {
							$fecha_new = '01-08-' . $fecha_proxima->format('Y') . ' 05:00:00';
							$pensiones = new \App\HistoricoCartera;
							$pensiones->servicio = 'Pension';
							$pensiones->valor = $valor;
							$pensiones->otro = 'No';
							$pensiones->acudiente_id = $user->id;
							$pensiones->matricula_id = $informe->id;
							$pensiones->status = 'inactiva';
							$pensiones->created_at = new \DateTime($fecha_new);
							$pensiones->save();
						}
					}
				}
			}

			$activarpension = HistoricoCartera::where('servicio', 'Pension')->where('acudiente_id', $user->id)
			->where('status', 'inactiva')->first();
			$activarpension->status = 'preactiva';
			$activarpension->save();
		}
		
		return $request;
	}
}