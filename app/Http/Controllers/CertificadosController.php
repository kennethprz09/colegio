<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Certificados;
use App\InformePago;

class CertificadosController extends Controller {
	public function ListCertificados() {
		$certificados = Certificados::where('status', 'Privado')->get();
		foreach ($certificados as $certificado) {
			$certificado->valor = preg_replace('/[.]/', '', $certificado->valor);
		}
		return $certificados;
	}

	public function List(Request $request) {
		$columns = [ 'id' ];

		$length = $request->input('length');
		$column = $request->input('column'); //Index
		$dir = $request->input('dir');
		$searchValue = $request->input('search');

		$query = Certificados::orderBy($columns[$column], $dir);

		if($searchValue) {
			$query->where(function($query) use ($searchValue) {
				$query->where('concepto', 'LIKE', '%' . $searchValue . '%')
				->orWhere('entrega', 'LIKE', '%' . $searchValue . '%')
				->orWhere('canal', 'LIKE', '%' . $searchValue . '%')
				->orWhere('valor', 'LIKE', '%' . $searchValue . '%')
				->orWhere('status', 'LIKE', '%' . $searchValue . '%');
			});
		}

		$projects = $query->paginate($length);
		return ['data' => $projects, 'draw' => $request->input('draw')];
	}

	public function Save(Request $request) {
		$this->validate($request, [
			'concepto' => 'required',
			'entrega' => 'required',
			'canal' => 'required',
			'valor' => 'required',
			'status' => 'required',
		]);

		Certificados::create($request->all());
	}

	public function Update(Request $request) {
		$this->validate($request, [
			'id' => 'required'
		]);

		Certificados::find($request->id)->update($request->all());
	}

	public function Delete($id) {
		Certificados::find($id)->delete();
	}

	public function Pagado(Request $request) {
		$informe = InformePago::create($request->all());
		return $request;
	}
}
