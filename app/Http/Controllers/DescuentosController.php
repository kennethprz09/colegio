<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Descuentos;
use App\InformePago;
use App\User;
use Auth;

class DescuentosController extends Controller {
	public function __construct() {
		$this->middleware('auth');
	}

	protected function ListDetalles($items) {
		foreach ($items as $item) {
			$item->inicia = $item->inicia_at->format('Y-m-d');
			$item->termina = $item->termina_at->format('Y-m-d');
			$item->inicia_l = $item->inicia_at->format('d-m-Y');
			$item->termina_l = $item->termina_at->format('d-m-Y');
		}
	}

	public function List(Request $request) {
		$columns = [ 'id' ];

		$length = $request->input('length');
		$column = $request->input('column'); //Index
		$dir = $request->input('dir');
		$searchValue = $request->input('search');

		$query = Descuentos::orderBy($columns[$column], $dir);

		if($searchValue) {
			$query->where(function($query) use ($searchValue) {
				$query->where('servicio', 'LIKE', '%' . $searchValue . '%')
				->orWhere('porcentaje', 'LIKE', '%' . $searchValue . '%')
				->orWhere('inicia_at', 'LIKE', '%' . $searchValue . '%')
				->orWhere('termina_at', 'LIKE', '%' . $searchValue . '%')
				->orWhere('codigo', 'LIKE', '%' . $searchValue . '%')
				->orWhere('uso', 'LIKE', '%' . $searchValue . '%');
			});
		}

		$projects = $query->paginate($length);
		$this->ListDetalles($projects);
		return ['data' => $projects, 'draw' => $request->input('draw')];
	}

	public function ListEstudiantes(Request $request) {
		$query = Descuentos::orderBy('id', 'desc')->where('codigo_estudiante', '!=', null)->get();
		foreach ($query as $item) {
			$acudiente = User::where('codigo', $item->codigo_estudiante)->first();
			if($acudiente) {
				$item->name_estudiante = $acudiente->name_estudiante;
				$item->grado = $acudiente->grado;
			}
		}
		return $query;
	}

	public function Save(Request $request) {
		$this->validate($request, [
			'servicio' => 'required',
			'inicia_at' => 'required',
			'termina_at' => 'required',
			'porcentaje' => 'required',
			'codigo' => 'required',
			'uso' => 'required',
		]);

		$objeto = new Descuentos;
		$objeto->servicio = $request->servicio;
		$objeto->inicia_at = new \DateTime($request->inicia_at);
		$objeto->termina_at = new \DateTime($request->termina_at);
		$objeto->porcentaje = $request->porcentaje;
		$objeto->codigo = $request->codigo;
		$objeto->codigo_estudiante = $request->codigo_estudiante;
		$objeto->uso = $request->uso;
		$objeto->save();

		// Descuentos::create($request->all());
	}

	public function Update(Request $request) {
		$this->validate($request, [
			'id' => 'required'
		]);

		$objeto = Descuentos::find($request->id);
		$objeto->servicio = $request->servicio;
		$objeto->inicia_at = new \DateTime($request->inicia_at);
		$objeto->termina_at = new \DateTime($request->termina_at);
		$objeto->porcentaje = $request->porcentaje;
		$objeto->codigo = $request->codigo;
		$objeto->codigo_estudiante = $request->codigo_estudiante;
		$objeto->uso = $request->uso;
		$objeto->save();

		// Descuentos::find($request->id)->update($request->all());
	}

	public function Delete($id) {
		Descuentos::find($id)->delete();
	}

	public function StatusChange(Request $request, $id) {
		$descuento = Descuentos::find($id);
		if ($descuento) {
			$descuento->status = $request->status;
			$descuento->save();
		}
	}

	public function BuscarDescuento(Request $request, $codigo, $tramite) {
		// Falta validar por estudiante
		if ($tramite == 'Extracurriculares') {
			$servicio = $tramite;
		} elseif ($tramite == 'Cursos') {
			$servicio = $tramite;
		}  elseif ($tramite == 'Nivelacion') {
			$servicio = $tramite;
		} elseif ($tramite == 'Pension') {
			$servicio = $tramite;
		} elseif ($tramite == 'Matriculas') {
			$servicio = $tramite;
		} elseif ($tramite == 'Matricula') {
			$servicio = 'Matriculas';
		} elseif ($tramite == 'Extracurricular') {
			$servicio = 'Extracurriculares';
		} elseif ($tramite == 'Curso') {
			$servicio = 'Cursos';
		} elseif ($tramite == 'Otro') {
			$servicio = 'Otro';
		} else {
			$servicio = null;
		}
		$hoy = new \DateTime();
		$descuento = Descuentos::where('codigo', $codigo)->where('servicio', $servicio)->whereDate('inicia_at', '<=', $hoy->format('Y-m-d'))->whereDate('termina_at', '>=', $hoy->format('Y-m-d'))->first();
		if($descuento) {
			if ($descuento->status == 'si') {
				if ($descuento->uso == 'Frecuente') {
					return $descuento;
				} else {
					$tipo_user = Auth::user()->roll;
					if ($tipo_user == "acudiente") {
						$pagos_old = InformePago::where('aprobado', 'A')->where('cupon', $codigo)->where('acudiente_id', Auth::user()->id)->get();
					} else {
						if ($request->codigoUser) {
							$acudiente = User::where('roll', 'acudiente')->where('codigo', $request->codigoUser)->first();
    						if ($acudiente) {
								$pagos_old = InformePago::where('aprobado', 'A')->where('cupon', $codigo)->where('acudiente_id', $acudiente->id)->get();
							}
						} else {
							$pagos_old = InformePago::where('aprobado', 'A')->where('cupon', $codigo)->where('acudiente_id', $request->user_id)->get();
						}
					}
					if (count($pagos_old) == 0) {
						return $descuento;
					} else {
						return 'Bad';
					}
				}
			} else {
				return 'Bad';
			}
		} else {
			return 'Bad';
		}
	}
}
