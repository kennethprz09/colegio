<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\User;
use App\AcuerdoPago;
use App\AcuerdoCuotas;
use App\HistoricoCartera;
use App\InformePago;
use Auth;
use Carbon\Carbon;

class AcuerdoPagoController extends Controller
{
    public function List(Request $request)
    {
        $columns = ['acuerdo_pagos.id', 'users.name_estudiante'];

        $length = $request->input('length');
        $column = $request->input('column'); //Index
        $dir = $request->input('dir');
        $searchValue = $request->input('search');

        $query = AcuerdoPago::orderBy($columns[$column], $dir)
            ->join('users', 'acuerdo_pagos.acudiente_id', '=', 'users.id')
            ->select('acuerdo_pagos.*', 'users.name_estudiante as acudiente_name', 'users.codigo as acudiente_codigo');

        if ($searchValue) {
            $query->where(function ($query) use ($searchValue) {
                $query->where('users.name_estudiante', 'LIKE', '%' . $searchValue . '%')
                    ->orWhere('users.codigo', 'LIKE', '%' . $searchValue . '%')
                    ->orWhere('acuerdo_pagos.deuda_total', 'LIKE', '%' . $searchValue . '%')
                    ->orWhere('acuerdo_pagos.created_at', 'LIKE', '%' . $searchValue . '%');
            });
        }

        $projects = $query->paginate($length);
        foreach ($projects as $item) {
            $item->pagadas = AcuerdoCuotas::where('acuerdo_id', $item->id)->where('estatus', 'Pagado')->count();
            $item->total = AcuerdoCuotas::where('acuerdo_id', $item->id)->count();
        }
        return ['data' => $projects, 'draw' => $request->input('draw')];
    }

    public function new_acuerdo(Request $request)
    {
        $this->validate($request, [
            'cantidad_cuotas' => 'required',
            'deuda_cuotas' => 'required',
            'deuda_total' => 'required',
            'fecha_at' => 'required',
        ]);

        $acuerdo_vigente = AcuerdoPago::where('acudiente_id', Auth::user()->id)->where('finalizado', 'no')->count();
        if ($acuerdo_vigente == 0) {
            $acuerdo = new AcuerdoPago;
            $acuerdo->cantidad_cuotas = $request->cantidad_cuotas;
            $acuerdo->deuda_cuotas = $request->deuda_cuotas;
            $acuerdo->deuda_total = $request->deuda_total;
            $acuerdo->fecha_at = new \DateTime($request->fecha_at);
            $acuerdo->acudiente_id = Auth::user()->id;
            $acuerdo->save();

            for ($i = 0; $i < $acuerdo->cantidad_cuotas; $i++) {
                $cuotas = new AcuerdoCuotas;
                $cuotas->mes = Carbon::create($request->fecha_at)->addMonth($i);
                $cuotas->valor = $request->deuda_cuotas;
                $cuotas->estatus = 'Pendiente';
                $cuotas->acuerdo_id = $acuerdo->id;
                $cuotas->save();
            }

            $deudas = HistoricoCartera::where('acudiente_id', Auth::user()->id)->where('status', 'activa')->get();
            foreach ($deudas as $deuda) {
                $deuda->acuerdo_id = $acuerdo->id;
                $deuda->save();
            }
        } else {
            return response()->json(['errors' => ['status' => ['Acuerdo de pago vigente']]], 401);
        }
    }

    public function Show($id)
    {
        $acuerdo = AcuerdoPago::find($id);
        if ($acuerdo) {
            $acuerdo->cuotas = AcuerdoCuotas::where('acuerdo_id', $acuerdo->id)->get();
            $acuerdo->deudas = HistoricoCartera::where('acuerdo_id', $acuerdo->id)->get();
            $acudiente = User::find($acuerdo->acudiente_id);
            if ($acudiente) {
                $acuerdo->acudiente_name = $acudiente->name_estudiante;
                $acuerdo->acudiente_codigo = $acudiente->codigo;
            }

            return $acuerdo;
        }
    }

    public function Aprobar($id)
    {
        $acuerdo = AcuerdoPago::find($id);
        if ($acuerdo) {
            $acuerdo->status = 'Aprobado';
            $acuerdo->save();
        }
    }

    public function Autenticar($id)
    {
        $acuerdo = AcuerdoPago::find($id);
        if ($acuerdo) {
            $acuerdo->status = 'Autenticado';
            $acuerdo->save();

            $deudas = HistoricoCartera::where('acuerdo_id', $acuerdo->id)->get();
            foreach ($deudas as $deuda) {
                $deuda->status = "Acuerdo";
                $deuda->save();
            }
        }
    }

    public function Rechazar($id)
    {
        $acuerdo = AcuerdoPago::find($id);
        if ($acuerdo) {
            $acuerdo->status = 'Rechazado';
            $acuerdo->save();
        }
    }

    public function Finalizar($id)
    {
        $acuerdo = AcuerdoPago::find($id)->delete();
    }

    public function Pdf(Request $request, $id)
    {
        $acuerdo = AcuerdoPago::find($id);
        if ($acuerdo) {
            $file = $request->file('pdf'); // Manipulando imagen
            $file_veri = file_exists($file);
            if ($file_veri == 'true') {
                $destinationPath = public_path() . '/acuerdos';
                $filenameO = $file->getClientOriginalName();
                $filename = Str::random(12);
                $upload_success = $file->move($destinationPath, $filename . '-' . $filenameO);
                $acuerdo->pdf = $filename . '-' . $filenameO; // Guardando Imagen
            }
            $acuerdo->save();
        }
    }

    public function Show_acudiente()
    {
        $acuerdo = AcuerdoPago::where('acudiente_id', Auth::user()->id)->where('finalizado', 'no')->orderBy('id', 'desc')->first();
        if ($acuerdo) {
            $acuerdo->cuotas = AcuerdoCuotas::where('acuerdo_id', $acuerdo->id)->get();
        }
        return $acuerdo;
    }

    public function Pagado(Request $request)
    {
        $informe = InformePago::create($request->all());
        if ($informe->aprobado == 'A') {
            $cuota = AcuerdoCuotas::Find($request->_uid);
            $cuota->estatus = 'Pagado';
            $cuota->save();

            $cuotas = AcuerdoCuotas::where("acuerdo_id", $cuota->acuerdo_id)->where("estatus", "Pendiente")->count();
            if ($cuotas == 0) {
                $acuerdo = AcuerdoPago::Find($cuota->acuerdo_id);
                $acuerdo->finalizado = 'Cumplido';
                $acuerdo->save();
            }
        }

        return $request;
    }
}
