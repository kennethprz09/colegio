<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\InformePago;
use App\HistoricoCartera;
use App\User;
use App\Mail\RecordarCartera;
use Auth, Mail;

class HistoricoCarteraController extends Controller { 
  public function Recordatorio(Request $request) {
    $user = User::find($request->acudiente_id);
    Mail::to($user->email)->send(new RecordarCartera($user));
    Mail::to('cartera@colegiocatolicocali.edu.co')->send(new RecordarCartera($user));
    // Mail::to('kennethprz09@gmail.com')->send(new RecordarCartera($user));
    return "Good";
  }

  public function List(Request $request) {
    $columns = [ 'historico_carteras.id'];

    $length = $request->input('length');
    $column = $request->input('column'); //Index
    $dir = $request->input('dir');
    $searchValue = $request->input('search');

    $query = HistoricoCartera::orderBy($columns[$column], $dir)->where('status', 'activa')
    ->join('users', 'historico_carteras.acudiente_id', '=', 'users.id')
    ->select('historico_carteras.*', 'users.name_estudiante as acudiente_name', 'users.codigo as acudiente_codigo', 'users.grado as acudiente_grado', 'users.email as acudiente_email')
    ->groupby('historico_carteras.acudiente_id')->distinct();

    if($searchValue) {
      $query->where(function($query) use ($searchValue) {
        $query->where('users.name_estudiante', 'LIKE', '%' . $searchValue . '%')
        ->orWhere('users.codigo', 'LIKE', '%' . $searchValue . '%')
        ->orWhere('users.grado', 'LIKE', '%' . $searchValue . '%')
        ->orWhere('users.email', 'LIKE', '%' . $searchValue . '%')
        ->orWhere('historico_carteras.servicio', 'LIKE', '%' . $searchValue . '%')
        ->orWhere('historico_carteras.valor', 'LIKE', '%' . $searchValue . '%');
      });
    }

    $projects = $query->paginate($length);
    foreach ($projects as $item) {
      $item->valor_total = HistoricoCartera::where('status', 'activa')->where('acudiente_id', $item->acudiente_id)->sum('valor');
    }
    return ['data' => $projects, 'draw' => $request->input('draw')];
  }

  public function ListAcudiente(Request $request) {
    $columns = [ 'historico_carteras.id'];

    $length = $request->input('length');
    $column = $request->input('column'); //Index
    $dir = $request->input('dir');
    $searchValue = $request->input('search');

    $query = HistoricoCartera::orderBy($columns[$column], $dir)->where('status', 'activa')
    ->where('historico_carteras.acudiente_id', Auth::user()->id)
    ->join('users', 'historico_carteras.acudiente_id', '=', 'users.id')
    ->select('historico_carteras.*', 'users.name_estudiante as acudiente_name', 'users.codigo as acudiente_codigo', 'users.grado as acudiente_grado', 'users.email as acudiente_email');

    if($searchValue) {
      $query->where(function($query) use ($searchValue) {
        $query->where('users.name_estudiante', 'LIKE', '%' . $searchValue . '%')
        ->orWhere('users.codigo', 'LIKE', '%' . $searchValue . '%')
        ->orWhere('users.grado', 'LIKE', '%' . $searchValue . '%')
        ->orWhere('users.email', 'LIKE', '%' . $searchValue . '%')
        ->orWhere('historico_carteras.servicio', 'LIKE', '%' . $searchValue . '%')
        ->orWhere('historico_carteras.valor', 'LIKE', '%' . $searchValue . '%');
      });
    }

    $projects = $query->paginate($length);
    return ['data' => $projects, 'draw' => $request->input('draw')];
  }

  public function ListAdminDetalles(Request $request, $codigo) {
    $estudiante = User::where('codigo', $codigo)->first();  // get estudiantes
		if(!$estudiante){
			return 'Bad';
		}
    $columns = [ 'historico_carteras.id'];

    $length = $request->input('length');
    $column = $request->input('column'); //Index
    $dir = $request->input('dir');
    $searchValue = $request->input('search');

    $query = HistoricoCartera::orderBy('historico_carteras.id', 'desc')->where('status', 'activa')
    ->where('historico_carteras.acudiente_id', $estudiante->id)
    ->join('users', 'historico_carteras.acudiente_id', '=', 'users.id')
    ->select('historico_carteras.*', 'users.name_estudiante as acudiente_name', 'users.codigo as acudiente_codigo', 'users.grado as acudiente_grado', 'users.email as acudiente_email');

    if($searchValue) {
      $query->where(function($query) use ($searchValue) {
        $query->where('users.name_estudiante', 'LIKE', '%' . $searchValue . '%')
        ->orWhere('users.codigo', 'LIKE', '%' . $searchValue . '%')
        ->orWhere('users.grado', 'LIKE', '%' . $searchValue . '%')
        ->orWhere('users.email', 'LIKE', '%' . $searchValue . '%')
        ->orWhere('historico_carteras.servicio', 'LIKE', '%' . $searchValue . '%')
        ->orWhere('historico_carteras.valor', 'LIKE', '%' . $searchValue . '%');
      });
    }

    $projects = $query->paginate($length);
    return ['data' => $projects, 'draw' => $request->input('draw')];
  }
  
  public function ListAdminDetallesUser(Request $request, $codigo) {
    $estudiante = User::where('codigo', $codigo)->first();  // get estudiantes
		if(!$estudiante){
			return 'Bad';
		} else {
      $deudas = HistoricoCartera::orderBy('id', 'desc')->where('status', 'activa')->where('acudiente_id', $estudiante->id)->pluck('valor');
      $deuda = 0;
      foreach ($deudas as $value) {
        $deuda = $deuda + $value;
      }
      $estudiante->deuda = $deuda;
      return $estudiante;
    }
  }

  public function ListAdmin($codigo) {
    $estudiante = User::where('codigo', $codigo)->first();  // get estudiantes
		if(!$estudiante){
			return 'Bad';
		}

    $query = HistoricoCartera::orderBy('historico_carteras.id', 'desc')
    ->where('historico_carteras.acudiente_id', $estudiante->id)
    ->where(function($query) {
      $query->where('status', '=', "activa")
        ->orWhere('status', '=', "preactiva");
    })
    ->join('users', 'historico_carteras.acudiente_id', '=', 'users.id')
    ->select('historico_carteras.*', 'users.name_estudiante as acudiente_name', 'users.codigo as acudiente_codigo', 'users.grado as acudiente_grado', 'users.email as acudiente_email')
    ->get();

    return $query;
  }

  public function ListOtros(Request $request) {
    $columns = [ 'historico_carteras.id'];

    $length = $request->input('length');
    $column = $request->input('column'); //Index
    $dir = $request->input('dir');
    $searchValue = $request->input('search');

    $query = HistoricoCartera::orderBy($columns[$column], $dir)->where('otro', 'Si')->where('status', 'activa')
    ->join('users', 'historico_carteras.acudiente_id', '=', 'users.id')
    ->select('historico_carteras.*', 'users.name_estudiante as acudiente_name', 'users.codigo as acudiente_codigo', 'users.grado as acudiente_grado', 'users.email as acudiente_email');

    if($searchValue) {
      $query->where(function($query) use ($searchValue) {
        $query->where('users.name_estudiante', 'LIKE', '%' . $searchValue . '%')
        ->orWhere('users.codigo', 'LIKE', '%' . $searchValue . '%')
        ->orWhere('users.grado', 'LIKE', '%' . $searchValue . '%')
        ->orWhere('users.email', 'LIKE', '%' . $searchValue . '%')
        ->orWhere('historico_carteras.descripcion', 'LIKE', '%' . $searchValue . '%')
        ->orWhere('historico_carteras.servicio', 'LIKE', '%' . $searchValue . '%')
        ->orWhere('historico_carteras.valor', 'LIKE', '%' . $searchValue . '%');
      });
    }

    $projects = $query->paginate($length);
    return ['data' => $projects, 'draw' => $request->input('draw')];
  }

  public function OtrosSave(Request $request) {
    $acudiente = User::where('roll', 'acudiente')->where('codigo', $request->codigo_estudiante)->first();
    if ($acudiente) {
      $otro = new HistoricoCartera;
      $otro->servicio = $request->servicio;
      $otro->valor = preg_replace('/[.]/', '', $request->valor);
      $otro->otro = 'Si';
      $otro->descripcion = $request->descripcion;
      $otro->acudiente_id = $acudiente->id;
      $otro->status = 'activa';
      if ($request->created_at) {
        $fecha_new = new \DateTime($request->created_at);
        $otro->created_at = new \DateTime($fecha_new->format('d-m-Y') . ' 05:00:00');
      }
      $otro->save();
    } else {
      return 'Bad';
    }
  }

  public function OtrosUpdate(Request $request) {
    $acudiente = User::where('roll', 'acudiente')->where('codigo', $request->codigo_estudiante)->first();
    if ($acudiente) {
      $otro = HistoricoCartera::find($request->id);
      if ($otro) {
        $otro->servicio = $request->servicio;
        $otro->valor = preg_replace('/[.]/', '', $request->valor);
        $otro->descripcion = $request->descripcion;
        $otro->acudiente_id = $acudiente->id;
        if ($request->created_at) {
          $fecha_new = new \DateTime($request->created_at);
          $otro->created_at = new \DateTime($fecha_new->format('d-m-Y') . ' 05:00:00');
        }
        $otro->save();
      }
    } else {
      return 'Bad';
    }
  }

  public function OtrosDelete($id) {
    $otro = HistoricoCartera::find($id)->delete();
  }

  public function Pagado(Request $request) {
		$informe = InformePago::create($request->all());
		if($informe->aprobado == 'A') {
      $cartera = HistoricoCartera::find($request->_uid);
			$cartera->status = 'Pagada';
			$cartera->save();
		}
		
		return $request;
	}
}
