<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cursos;
use App\Docente;
use App\Nivelacion;
use App\InformePago;

class CursosController extends Controller {
	public function ListNivelacion() {
		$nivelaciones = Nivelacion::all();
		$this->ListDetalles($nivelaciones);
		return $nivelaciones;
	}

	public function NivelacionUpdate(Request $request) {
		$messages = [
			'after' => 'La fecha de terminación debe ser mayor a la de iniciación.',
		];
		$this->validate($request, [
			'id' => 'required',
			'inicia_at' => 'required',
			'termina_at' => 'required|after:inicia_at',
		], $messages);

		$objeto = Nivelacion::find($request->id);
		$objeto->email = $request->email;
		$objeto->valor = $request->valor;
		$objeto->inicia_at = new \DateTime($request->inicia_at);
		$objeto->termina_at = new \DateTime($request->termina_at);
		$objeto->save();
	}


	protected function ListDetalles($items) {
		foreach ($items as $item) {
			$item->inicia = $item->inicia_at->format('Y-m-d');
			$item->termina = $item->termina_at->format('Y-m-d');
			$item->inicia_l = $item->inicia_at->format('d-m-Y');
			$item->termina_l = $item->termina_at->format('d-m-Y');
			$item->valor = preg_replace('/[.]/', '', $item->valor);
		}
	}

	public function List(Request $request) {
		$columns = [ 'id' ];

		$length = $request->input('length');
		$column = $request->input('column'); //Index
		$dir = $request->input('dir');
		$searchValue = $request->input('search');
		
		$hoy = new \DateTime();
		$query = Cursos::orderBy($columns[$column], $dir)->whereDate('inicia_at', '<=', $hoy->format('Y-m-d'))->whereDate('termina_at', '>=', $hoy->format('Y-m-d'));

		if($searchValue) {
			$query->where(function($query) use ($searchValue) {
				$query->where('asignatura', 'LIKE', '%' . $searchValue . '%')
				->orWhere('inicia_at', 'LIKE', '%' . $searchValue . '%')
				->orWhere('termina_at', 'LIKE', '%' . $searchValue . '%')
				->orWhere('tarifa', 'LIKE', '%' . $searchValue . '%');
			});
		}

		$projects = $query->paginate($length);
		$this->ListDetalles($projects);
		return ['data' => $projects, 'draw' => $request->input('draw')];
	}

	public function ListManual(Request $request) {
		$cursos = Cursos::orderBy('id', 'desc')->get();
		foreach ($cursos as $curso) {
			$curso->tarifa = preg_replace('/[.]/', '', $curso->tarifa);
		}

		return $cursos;
	}

	public function ListDocentes() {
		return Docente::all();
	}

	public function Save(Request $request) {
		$messages = [
			'after' => 'La fecha de terminación debe ser mayor a la de iniciación.',
		];
		$this->validate($request, [
			'asignatura' => 'required',
			'inicia_at' => 'required',
			'termina_at' => 'required|after:inicia_at',
			'docente_id' => 'required',
			'tarifa' => 'required',
		], $messages);

		$docente = Docente::find($request->docente_id);

		$objeto = new Cursos;
		$objeto->asignatura = $request->asignatura;
		$objeto->inicia_at = new \DateTime($request->inicia_at);
		$objeto->termina_at = new \DateTime($request->termina_at);
		$objeto->docente_id = $request->docente_id;
		$objeto->docente = $docente->nombre;
		$objeto->tarifa = $request->tarifa;
		$objeto->save();

		// Cursos::create($request->all());
	}

	public function Update(Request $request) {
		$messages = [
			'after' => 'La fecha de terminación debe ser mayor a la de iniciación.',
		];
		$this->validate($request, [
			'id' => 'required',
			'inicia_at' => 'required',
			'termina_at' => 'required|after:inicia_at',
		], $messages);

		$docente = Docente::find($request->docente_id);

		$objeto = Cursos::find($request->id);
		$objeto->asignatura = $request->asignatura;
		$objeto->inicia_at = new \DateTime($request->inicia_at);
		$objeto->termina_at = new \DateTime($request->termina_at);
		$objeto->docente_id = $request->docente_id;
		$objeto->docente = $docente->nombre;
		$objeto->tarifa = $request->tarifa;
		$objeto->save();

		// Cursos::find($request->id)->update($request->all());
	}

	public function Pagado(Request $request) {
		$informe = InformePago::create($request->all());
		return $request;
	}

	public function Delete($id) {
		Cursos::find($id)->delete();
	}
}
