<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transporte;
use App\InformePago;

class TransporteController extends Controller {
	public function ListManual(Request $request) {
		$query = Transporte::orderBy('id', 'desc')->get();
		return $query;
	}

	public function List(Request $request) {
		$columns = [ 'id' ];

		$length = $request->input('length');
		$column = $request->input('column'); //Index
		$dir = $request->input('dir');
		$searchValue = $request->input('search');

		$query = Transporte::orderBy($columns[$column], $dir);

		if($searchValue) {
			$query->where(function($query) use ($searchValue) {
				$query->where('descripcion', 'LIKE', '%' . $searchValue . '%')
				->orWhere('pago', 'LIKE', '%' . $searchValue . '%')
				->orWhere('valor', 'LIKE', '%' . $searchValue . '%');
			});
		}

		$projects = $query->paginate($length);
		return ['data' => $projects, 'draw' => $request->input('draw')];
	}

	public function Save(Request $request) {
		$this->validate($request, [
			'descripcion' => 'required',
			'pago' => 'required',
			'valor' => 'required'
		]);

		Transporte::create($request->all());
	}

	public function Update(Request $request) {
		$this->validate($request, [
			'id' => 'required'
		]);

		Transporte::find($request->id)->update($request->all());
	}

	public function Delete($id) {
		Transporte::find($id)->delete();
	}
	
	public function Pagado(Request $request) {
		$informe = InformePago::create($request->all());
		return $request;
	}
}
