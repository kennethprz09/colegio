<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SolicitudIngreso;
use App\InformePago;
use App\User;
use App\Config;
use stdClass;

class SolicitudIngresoController extends Controller {
	public function List(Request $request) {
		$columns = [ 'id' ];

		$length = $request->input('length');
		$column = $request->input('column'); //Index
		$dir = $request->input('dir');
		$searchValue = $request->input('search');

		$query = SolicitudIngreso::orderBy($columns[$column], $dir)->where('codigo_estudiante', '!=', null);

		if($searchValue) {
			$query->where(function($query) use ($searchValue) {
				$query->where('step1_nombre_completo', 'LIKE', '%' . $searchValue . '%')
				->orWhere('step1_grado', 'LIKE', '%' . $searchValue . '%')
				->orWhere('step2_nombre_padre', 'LIKE', '%' . $searchValue . '%')
				->orWhere('step2_email_padre', 'LIKE', '%' . $searchValue . '%')
				->orWhere('step2_nombre_madre', 'LIKE', '%' . $searchValue . '%')
				->orWhere('step2_email_madre', 'LIKE', '%' . $searchValue . '%')
				->orWhere('step3_nombre_otro', 'LIKE', '%' . $searchValue . '%')
				->orWhere('step3_email_otro', 'LIKE', '%' . $searchValue . '%');
			});
		}

		$projects = $query->paginate($length);
		return ['data' => $projects, 'draw' => $request->input('draw')];
	}

	public function ListCompromiso(Request $request) {
		$columns = [ 'id' ];

		$length = $request->input('length');
		$column = $request->input('column'); //Index
		$dir = $request->input('dir');
		$searchValue = $request->input('search');

		$query = SolicitudIngreso::orderBy($columns[$column], $dir)->where('codigo_estudiante', '!=', null)->where('nivelacion', 'Si');

		if($searchValue) {
			$query->where(function($query) use ($searchValue) {
				$query->where('step1_nombre_completo', 'LIKE', '%' . $searchValue . '%')
				->orWhere('step1_grado', 'LIKE', '%' . $searchValue . '%')
				->orWhere('step2_nombre_padre', 'LIKE', '%' . $searchValue . '%')
				->orWhere('step2_email_padre', 'LIKE', '%' . $searchValue . '%')
				->orWhere('step2_nombre_madre', 'LIKE', '%' . $searchValue . '%')
				->orWhere('step2_email_madre', 'LIKE', '%' . $searchValue . '%')
				->orWhere('step3_nombre_otro', 'LIKE', '%' . $searchValue . '%')
				->orWhere('step3_nombre_otro', 'LIKE', '%' . $searchValue . '%')
				->orWhere('compromiso', 'LIKE', '%' . $searchValue . '%');
			});
		}

		$projects = $query->paginate($length);
		return ['data' => $projects, 'draw' => $request->input('draw')];
	}

    public function Save(Request $request) {
		$postulacion = SolicitudIngreso::create($request->all());
		return $postulacion; 
	}

	public function UpdateState(Request $request) {
		$postulacion = SolicitudIngreso::find($request->id);
		if ($request->status) {
			$postulacion->status = $request->status;
		}
		if ($request->nivelacion) {
			$postulacion->nivelacion = $request->nivelacion;
			$postulacion->compromiso = $request->compromiso;
		}
		if ($request->status_nivelacion) {
			$postulacion->status_nivelacion = $request->status_nivelacion;
			if ($postulacion->status == "Matriculado") {
				$acudiente = User::where('roll', 'acudiente')->where('codigo', $postulacion->codigo_estudiante)->first();
				if ($acudiente) {
					if ($request->status_nivelacion == "Si") {
						$acudiente->nivelacion = "No";
						$acudiente->save();
					}
				}
			}
		}
		$postulacion->save();

		if ($request->status == 'Matriculado') {
			$acudiente = new stdClass;
			if ($postulacion->step3_responsable == 'Madre' ) {
				$acudiente->name = $postulacion->step2_nombre_madre;
				$acudiente->email = $postulacion->step2_email_madre;
			}
			if ($postulacion->step3_responsable == 'Padre' ) {
				$acudiente->name = $postulacion->step2_nombre_padre;
				$acudiente->email = $postulacion->step2_email_padre;
			}
			if ($postulacion->step3_responsable == 'Acudiente' ) {
				$acudiente->name = $postulacion->step3_nombre_otro;
				$acudiente->email = $postulacion->step3_email_otro;
			}

			$user = new User;
			$user->name = $acudiente->name;
			$user->email = $acudiente->email;
			
            $user->codigo = $postulacion->codigo_estudiante;
            $user->grado = $postulacion->step1_grado;
            $user->name_estudiante = $postulacion->step1_nombre_completo;
			$user->password = bcrypt($postulacion->codigo_estudiante);
			$user->nivelacion = $postulacion->nivelacion;
			$user->save();

			// Aca debo mandar a crear en el otro sistema
		}
	}

	public function PostulacionManual(Request $request) {
		$config = Config::first();
		$config->precio_ingreso = preg_replace('/[.]/', '', $config->precio_ingreso);

		$postulacion = SolicitudIngreso::create($request->all());
		// Genero su codigo unico de estudiante
		$año = new \DateTime();
		$serie = 3;
		$number = SolicitudIngreso::where('codigo_estudiante', '!=', null)->whereYear('created_at', $año->format("Y"))->count() + 1;
		$subcodigo = substr(str_repeat(0, $serie).$number, - $serie);
		$codigoFinal = $año->format("y") . $subcodigo;
		
		$postulacion->codigo_estudiante = $codigoFinal;
		$postulacion->save();

		$informe = InformePago::create($request->all());
		$informe->aprobado = "A";
		$informe->valor = $config->precio_ingreso;
		$informe->valor_subtotal = $config->precio_ingreso;
		$informe->_tramite = "Inscripcion";
		$informe->_uid = $postulacion->id;
		$informe->tipo = 'Caja';
		$informe->tipo_pago = $request->tipo_pago;
		$informe->save();
		return "Good";
	}

	public function Pagado(Request $request) {
		$informe = InformePago::create($request->all());
		if ($request->aprobado == 'A') {
			$postulacion = SolicitudIngreso::find($request->_uid);
			// Genero su codigo unico de estudiante
			$año = new \DateTime();
			$serie = 3;
			$number = SolicitudIngreso::where('codigo_estudiante', '!=', null)->whereYear('created_at', $año->format("Y"))->count() + 1;
			$subcodigo = substr(str_repeat(0, $serie).$number, - $serie);
			$codigoFinal = $año->format("y") . $subcodigo;
			
			$postulacion->codigo_estudiante = $codigoFinal;
			$postulacion->save();

			return $postulacion->codigo_estudiante;
		}
	}
}
