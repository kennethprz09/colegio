<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\InformePago;
use App\User;
use App\Extracurriculares;
use App\MediasTecnicas;
use App\Certificados;
use App\Matriculas;
use App\Pension;
use App\Nivelacion;
use App\Cursos;
use App\HistoricoCartera;
use App\AcuerdoPago;
use App\AcuerdoCuotas;
use App\SolicitudIngreso;
use App\Cafeteria;
use App\Transporte;
use App\SolicitudCertificadosPublicos;
use App\Externos\Estudiante;
use App\Externos\Madre;
use App\Externos\Padre;
use App\Externos\Acudiente;
use Carbon\Carbon;
use Auth, File, NumberFormatter;

class InformePagoController extends Controller {
	protected function ListDetalles($items) {
		foreach ($items as $item) {
			$item->create = $item->created_at->format('d-m-Y');
			if ($item->acudiente_id) {
				$acudiente = User::find($item->acudiente_id);
				if ($acudiente) {
					$item->name = $acudiente->name_estudiante;
					$item->codigo = $acudiente->codigo;
				}
			}
			if ($item->_tramite == "Inscripcion") {
				$acudiente = SolicitudIngreso::find($item->_uid);
				if ($acudiente) {
					$item->name = $acudiente->step1_nombre_completo;
					$item->codigo = $acudiente->codigo_estudiante;
				}
			}
			if ($item->_tramite == "Pension") {
				$pension = HistoricoCartera::find($item->_uid);
				setlocale(LC_ALL, 'es_ES');
				if ($pension) {
					$fecha = Carbon::parse($pension->created_at);
					$fecha->format("F"); // Inglés.
					$item->_tramite_desc = $fecha->formatLocalized('%B');// mes en idioma español
				}
			}
			if ($item->_tramite == "Otro") {
				$otro = HistoricoCartera::find($item->_uid);
				if($otro){
    				$item->_tramite_desc = $otro->descripcion;
				}
			}
			if ($item->_tramite == "Certificado") {
				$certificados = Certificados::find($item->_uid);
				if($certificados){
    				$item->_tramite_desc = $certificados->concepto;
				}
			}
			if ($item->_tramite == "Curso") {
				$cursos = Cursos::find($item->_uid);
				if ($cursos) {
					$item->_tramite_desc = $cursos->asignatura;
				}
			}
			if ($item->_tramite == "Cafeteria") {
				$cafeteria = Cafeteria::find($item->_uid);
				if ($cafeteria) {
					$item->_tramite_desc = $cafeteria->descripcion;
				}
			}
			if ($item->_tramite == "Transporte") {
				$transporte = Transporte::find($item->_uid);
				if ($transporte) {
					$item->_tramite_desc = $transporte->descripcion;
				}
			}
			if ($item->_tramite == "Extracurricular") {
				$extracurricular = Extracurriculares::find($item->_uid);
				if ($extracurricular) {
					$item->_tramite_desc = $extracurricular->actividad;
				}
			}
			if ($item->_tramite == "Certificado publico") {
				$solicitud = SolicitudCertificadosPublicos::find($item->servicio_id);
				if ($solicitud) {
					$item->name = $solicitud->nombres_estudiante;
				}
				$certificados = Certificados::find($item->_uid);
				if ($certificados) {
					$item->_tramite_desc = $certificados->concepto;
				}
			}
		}
	}

    public function List(Request $request) {
		$columns = [ 'id' ];

		$length = $request->input('length');
		$column = $request->input('column'); //Index
		$dir = $request->input('dir');
		$searchValue = $request->input('search');

		$query = InformePago::orderBy($columns[$column], $dir)->where('informe_pagos.aprobado', 'A')->whereNotNull('informe_pagos.acudiente_id')
		->join('users', 'informe_pagos.acudiente_id', '=', 'users.id')
		->select('informe_pagos.*', 'users.codigo as codigo_estudiante', 'users.name as name_estudiante');

		if($searchValue) {
			$query->where(function($query) use ($searchValue) {
				$query->where('users.codigo', 'LIKE', '%' . $searchValue . '%')
				->orWhere('users.name_estudiante', 'LIKE', '%' . $searchValue . '%')
				->orWhere('informe_pagos._tramite', 'LIKE', '%' . $searchValue . '%');
			});
		}

		$projects = $query->paginate($length);
		$this->ListDetalles($projects);

		return ['data' => $projects, 'draw' => $request->input('draw')];
	}

	public function ListPublicos(Request $request) {
		$columns = [ 'id' ];

		$length = $request->input('length');
		$column = $request->input('column'); //Index
		$dir = $request->input('dir');
		$searchValue = $request->input('search');

		$query = InformePago::orderBy($columns[$column], $dir)->where('informe_pagos.aprobado', 'A')->whereNull('informe_pagos.acudiente_id');

		if($searchValue) {
			$query->where(function($query) use ($searchValue) {
				$query->where('informe_pagos._tramite', 'LIKE', '%' . $searchValue . '%');
			});
		}

		$projects = $query->paginate($length);
		$this->ListDetalles($projects);

		return ['data' => $projects, 'draw' => $request->input('draw')];
	}

	public function ListManual(Request $request) {
		$columns = [ 'id'];

		$length = $request->input('length');
		$column = $request->input('column'); //Index
		$dir = $request->input('dir');
		$searchValue = $request->input('search');

		$query = InformePago::orderBy($columns[$column], $dir)->where('tipo', 'Caja')
		->join('users', 'informe_pagos.acudiente_id', '=', 'users.id')
		->select('informe_pagos.*', 'users.codigo as codigo_estudiante', 'users.name_estudiante as name');

		if($searchValue) {
			$query->where(function($query) use ($searchValue) {
				$query->where('users.name', 'LIKE', '%' . $searchValue . '%')
				->orWhere('informe_pagos.tipo_pago', 'LIKE', '%' . $searchValue . '%')
				->orWhere('informe_pagos.created_at', 'LIKE', '%' . $searchValue . '%')
				->orWhere('informe_pagos._tramite', 'LIKE', '%' . $searchValue . '%')
				->orWhere('informe_pagos.valor', 'LIKE', '%' . $searchValue . '%');
			});
		}

		$projects = $query->paginate($length);
		$this->ListDetalles($projects);
		return ['data' => $projects, 'draw' => $request->input('draw')];
	}

	public function Listcertificados(Request $request) {
		$columns = [ 'id', 'transaccionId' ];

		$length = $request->input('length');
		$column = $request->input('column'); //Index
		$dir = $request->input('dir');
		$searchValue = $request->input('search');

		$query = InformePago::orderBy($columns[$column], $dir)->where('informe_pagos.aprobado', 'A')->where('informe_pagos._tramite', 'Certificado')
		->join('users', 'informe_pagos.acudiente_id', '=', 'users.id')
        ->select('informe_pagos.*', 'users.name_estudiante as name', 'users.codigo as codigo');

		if($searchValue) {
			$query->where(function($query) use ($searchValue) {
				$query->where('users.name_estudiante', 'LIKE', '%' . $searchValue . '%')
				->orWhere('users.codigo', 'LIKE', '%' . $searchValue . '%')
				->orWhere('informe_pagos.created_at', 'LIKE', '%' . $searchValue . '%')
				->orWhere('informe_pagos.certificado_motivo', 'LIKE', '%' . $searchValue . '%');
			});
		}

		$projects = $query->paginate($length);
		foreach ($projects as $item) {
			$certificado = Certificados::find($item->servicio_id);
			if ($certificado) {
				$item->concepto = $certificado->concepto;
				$item->canal = $certificado->canal;
			}
		}
		return ['data' => $projects, 'draw' => $request->input('draw')];
	}

	public function CertificadosListo($id) {
		$certificado = InformePago::find($id);
		$certificado->certificado_estado = 'Procesado';
		$certificado->save();
	}

	public function CertificadosPdf(Request $request, $id) {
		$certificado = InformePago::find($id);
		$file = $request->file('pdf'); // Manipulando imagen
			$file_veri = file_exists($file);
			if ($file_veri == 'true') {
				$destinationPath = public_path().'/certificados';
				$filenameO = $file->getClientOriginalName();
				$filename = Str::random(12);
				$upload_success = $file->move($destinationPath, $filename . '-' . $filenameO);
				$certificado->certificado_archivo = $filename . '-' . $filenameO; // Guardando Imagen
				$certificado->certificado_estado = 'Procesado';
			}
		$certificado->save();
	}

	public function ListAcudiente(Request $request) {
		$columns = [ 'id' ];

		$length = $request->input('length');
		$column = $request->input('column'); //Index
		$dir = $request->input('dir');
		$searchValue = $request->input('search');

		$query = InformePago::orderBy($columns[$column], $dir)->where('aprobado', 'A')->where('acudiente_id', Auth::User()->id);

		if($searchValue) {
			$query->where(function($query) use ($searchValue) {
				$query->where('transaccionId', 'LIKE', '%' . $searchValue . '%')
				->orWhere('_tramite', 'LIKE', '%' . $searchValue . '%')
				->orWhere('created_at', 'LIKE', '%' . $searchValue . '%')
				->orWhere('valor', 'LIKE', '%' . $searchValue . '%');
			});
		}

		$projects = $query->paginate($length);
		$this->ListDetalles($projects);
		return ['data' => $projects, 'draw' => $request->input('draw')];
	}

	public function ListAcudienteDocs(Request $request) {
		$columns = [ 'id' ];

		$length = $request->input('length');
		$column = $request->input('column'); //Index
		$dir = $request->input('dir');
		$searchValue = $request->input('search');

		$query = InformePago::orderBy($columns[$column], $dir)->where('informe_pagos.aprobado', 'A')->where('informe_pagos.acudiente_id', Auth::User()->id)->where('informe_pagos._tramite', 'Certificado')
		->join('certificados', 'informe_pagos.servicio_id', '=', 'certificados.id')
        ->select('informe_pagos.*', 'certificados.concepto as concepto', 'certificados.canal as canal', 'certificados.entrega as entrega');

		if($searchValue) {
			$query->where(function($query) use ($searchValue) {
				$query->where('certificados.concepto', 'LIKE', '%' . $searchValue . '%')
				->orWhere('informe_pagos.created_at', 'LIKE', '%' . $searchValue . '%')
				->orWhere('informe_pagos.certificado_estado', 'LIKE', '%' . $searchValue . '%')
				->orWhere('certificados.entrega', 'LIKE', '%' . $searchValue . '%')
				->orWhere('informe_pagos.valor', 'LIKE', '%' . $searchValue . '%');
			});
		}

		$projects = $query->paginate($length);
		return ['data' => $projects, 'draw' => $request->input('draw')];
	}

	public function BuscarDatos($codigo) {
		$estudiante = Estudiante::where('estado', null)->where('codigo_estuiante', $codigo)->first();  // get estudiantes
		if(!$estudiante){
			return 'Bad';
		}
		$mama = Madre::where('id_estudiante', $estudiante->id)->first();
		$padre = Padre::where('id_estudiante', $estudiante->id)->first();
		$acudiente = Acudiente::where('id_estudiante', $estudiante->id)->first();

		// Validamor quien tiene que paga 
		if ($mama['responsable'] == 'si') {
			$estudiante->prefijo = 'madre';
			$estudiante->acudiente_name = $mama->nombre_madre;
			$estudiante->acudiente_email = $mama->email_madre;

			$estudiante->acudiente_estado = $mama->estado_madre;
			$estudiante->ide = $mama->tipo_dni;
			$estudiante->numero_acudiente_expedicion = $mama->dni_madre;
			$estudiante->profesion = $mama->profesion_madre;
			$estudiante->empresa = $mama->nombre_empresa_madre;
			$estudiante->cargo = $mama->cargo_madre;
			$estudiante->telefono = $mama->telefono_madre;
		}
		else if ($padre['responsable'] == 'si') {
			$estudiante->prefijo = 'padre';
			$estudiante->acudiente_name = $padre->nombre_padre;
			$estudiante->acudiente_email = $padre->email_padre;

			$estudiante->acudiente_estado = $padre->estado_padre;
			$estudiante->ide = $padre->tipo_dni;
			$estudiante->numero_acudiente_expedicion = $padre->dni_padre;
			$estudiante->profesion = $padre->profesion_padre;
			$estudiante->empresa = $padre->nombre_empresa_padre;
			$estudiante->cargo = $padre->cargo_padre;
			$estudiante->telefono = $padre->telefono_padre;
		}
		else if ($acudiente['responsable'] == 'si') {
			$estudiante->prefijo = 'acudiente';
			$estudiante->acudiente_name = $acudiente->nombre_acudiente;
			$estudiante->acudiente_email = $acudiente->email_acudiente;

			$estudiante->acudiente_estado = $acudiente->estado_acudiente;
			$estudiante->ide = $acudiente->tipo_dni;
			$estudiante->numero_acudiente_expedicion = $acudiente->dni_acudiente;
			$estudiante->profesion = $acudiente->profesion_acudiente;
			$estudiante->empresa = $acudiente->nombre_empresa_acudiente;
			$estudiante->cargo = $acudiente->cargo_acudiente;
			$estudiante->telefono = $acudiente->telefono_acudiente;
		}
		else {
			$estudiante->prefijo = 'acudiente';
			$estudiante->acudiente_name = $acudiente->nombre_acudiente;
			$estudiante->acudiente_email = $acudiente->email_acudiente;

			$estudiante->acudiente_estado = $acudiente->estado_acudiente;
			$estudiante->ide = $acudiente->tipo_dni;
			$estudiante->numero_acudiente_expedicion = $acudiente->dni_acudiente;
			$estudiante->profesion = $acudiente->profesion_acudiente;
			$estudiante->empresa = $acudiente->nombre_empresa_acudiente;
			$estudiante->cargo = $acudiente->cargo_acudiente;
			$estudiante->telefono = $acudiente->telefono_acudiente;
		}

		return $estudiante;
	}

	public function FiltrarDatos($servicio) {
		$datos = [];
		if($servicio == 'Extracurricular') {
			$datos = Extracurriculares::orderBy('id', 'desc')->get();
		}
		if($servicio == 'Medias tecnicas') {
			$datos = MediasTecnicas::orderBy('id', 'desc')->get();
		}
		if($servicio == 'Certificado') {
			$datos = Certificados::orderBy('id', 'desc')->get();
		}

		if($servicio == 'Nivelacion') {
			$datos = Nivelacion::orderBy('id', 'desc')->get();
		}

		foreach ($datos as $dato) {
			$dato->valor = preg_replace('/[.]/', '', $dato->valor);
		}

		if($servicio == 'Curso') {
			$datos = Cursos::orderBy('id', 'desc')->get();
			foreach ($datos as $dato) {
				$dato->valor = preg_replace('/[.]/', '', $dato->tarifa);
			}
		}

		return $datos;
	}

	public function PagoManual(Request $request) {
		$acudiente = User::where('codigo', $request->codigoUser)->first();
		if($acudiente) {
			$pago = new InformePago;
			$pago->aprobado = 'A';
			$pago->tipo = 'Caja';
			$pago->_tramite = $request->servicio;
			$pago->valor = preg_replace('/[.]/', '', $request->valor_pago);
			$pago->valor_subtotal = preg_replace('/[.]/', '', $request->valor_subtotal);
			$pago->acudiente_id = $acudiente->id;
			$pago->meses = $request->meses;
			$pago->servicio_id = $request->servicio_id;
			$pago->_uid = $request->servicio_id;
			$pago->tipo_pago = $request->tipo_pago;
			$pago->descuento = $request->porcentaje;
			if($request->servicio == 'Certificado') {
				$pago->certificado_estado = 'Pendiente';
				$pago->certificado_motivo = $request->motivo;
			}
			$pago->save();

			if($request->servicio == 'Saldo a favor') {
				$acudiente->saldo = $acudiente->saldo + preg_replace('/[.]/', '', $request->valor_pago);
				$acudiente->save();
			}
			if($request->servicio == 'Matricula') {
				$pago->matricula_estado = 'Activo';
				$pago->save();
				
				$acudiente->pension = $request->pension;
				$acudiente->save();
	
				// Cargo pensiones en forma de cartera inactiva
				$acudiente_grado = $acudiente->grado;
				$semejantes = null;
				if ($acudiente_grado == 'PJ' || $acudiente_grado == 'JD' || $acudiente_grado == 'TR') {
					if ($acudiente_grado == 'PJ') {
						$grado = 'Pre. Jardin';
					}
					if ($acudiente_grado == 'JD') {
						$grado = 'Jardin';
					}
					if ($acudiente_grado == 'TR') {
						$grado = 'Transicion';
					}
					$pension_asignada = Pension::where('desde', $grado)->first();
					$pension_asignada->valor = preg_replace('/[.]/', '', $pension_asignada->valor);
					$semejantes = $pension_asignada;
				} else {
					$pension_asignada = Pension::where('desde', $acudiente_grado)->first();
					$pension_asignada->valor = preg_replace('/[.]/', '', $pension_asignada->valor);
					$semejantes = $pension_asignada;
				}

				$fecha = Carbon::now();
				$mes_actual = $fecha->format('m');
				$periodo1 = false;
				$periodo2 = false;
				if (($mes_actual == 5) || ($mes_actual == 6) || ($mes_actual == 7) || ($mes_actual == 8) || ($mes_actual == 9) || ($mes_actual == 10) || ($mes_actual == 11) || ($mes_actual == 12)) {
					$fecha_proxima = Carbon::now()->addYear();
					$periodo1 = true;
					$periodo2 = true;
				}
				if (($mes_actual == 1) || ($mes_actual == 2) || ($mes_actual == 3) || ($mes_actual == 4) ||
				($mes_actual == 5) || ($mes_actual == 6)) {
					$fecha_proxima = Carbon::now();
					$periodo2 = true;
				}
				$cuotas = $request->pension;
				$total = preg_replace('/[.]/', '', $semejantes->valor) * 10;
				$dosporciento = 0;
				if ($cuotas == 12) {
					$dosporciento = (2 / 100) * $total;
				}
				$valorbase = ($total + $dosporciento) / $cuotas;
				$valor = intval($valorbase);
				for ($i=1; $i <= 12; $i++) { 
					if ($periodo1) {
						if (($i == 1) && ($mes_actual < 9)) {
							$fecha_new = '01-09-' . $fecha->format('Y') . ' 05:00:00';
							$pensiones = new \App\HistoricoCartera;
							$pensiones->servicio = 'Pension';
							$pensiones->valor = $valor;
							$pensiones->otro = 'No';
							$pensiones->acudiente_id = $acudiente->id;
							$pensiones->matricula_id = $pago->id;
							$pensiones->status = 'inactiva';
							$pensiones->created_at = new \DateTime($fecha_new);
							$pensiones->save();
						}
						if (($i == 2) && ($mes_actual < 10)) {
							$fecha_new = '01-10-' . $fecha->format('Y') . ' 05:00:00';
							$pensiones = new \App\HistoricoCartera;
							$pensiones->servicio = 'Pension';
							$pensiones->valor = $valor;
							$pensiones->otro = 'No';
							$pensiones->acudiente_id = $acudiente->id;
							$pensiones->matricula_id = $pago->id;
							$pensiones->status = 'inactiva';
							$pensiones->created_at = new \DateTime($fecha_new);
							$pensiones->save();
						}
						if (($i == 3) && ($mes_actual < 11)) {
							$fecha_new = '01-11-' . $fecha->format('Y') . ' 05:00:00';
							$pensiones = new \App\HistoricoCartera;
							$pensiones->servicio = 'Pension';
							$pensiones->valor = $valor;
							$pensiones->otro = 'No';
							$pensiones->acudiente_id = $acudiente->id;
							$pensiones->matricula_id = $pago->id;
							$pensiones->status = 'inactiva';
							$pensiones->created_at = new \DateTime($fecha_new);
							$pensiones->save();
						}
						if (($i == 4) && ($mes_actual < 12)) {
							$fecha_new = '01-12-' . $fecha->format('Y') . ' 05:00:00';
							$pensiones = new \App\HistoricoCartera;
							$pensiones->servicio = 'Pension';
							$pensiones->valor = $valor;
							$pensiones->otro = 'No';
							$pensiones->acudiente_id = $acudiente->id;
							$pensiones->matricula_id = $pago->id;
							$pensiones->status = 'inactiva';
							$pensiones->created_at = new \DateTime($fecha_new);
							$pensiones->save();
						}
					}
					if ($periodo2) {
						if (($mes_actual == 5) || ($mes_actual == 6) || ($mes_actual == 7) || ($mes_actual == 8) || ($mes_actual == 9) || ($mes_actual == 10) || ($mes_actual == 11) || ($mes_actual == 12)) {
							if (($i == 5)) {
								$fecha_new = '01-01-' . $fecha_proxima->format('Y') . ' 05:00:00';
								$pensiones = new \App\HistoricoCartera;
								$pensiones->servicio = 'Pension';
								$pensiones->valor = $valor;
								$pensiones->otro = 'No';
								$pensiones->acudiente_id = $acudiente->id;
								$pensiones->matricula_id = $pago->id;
								$pensiones->status = 'inactiva';
								$pensiones->created_at = new \DateTime($fecha_new);
								$pensiones->save();
							}
							if (($i == 6)) {
								$fecha_new = '01-02-' . $fecha_proxima->format('Y') . ' 05:00:00';
								$pensiones = new \App\HistoricoCartera;
								$pensiones->servicio = 'Pension';
								$pensiones->valor = $valor;
								$pensiones->otro = 'No';
								$pensiones->acudiente_id = $acudiente->id;
								$pensiones->matricula_id = $pago->id;
								$pensiones->status = 'inactiva';
								$pensiones->created_at = new \DateTime($fecha_new);
								$pensiones->save();
							}
							if (($i == 7)) {
								$fecha_new = '01-03-' . $fecha_proxima->format('Y') . ' 05:00:00';
								$pensiones = new \App\HistoricoCartera;
								$pensiones->servicio = 'Pension';
								$pensiones->valor = $valor;
								$pensiones->otro = 'No';
								$pensiones->acudiente_id = $acudiente->id;
								$pensiones->matricula_id = $pago->id;
								$pensiones->status = 'inactiva';
								$pensiones->created_at = new \DateTime($fecha_new);
								$pensiones->save();
							}
							if (($i == 8)) {
								$fecha_new = '01-04-' . $fecha_proxima->format('Y') . ' 05:00:00';
								$pensiones = new \App\HistoricoCartera;
								$pensiones->servicio = 'Pension';
								$pensiones->valor = $valor;
								$pensiones->otro = 'No';
								$pensiones->acudiente_id = $acudiente->id;
								$pensiones->matricula_id = $pago->id;
								$pensiones->status = 'inactiva';
								$pensiones->created_at = new \DateTime($fecha_new);
								$pensiones->save();
							}
							if (($i == 9)) {
								$fecha_new = '01-05-' . $fecha_proxima->format('Y') . ' 05:00:00';
								$pensiones = new \App\HistoricoCartera;
								$pensiones->servicio = 'Pension';
								$pensiones->valor = $valor;
								$pensiones->otro = 'No';
								$pensiones->acudiente_id = $acudiente->id;
								$pensiones->matricula_id = $pago->id;
								$pensiones->status = 'inactiva';
								$pensiones->created_at = new \DateTime($fecha_new);
								$pensiones->save();
							}
							if (($i == 10)) {
								$fecha_new = '01-06-' . $fecha_proxima->format('Y') . ' 05:00:00';
								$pensiones = new \App\HistoricoCartera;
								$pensiones->servicio = 'Pension';
								$pensiones->valor = $valor;
								$pensiones->otro = 'No';
								$pensiones->acudiente_id = $acudiente->id;
								$pensiones->matricula_id = $pago->id;
								$pensiones->status = 'inactiva';
								$pensiones->created_at = new \DateTime($fecha_new);
								$pensiones->save();
							}
							if (($i == 11) && ($cuotas == 12)) {
								$fecha_new = '01-07-' . $fecha_proxima->format('Y') . ' 05:00:00';
								$pensiones = new \App\HistoricoCartera;
								$pensiones->servicio = 'Pension';
								$pensiones->valor = $valor;
								$pensiones->otro = 'No';
								$pensiones->acudiente_id = $acudiente->id;
								$pensiones->matricula_id = $pago->id;
								$pensiones->status = 'inactiva';
								$pensiones->created_at = new \DateTime($fecha_new);
								$pensiones->save();
							}
							if (($i == 12) && ($cuotas == 12)) {
								$fecha_new = '01-08-' . $fecha_proxima->format('Y') . ' 05:00:00';
								$pensiones = new \App\HistoricoCartera;
								$pensiones->servicio = 'Pension';
								$pensiones->valor = $valor;
								$pensiones->otro = 'No';
								$pensiones->acudiente_id = $acudiente->id;
								$pensiones->matricula_id = $pago->id;
								$pensiones->status = 'inactiva';
								$pensiones->created_at = new \DateTime($fecha_new);
								$pensiones->save();
							}
						} else {
							if (($i == 5) && ($mes_actual < 1)) {
								$fecha_new = '01-01-' . $fecha_proxima->format('Y') . ' 05:00:00';
								$pensiones = new \App\HistoricoCartera;
								$pensiones->servicio = 'Pension';
								$pensiones->valor = $valor;
								$pensiones->otro = 'No';
								$pensiones->acudiente_id = $acudiente->id;
								$pensiones->matricula_id = $pago->id;
								$pensiones->status = 'inactiva';
								$pensiones->created_at = new \DateTime($fecha_new);
								$pensiones->save();
							}
							if (($i == 6) && ($mes_actual < 2)) {
								$fecha_new = '01-02-' . $fecha_proxima->format('Y') . ' 05:00:00';
								$pensiones = new \App\HistoricoCartera;
								$pensiones->servicio = 'Pension';
								$pensiones->valor = $valor;
								$pensiones->otro = 'No';
								$pensiones->acudiente_id = $acudiente->id;
								$pensiones->matricula_id = $pago->id;
								$pensiones->status = 'inactiva';
								$pensiones->created_at = new \DateTime($fecha_new);
								$pensiones->save();
							}
							if (($i == 7) && ($mes_actual < 3)) {
								$fecha_new = '01-03-' . $fecha_proxima->format('Y') . ' 05:00:00';
								$pensiones = new \App\HistoricoCartera;
								$pensiones->servicio = 'Pension';
								$pensiones->valor = $valor;
								$pensiones->otro = 'No';
								$pensiones->acudiente_id = $acudiente->id;
								$pensiones->matricula_id = $pago->id;
								$pensiones->status = 'inactiva';
								$pensiones->created_at = new \DateTime($fecha_new);
								$pensiones->save();
							}
							if (($i == 8) && ($mes_actual < 4)) {
								$fecha_new = '01-04-' . $fecha_proxima->format('Y') . ' 05:00:00';
								$pensiones = new \App\HistoricoCartera;
								$pensiones->servicio = 'Pension';
								$pensiones->valor = $valor;
								$pensiones->otro = 'No';
								$pensiones->acudiente_id = $acudiente->id;
								$pensiones->matricula_id = $pago->id;
								$pensiones->status = 'inactiva';
								$pensiones->created_at = new \DateTime($fecha_new);
								$pensiones->save();
							}
							if (($i == 9) && ($mes_actual < 5)) {
								$fecha_new = '01-05-' . $fecha_proxima->format('Y') . ' 05:00:00';
								$pensiones = new \App\HistoricoCartera;
								$pensiones->servicio = 'Pension';
								$pensiones->valor = $valor;
								$pensiones->otro = 'No';
								$pensiones->acudiente_id = $acudiente->id;
								$pensiones->matricula_id = $pago->id;
								$pensiones->status = 'inactiva';
								$pensiones->created_at = new \DateTime($fecha_new);
								$pensiones->save();
							}
							if (($i == 10) && ($mes_actual < 6)) {
								$fecha_new = '01-06-' . $fecha_proxima->format('Y') . ' 05:00:00';
								$pensiones = new \App\HistoricoCartera;
								$pensiones->servicio = 'Pension';
								$pensiones->valor = $valor;
								$pensiones->otro = 'No';
								$pensiones->acudiente_id = $acudiente->id;
								$pensiones->matricula_id = $pago->id;
								$pensiones->status = 'inactiva';
								$pensiones->created_at = new \DateTime($fecha_new);
								$pensiones->save();
							}
							if (($i == 11) && ($mes_actual < 7) && ($cuotas == 12)) {
								$fecha_new = '01-07-' . $fecha_proxima->format('Y') . ' 05:00:00';
								$pensiones = new \App\HistoricoCartera;
								$pensiones->servicio = 'Pension';
								$pensiones->valor = $valor;
								$pensiones->otro = 'No';
								$pensiones->acudiente_id = $acudiente->id;
								$pensiones->matricula_id = $pago->id;
								$pensiones->status = 'inactiva';
								$pensiones->created_at = new \DateTime($fecha_new);
								$pensiones->save();
							}
							if (($i == 12) && ($mes_actual < 8) && ($cuotas == 12)) {
								$fecha_new = '01-08-' . $fecha_proxima->format('Y') . ' 05:00:00';
								$pensiones = new \App\HistoricoCartera;
								$pensiones->servicio = 'Pension';
								$pensiones->valor = $valor;
								$pensiones->otro = 'No';
								$pensiones->acudiente_id = $acudiente->id;
								$pensiones->matricula_id = $pago->id;
								$pensiones->status = 'inactiva';
								$pensiones->created_at = new \DateTime($fecha_new);
								$pensiones->save();
							}
						}
					}
				}
	
				$activarpension = HistoricoCartera::where('servicio', 'Pension')->where('acudiente_id', $acudiente->id)
				->where('status', 'inactiva')->first();
				$activarpension->status = 'preactiva';
				$activarpension->save();
			}
		} else {
			return 'Bad';
		}
	}

	public function PagoManualDetalles(Request $request) {
		$pago = new InformePago;
		$pago->aprobado = 'A';
		$pago->tipo = 'Caja';
		$pago->_tramite = $request->_tramite;
		$pago->valor = preg_replace('/[.]/', '', $request->valor);
		$pago->valor_subtotal = preg_replace('/[.]/', '', $request->valor_subtotal);
		$pago->acudiente_id = $request->acudiente_id;
		$pago->_uid = $request->servicio_id;
		$pago->servicio_id = $request->servicio_id;
		$pago->tipo_pago = $request->tipo_pago;
		$pago->descuento = $request->descuento;
		$pago->cupon = $request->cupon;
		$pago->save();
		
		$cartera = HistoricoCartera::find($request->servicio_id);
		if ($cartera->matricula_id) {
			$cartera->status = 'Pagada';
			$cartera->save();

			$activarpension = HistoricoCartera::where('servicio', 'Pension')->where('acudiente_id', $request->acudiente_id)
			->where('status', 'inactiva')->first();
			if ($activarpension) {
				$activarpension->status = 'preactiva';
				$activarpension->save();
			}
		} else {
			$cartera->delete();
		}

		return 'Good';
	}

	public function PagoMatricula(Request $request) {
		$estudiante = User::where('codigo', $request->codigo)->first();  // get estudiantes
		if(!$estudiante){
			return 'Bad';
		}

		$acudiente_grado = $request->grado;
		$matricula_activa = InformePago::orderBy('id', 'desc')->where('aprobado', 'A')->where('acudiente_id', $estudiante->id)
		->where('_tramite', 'Matricula')->where('matricula_estado', 'Activo')->first();
		$semejantes = null;
		if ($acudiente_grado == 'PJ' || $acudiente_grado == 'JD' || $acudiente_grado == 'TR') {
			if ($acudiente_grado == 'PJ') {
				$grado = 'Pre. Jardin';
			}
			if ($acudiente_grado == 'JD') {
				$grado = 'Jardin';
			}
			if ($acudiente_grado == 'TR') {
				$grado = 'Transicion';
			}
			$matricula = Matriculas::where('desde', $grado)->first();
			$matricula->valor_ex = preg_replace('/[.]/', '', $matricula->valor_ex);
			$matricula->valor_or = preg_replace('/[.]/', '', $matricula->valor_or);
			$semejantes = $matricula;
		} else {
			$matricula = Matriculas::where('desde', $acudiente_grado)->first();
			$matricula->valor_ex = preg_replace('/[.]/', '', $matricula->valor_ex);
			$matricula->valor_or = preg_replace('/[.]/', '', $matricula->valor_or);
			$semejantes = $matricula;
		}

		return ["matricula" => $semejantes, "matricula_activa" => $matricula_activa];
	}

	public function PagoPension(Request $request) {
		$acudiente_grado = $request->grado;
		$semejantes = null;

		if ($acudiente_grado == 'PJ' || $acudiente_grado == 'JD' || $acudiente_grado == 'TR') {
			if ($acudiente_grado == 'PJ') {
				$grado = 'Pre. Jardin';
			}
			if ($acudiente_grado == 'JD') {
				$grado = 'Jardin';
			}
			if ($acudiente_grado == 'TR') {
				$grado = 'Transicion';
			}
			$pension_asignada = Pension::where('desde', $grado)->first();
			$pension_asignada->valor = preg_replace('/[.]/', '', $pension_asignada->valor);
			$semejantes = $pension_asignada;
		} else {
			$pension_asignada = Pension::where('desde', $acudiente_grado)->first();
			$pension_asignada->valor = preg_replace('/[.]/', '', $pension_asignada->valor);
			$semejantes = $pension_asignada;
		}

		return $semejantes;
	}

	public function Pagado(Request $request) {
		$informe = InformePago::create($request->all());
		if($informe->aprobado == 'A') {
			$acudiente = User::find($informe->acudiente_id);
			$acudiente->saldo = $acudiente->saldo + $informe->valor;
			$acudiente->save();
		}
		
		return $request;
	}

	public function SaldoPagoAcudiente(Request $request) {
		$informe = InformePago::create($request->all());
		$informe->tipo = 'bolsillo';
		$informe->save();

		$acudiente = User::find($informe->acudiente_id);
		$acudiente->saldo = preg_replace('/[.]/', '', $acudiente->saldo) - $informe->valor;
		$acudiente->save();

		if ($request->_tramite == 'Pension Multiple') {
			$pensiones = HistoricoCartera::where('servicio', 'Pension')->where('acudiente_id', Auth::User()->id)
			->where(function($query) {
				$query->where('status', '=', "inactiva")
					->orWhere('status', '=', "preactiva")
					->orWhere('status', '=', "activa");
			})->limit($request->meses)->get();

			foreach ($pensiones as $pension) {
				$pension->status = "Pagada";
				$pension->save();
				setlocale(LC_ALL, 'es_ES');
				$fecha = Carbon::parse($pension->created_at);
				$fecha->format("F"); // Inglés.
				$pension->created_f = $fecha->formatLocalized('%B');// mes en idioma español
			}

			$informe->factura_pdf = '/facturas/Factura-'.$informe->id.'-'.Auth::User()->codigo.'.pdf';
			$informe->save();
			
			$informe->name_estudiante = Auth::User()->name_estudiante;
			$informe->codigo = Auth::User()->codigo;
			$informe->email = Auth::User()->email;
			$formatterES = new NumberFormatter("es", NumberFormatter::SPELLOUT);
			$pdf = \PDF::loadView('pdf.facturaPensionMultiple', [ 
				'data' => $informe,
				'pagos' => $pensiones,
				'precio' => $formatterES->format($informe->valor)
			])->save(public_path().'/facturas/Factura-'.$informe->id.'-'.Auth::User()->codigo.'.pdf');

			$activarpension = HistoricoCartera::where('servicio', 'Pension')->where('acudiente_id', Auth::User()->id)
			->where('status', 'inactiva')->first();
			$pensionesActivas = HistoricoCartera::where('servicio', 'Pension')->where('acudiente_id', Auth::User()->id)
			->where('status', 'activa')->count();
			if ($activarpension) {
				if ($pensionesActivas == 0) {
					$activarpension->status = 'preactiva';
					$activarpension->save();
				}
			}
		}
		
		if($informe->_tramite == 'Certificado') {
			$informe->certificado_estado = 'Pendiente';
			$informe->save();
		}

		if($informe->_tramite == 'Matricula') {
			$informe->matricula_estado = 'Activo';
			$informe->save();

			// Cargo pensiones en forma de cartera inactiva
			$acudiente_grado = Auth::User()->grado;
			$semejantes = null;
			if ($acudiente_grado == 'PJ' || $acudiente_grado == 'JD' || $acudiente_grado == 'TR') {
				if ($acudiente_grado == 'PJ') {
					$grado = 'Pre. Jardin';
				}
				if ($acudiente_grado == 'JD') {
					$grado = 'Jardin';
				}
				if ($acudiente_grado == 'TR') {
					$grado = 'Transicion';
				}
				$pension_asignada = Pension::where('desde', $grado)->first();
				$pension_asignada->valor = preg_replace('/[.]/', '', $pension_asignada->valor);
				$semejantes = $pension_asignada;
			} else {
				$pension_asignada = Pension::where('desde', $acudiente_grado)->first();
				$pension_asignada->valor = preg_replace('/[.]/', '', $pension_asignada->valor);
				$semejantes = $pension_asignada;
			}
			
			$fecha = Carbon::now();
			$mes_actual = $fecha->format('m');
			$periodo1 = false;
			$periodo2 = false;
			if (($mes_actual == 5) || ($mes_actual == 6) || ($mes_actual == 7) || ($mes_actual == 8) || ($mes_actual == 9) || ($mes_actual == 10) || ($mes_actual == 11) || ($mes_actual == 12)) {
				$fecha_proxima = Carbon::now()->addYear();
				$periodo1 = true;
				$periodo2 = true;
			}
			if (($mes_actual == 1) || ($mes_actual == 2) || ($mes_actual == 3) || ($mes_actual == 4)) {
				$fecha_proxima = Carbon::now();
				$periodo2 = true;
			}
			$cuotas = Auth::User()->pension;
			$total = preg_replace('/[.]/', '', $semejantes->valor) * 10;
			$dosporciento = 0;
			if ($cuotas == 12) {
				$dosporciento = (2 / 100) * $total;
			}
			
			$valorbase = ($total + $dosporciento) / $cuotas;
			$valor = intval($valorbase);
			for ($i=1; $i <= 12; $i++) { 
				if ($periodo1) {
					if (($i == 1) && ($mes_actual < 9)) {
						$fecha_new = '01-09-' . $fecha->format('Y') . ' 05:00:00';
						$pensiones = new \App\HistoricoCartera;
						$pensiones->servicio = 'Pension';
						$pensiones->valor = $valor;
						$pensiones->otro = 'No';
						$pensiones->acudiente_id = Auth::User()->id;
						$pensiones->matricula_id = $informe->id;
						$pensiones->status = 'inactiva';
						$pensiones->created_at = new \DateTime($fecha_new);
						$pensiones->save();
					}
					if (($i == 2) && ($mes_actual < 10)) {
						$fecha_new = '01-10-' . $fecha->format('Y') . ' 05:00:00';
						$pensiones = new \App\HistoricoCartera;
						$pensiones->servicio = 'Pension';
						$pensiones->valor = $valor;
						$pensiones->otro = 'No';
						$pensiones->acudiente_id = Auth::User()->id;
						$pensiones->matricula_id = $informe->id;
						$pensiones->status = 'inactiva';
						$pensiones->created_at = new \DateTime($fecha_new);
						$pensiones->save();
					}
					if (($i == 3) && ($mes_actual < 11)) {
						$fecha_new = '01-11-' . $fecha->format('Y') . ' 05:00:00';
						$pensiones = new \App\HistoricoCartera;
						$pensiones->servicio = 'Pension';
						$pensiones->valor = $valor;
						$pensiones->otro = 'No';
						$pensiones->acudiente_id = Auth::User()->id;
						$pensiones->matricula_id = $informe->id;
						$pensiones->status = 'inactiva';
						$pensiones->created_at = new \DateTime($fecha_new);
						$pensiones->save();
					}
					if (($i == 4) && ($mes_actual < 12)) {
						$fecha_new = '01-12-' . $fecha->format('Y') . ' 05:00:00';
						$pensiones = new \App\HistoricoCartera;
						$pensiones->servicio = 'Pension';
						$pensiones->valor = $valor;
						$pensiones->otro = 'No';
						$pensiones->acudiente_id = Auth::User()->id;
						$pensiones->matricula_id = $informe->id;
						$pensiones->status = 'inactiva';
						$pensiones->created_at = new \DateTime($fecha_new);
						$pensiones->save();
					}
				}
				if ($periodo2) {
					if (($mes_actual == 5) || ($mes_actual == 6) || ($mes_actual == 7) || ($mes_actual == 8) || ($mes_actual == 9) || ($mes_actual == 10) || ($mes_actual == 11) || ($mes_actual == 12)) {
						if (($i == 5)) {
							$fecha_new = '01-01-' . $fecha_proxima->format('Y') . ' 05:00:00';
							$pensiones = new \App\HistoricoCartera;
							$pensiones->servicio = 'Pension';
							$pensiones->valor = $valor;
							$pensiones->otro = 'No';
							$pensiones->acudiente_id = Auth::User()->id;
							$pensiones->matricula_id = $informe->id;
							$pensiones->status = 'inactiva';
							$pensiones->created_at = new \DateTime($fecha_new);
							$pensiones->save();
						}
						if (($i == 6)) {
							$fecha_new = '01-02-' . $fecha_proxima->format('Y') . ' 05:00:00';
							$pensiones = new \App\HistoricoCartera;
							$pensiones->servicio = 'Pension';
							$pensiones->valor = $valor;
							$pensiones->otro = 'No';
							$pensiones->acudiente_id = Auth::User()->id;
							$pensiones->matricula_id = $informe->id;
							$pensiones->status = 'inactiva';
							$pensiones->created_at = new \DateTime($fecha_new);
							$pensiones->save();
						}
						if (($i == 7)) {
							$fecha_new = '01-03-' . $fecha_proxima->format('Y') . ' 05:00:00';
							$pensiones = new \App\HistoricoCartera;
							$pensiones->servicio = 'Pension';
							$pensiones->valor = $valor;
							$pensiones->otro = 'No';
							$pensiones->acudiente_id = Auth::User()->id;
							$pensiones->matricula_id = $informe->id;
							$pensiones->status = 'inactiva';
							$pensiones->created_at = new \DateTime($fecha_new);
							$pensiones->save();
						}
						if (($i == 8)) {
							$fecha_new = '01-04-' . $fecha_proxima->format('Y') . ' 05:00:00';
							$pensiones = new \App\HistoricoCartera;
							$pensiones->servicio = 'Pension';
							$pensiones->valor = $valor;
							$pensiones->otro = 'No';
							$pensiones->acudiente_id = Auth::User()->id;
							$pensiones->matricula_id = $informe->id;
							$pensiones->status = 'inactiva';
							$pensiones->created_at = new \DateTime($fecha_new);
							$pensiones->save();
						}
						if (($i == 9)) {
							$fecha_new = '01-05-' . $fecha_proxima->format('Y') . ' 05:00:00';
							$pensiones = new \App\HistoricoCartera;
							$pensiones->servicio = 'Pension';
							$pensiones->valor = $valor;
							$pensiones->otro = 'No';
							$pensiones->acudiente_id = Auth::User()->id;
							$pensiones->matricula_id = $informe->id;
							$pensiones->status = 'inactiva';
							$pensiones->created_at = new \DateTime($fecha_new);
							$pensiones->save();
						}
						if (($i == 10)) {
							$fecha_new = '01-06-' . $fecha_proxima->format('Y') . ' 05:00:00';
							$pensiones = new \App\HistoricoCartera;
							$pensiones->servicio = 'Pension';
							$pensiones->valor = $valor;
							$pensiones->otro = 'No';
							$pensiones->acudiente_id = Auth::User()->id;
							$pensiones->matricula_id = $informe->id;
							$pensiones->status = 'inactiva';
							$pensiones->created_at = new \DateTime($fecha_new);
							$pensiones->save();
						}
						if (($i == 11) && ($cuotas == 12)) {
							$fecha_new = '01-07-' . $fecha_proxima->format('Y') . ' 05:00:00';
							$pensiones = new \App\HistoricoCartera;
							$pensiones->servicio = 'Pension';
							$pensiones->valor = $valor;
							$pensiones->otro = 'No';
							$pensiones->acudiente_id = Auth::User()->id;
							$pensiones->matricula_id = $informe->id;
							$pensiones->status = 'inactiva';
							$pensiones->created_at = new \DateTime($fecha_new);
							$pensiones->save();
						}
						if (($i == 12) && ($cuotas == 12)) {
							$fecha_new = '01-08-' . $fecha_proxima->format('Y') . ' 05:00:00';
							$pensiones = new \App\HistoricoCartera;
							$pensiones->servicio = 'Pension';
							$pensiones->valor = $valor;
							$pensiones->otro = 'No';
							$pensiones->acudiente_id = Auth::User()->id;
							$pensiones->matricula_id = $informe->id;
							$pensiones->status = 'inactiva';
							$pensiones->created_at = new \DateTime($fecha_new);
							$pensiones->save();
						}
					} else {
						if (($i == 5) && ($mes_actual < 1)) {
							$fecha_new = '01-01-' . $fecha_proxima->format('Y') . ' 05:00:00';
							$pensiones = new \App\HistoricoCartera;
							$pensiones->servicio = 'Pension';
							$pensiones->valor = $valor;
							$pensiones->otro = 'No';
							$pensiones->acudiente_id = Auth::User()->id;
							$pensiones->matricula_id = $informe->id;
							$pensiones->status = 'inactiva';
							$pensiones->created_at = new \DateTime($fecha_new);
							$pensiones->save();
						}
						if (($i == 6) && ($mes_actual < 2)) {
							$fecha_new = '01-02-' . $fecha_proxima->format('Y') . ' 05:00:00';
							$pensiones = new \App\HistoricoCartera;
							$pensiones->servicio = 'Pension';
							$pensiones->valor = $valor;
							$pensiones->otro = 'No';
							$pensiones->acudiente_id = Auth::User()->id;
							$pensiones->matricula_id = $informe->id;
							$pensiones->status = 'inactiva';
							$pensiones->created_at = new \DateTime($fecha_new);
							$pensiones->save();
						}
						if (($i == 7) && ($mes_actual < 3)) {
							$fecha_new = '01-03-' . $fecha_proxima->format('Y') . ' 05:00:00';
							$pensiones = new \App\HistoricoCartera;
							$pensiones->servicio = 'Pension';
							$pensiones->valor = $valor;
							$pensiones->otro = 'No';
							$pensiones->acudiente_id = Auth::User()->id;
							$pensiones->matricula_id = $informe->id;
							$pensiones->status = 'inactiva';
							$pensiones->created_at = new \DateTime($fecha_new);
							$pensiones->save();
						}
						if (($i == 8) && ($mes_actual < 4)) {
							$fecha_new = '01-04-' . $fecha_proxima->format('Y') . ' 05:00:00';
							$pensiones = new \App\HistoricoCartera;
							$pensiones->servicio = 'Pension';
							$pensiones->valor = $valor;
							$pensiones->otro = 'No';
							$pensiones->acudiente_id = Auth::User()->id;
							$pensiones->matricula_id = $informe->id;
							$pensiones->status = 'inactiva';
							$pensiones->created_at = new \DateTime($fecha_new);
							$pensiones->save();
						}
						if (($i == 9) && ($mes_actual < 5)) {
							$fecha_new = '01-05-' . $fecha_proxima->format('Y') . ' 05:00:00';
							$pensiones = new \App\HistoricoCartera;
							$pensiones->servicio = 'Pension';
							$pensiones->valor = $valor;
							$pensiones->otro = 'No';
							$pensiones->acudiente_id = Auth::User()->id;
							$pensiones->matricula_id = $informe->id;
							$pensiones->status = 'inactiva';
							$pensiones->created_at = new \DateTime($fecha_new);
							$pensiones->save();
						}
						if (($i == 10) && ($mes_actual < 6)) {
							$fecha_new = '01-06-' . $fecha_proxima->format('Y') . ' 05:00:00';
							$pensiones = new \App\HistoricoCartera;
							$pensiones->servicio = 'Pension';
							$pensiones->valor = $valor;
							$pensiones->otro = 'No';
							$pensiones->acudiente_id = Auth::User()->id;
							$pensiones->matricula_id = $informe->id;
							$pensiones->status = 'inactiva';
							$pensiones->created_at = new \DateTime($fecha_new);
							$pensiones->save();
						}
						if (($i == 11) && ($mes_actual < 7) && ($cuotas == 12)) {
							$fecha_new = '01-07-' . $fecha_proxima->format('Y') . ' 05:00:00';
							$pensiones = new \App\HistoricoCartera;
							$pensiones->servicio = 'Pension';
							$pensiones->valor = $valor;
							$pensiones->otro = 'No';
							$pensiones->acudiente_id = Auth::User()->id;
							$pensiones->matricula_id = $informe->id;
							$pensiones->status = 'inactiva';
							$pensiones->created_at = new \DateTime($fecha_new);
							$pensiones->save();
						}
						if (($i == 12) && ($mes_actual < 8) && ($cuotas == 12)) {
							$fecha_new = '01-08-' . $fecha_proxima->format('Y') . ' 05:00:00';
							$pensiones = new \App\HistoricoCartera;
							$pensiones->servicio = 'Pension';
							$pensiones->valor = $valor;
							$pensiones->otro = 'No';
							$pensiones->acudiente_id = Auth::User()->id;
							$pensiones->matricula_id = $informe->id;
							$pensiones->status = 'inactiva';
							$pensiones->created_at = new \DateTime($fecha_new);
							$pensiones->save();
						}
					}
				}
			}

			$activarpension = HistoricoCartera::where('servicio', 'Pension')->where('acudiente_id', Auth::User()->id)
			->where('status', 'inactiva')->first();
			$activarpension->status = 'preactiva';
			$activarpension->save();
		}

		if($informe->_tramite == 'Acuerdo') {
			$cuota = AcuerdoCuotas::Find($request->_uid);
			$cuota->estatus = 'Pagado';
			$cuota->save();

			$cuotas = AcuerdoCuotas::where("acuerdo_id", $cuota->acuerdo_id)->where("estatus", "Pendiente")->count();
			if ($cuotas == 0) {
				$acuerdo = AcuerdoPago::Find($cuota->acuerdo_id);
				$acuerdo->finalizado = 'Cumplido';
            	$acuerdo->save();
			}
		}

		if($request->opcion == 'Cartera') {
			$cartera = HistoricoCartera::find($request->_uid);
			$cartera->status = 'Pagada';
			$cartera->save();
		}

		if($request->opcion == 'CarteraInactiva') {
			$cartera = HistoricoCartera::find($request->_uid);
			$cartera->status = 'Pagada';
			$cartera->save();

			$activarpension = HistoricoCartera::where('servicio', 'Pension')->where('acudiente_id', Auth::User()->id)
			->where('status', 'inactiva')->first();
			$pensionesActivas = HistoricoCartera::where('servicio', 'Pension')->where('acudiente_id', Auth::User()->id)
			->where('status', 'activa')->count();
			if ($activarpension) {
				if ($pensionesActivas == 0) {
					$activarpension->status = 'preactiva';
					$activarpension->save();
				}
			}
		}
	}
}
