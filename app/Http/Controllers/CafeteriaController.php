<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cafeteria;
use App\InformePago;

class CafeteriaController extends Controller {
	public function ListManual(Request $request) {
		$query = Cafeteria::orderBy('id', 'desc')->get();
		return $query;
	}

	public function List(Request $request) {
		$columns = [ 'id' ];

		$length = $request->input('length');
		$column = $request->input('column'); //Index
		$dir = $request->input('dir');
		$searchValue = $request->input('search');

		$query = Cafeteria::orderBy($columns[$column], $dir);

		if($searchValue) {
			$query->where(function($query) use ($searchValue) {
				$query->where('descripcion', 'LIKE', '%' . $searchValue . '%')
				->orWhere('pago', 'LIKE', '%' . $searchValue . '%')
				->orWhere('valor', 'LIKE', '%' . $searchValue . '%');
			});
		}

		$projects = $query->paginate($length);
		return ['data' => $projects, 'draw' => $request->input('draw')];
	}

	public function Save(Request $request) {
		$this->validate($request, [
			'descripcion' => 'required',
			'pago' => 'required',
			'valor' => 'required'
		]);

		Cafeteria::create($request->all());
	}

	public function Update(Request $request) {
		$this->validate($request, [
			'id' => 'required'
		]);

		Cafeteria::find($request->id)->update($request->all());
	}

	public function Delete($id) {
		Cafeteria::find($id)->delete();
	}

	public function Pagado(Request $request) {
		$informe = InformePago::create($request->all());
		return $request;
	}
}
