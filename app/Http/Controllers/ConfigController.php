<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Config;
use App\Certificados;

class ConfigController extends Controller {
	public function ListCertificados() {
		$certificados = Certificados::where('status', 'Publico')->get();
		return $certificados;
	}

    public function PrecioIngreso() {
		$config = Config::first();
		$config->precio_ingreso = preg_replace('/[.]/', '', $config->precio_ingreso);
		return $config;
	}

	public function PrecioUpdate(Request $request) {
		$config = Config::first();
		$config->precio_ingreso = $request->precio;
		$config->save();
	}
}
