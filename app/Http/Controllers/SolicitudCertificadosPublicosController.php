<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SolicitudCertificadosPublicos;
use App\InformePago;

class SolicitudCertificadosPublicosController extends Controller {
    public function List(Request $request) {
		$columns = [ 'id' ];

		$length = $request->input('length');
		$column = $request->input('column'); //Index
		$dir = $request->input('dir');
		$searchValue = $request->input('search');

		$query = SolicitudCertificadosPublicos::orderBy($columns[$column], $dir)
        ->join('certificados', 'solicitud_certificados_publicos.tipo_documento', '=', 'certificados.id')
        ->select('solicitud_certificados_publicos.*', 'certificados.concepto as tipo_documento_name');

		if($searchValue) {
			$query->where(function($query) use ($searchValue) {
				$query->where('solicitud_certificados_publicos.nombres_estudiante', 'LIKE', '%' . $searchValue . '%')
				->orWhere('solicitud_certificados_publicos.apellidos_estudiante', 'LIKE', '%' . $searchValue . '%')
				->orWhere('solicitud_certificados_publicos.telefono_estudiante', 'LIKE', '%' . $searchValue . '%')
				->orWhere('solicitud_certificados_publicos.correo_estudiante', 'LIKE', '%' . $searchValue . '%')
				->orWhere('solicitud_certificados_publicos.ci_estudiante', 'LIKE', '%' . $searchValue . '%')
				->orWhere('certificados.concepto', 'LIKE', '%' . $searchValue . '%')
				->orWhere('solicitud_certificados_publicos.cantidad', 'LIKE', '%' . $searchValue . '%');
			});
		}

		$projects = $query->paginate($length);
		return ['data' => $projects, 'draw' => $request->input('draw')];
	}

    public function Solictar(Request $request) {
        $this->validate($request, [
			'nombres' => 'required',
			'apellidos' => 'required',
			'tipo_doc' => 'required',
			'numero' => 'required',
			'direccion' => 'required',
			'ciudad' => 'required',
            'telefono' => 'required',
			'correo' => 'required',
			'responsabilidad_fiscal' => 'required',
			'actividad_economica' => 'required',
			'nombres_estudiante' => 'required',
			'apellidos_estudiante' => 'required',
			'telefono_estudiante' => 'required',
            'correo_estudiante' => 'required',
			'tipo_doc_estudiente' => 'required',
			'numero_estudiente' => 'required',
			'tipo_documento' => 'required',
			'cantidad' => 'required',
            'año_promocion' => 'required',
			'motivo' => 'required',
			'datos_certificacion' => 'required',
            'canal_envio' => 'required',
		]);

        $codigounico = SolicitudCertificadosPublicos::where('numero', $request->numero)->count();

		$solicitud = new SolicitudCertificadosPublicos;
        $solicitud->nombres = $request->nombres;
        $solicitud->apellidos = $request->apellidos;
        $solicitud->tipo_doc = $request->tipo_doc;
        $solicitud->numero = $request->numero;
        $solicitud->direccion = $request->direccion;
        $solicitud->ciudad = $request->ciudad;
        $solicitud->telefono = $request->telefono;
        $solicitud->correo = $request->correo;
        $solicitud->responsabilidad_fiscal = $request->responsabilidad_fiscal;
        $solicitud->actividad_economica = $request->actividad_economica;
        $solicitud->otro_cual = $request->otro_cual;
        $solicitud->nombres_estudiante = $request->nombres_estudiante;
        $solicitud->apellidos_estudiante = $request->apellidos_estudiante;
        $solicitud->tipo_doc_estudiente = $request->tipo_doc_estudiente;
        $solicitud->numero_estudiente = $request->numero_estudiente;
        $solicitud->telefono_estudiante = $request->telefono_estudiante;
        $solicitud->correo_estudiante = $request->correo_estudiante;
        $solicitud->tipo_documento = $request->tipo_documento;
        $solicitud->cantidad = $request->cantidad;
        $solicitud->año_promocion = $request->año_promocion;
        $solicitud->motivo = $request->motivo;
        $solicitud->datos_certificacion = $request->datos_certificacion;
        $solicitud->canal_envio = $request->canal_envio;
        $solicitud->codigounico = $request->numero . "_" . ($codigounico + 1);
        $solicitud->save();

		return $solicitud->codigounico;
	}

    public function SolictarUpdate(Request $request) {
		$solicitud = SolicitudCertificadosPublicos::find($request->id);
        $solicitud->valor_pagar = $request->valor_pagar;
        $solicitud->status = $request->status;
        $solicitud->save();

		return "Good";
	}

    public function PagadoPresencial(Request $request) {
		$solicitud = SolicitudCertificadosPublicos::find($request->id);
        $solicitud->status = "Pagado";
        $solicitud->save();

        $pago = new InformePago;
		$pago->aprobado = 'A';
		$pago->tipo = 'Caja';
		$pago->_tramite = "Certificado publico";
		$pago->valor = preg_replace('/[.]/', '', $solicitud->valor_pagar);
		$pago->valor_subtotal = preg_replace('/[.]/', '', $solicitud->valor_pagar);
		$pago->acudiente_id = null;
		$pago->_uid = $solicitud->tipo_documento;
		$pago->servicio_id = $solicitud->id;
		$pago->tipo_pago = "Efectivo";
		$pago->descuento = null;
		$pago->cupon = null;
		$pago->save();

		return "Good";
	}

    public function VerSolicitud($codigounico) {
        $solicitud = SolicitudCertificadosPublicos::where('codigounico', $codigounico)
        ->join('certificados', 'solicitud_certificados_publicos.tipo_documento', '=', 'certificados.id')
        ->select('solicitud_certificados_publicos.*', 'certificados.concepto as tipo_documento_name')
        ->first();
        
        return $solicitud;
    }
}
