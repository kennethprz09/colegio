<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use App\User;

class LoginController extends Controller
{
    public function login(Request $request) {
        $this->validate($request, [
            'username'       => 'required|string',
            'password'    => 'required|string',
            'remember_me' => 'boolean',
        ]);

        $http = new Client(['verify' => false]);
        // $http = new Client();
        try {
            $response = $http->post(config('services.passport.login_endpoint'),[
                'form_params' => [
                    'grant_type' => 'password',
                    'client_id' => config('services.passport.client_id'),
                    'client_secret' => config('services.passport.client_secret'),
                    'username' => $request->username,
                    'password' => $request->password,
                ]
            ]);
            
            return $response->getBody();

        } catch (BadResponseException $e) {
            
            if ($e->getCode() === 400) {
                return response()->json(['errors' => ['login' => ['Usuario o contraseña invalido']]], $e->getCode());
                // return response()->json('Invalid Request. Please enter a username or a password.', $e->getCode());
            } else if ($e->getCode() === 401) {
                return response()->json(['errors' => ['login' => ['Credenciales invalidas']]], $e->getCode());
                // return response()->json('Your credentials are incorrect. Please try again', $e->getCode());
            }

            return response()->json(['errors' => ['login' => ['Algo salió mal en el servidor.']]], $e->getCode());
            // return response()->json('Something went wrong on the server.', $e->getCode());
        }
    }

    public function Update_pass(Request $request) {
        $this->validate($request, [
			'id' => 'required',
			'password' => 'required|min:8',
        ]);
        
        $user = User::find($request->id);
        $user->password = bcrypt($request->password);
        $user->save();
    }
    
    public function logout() {
        auth()->user()->tokens->each(function ($token, $key) {
            $token->delete();
        });
        
        return response()->json('Logged out successfully', 200);
    }
}