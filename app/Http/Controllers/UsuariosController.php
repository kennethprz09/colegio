<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\HistoricoCartera;
use App\InformePago;
use App\Externos\Estudiante;
use App\Externos\Madre;
use App\Externos\Padre;
use App\Externos\Acudiente;
use Carbon\Carbon;
use DB;

class UsuariosController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}

	public function Save(Request $request)
	{
		$admin = new User;
		$admin->name = $request->name;
		$admin->email = $request->email;
		$admin->codigo = $request->codigo;
		$admin->password = bcrypt($request->password);
		$admin->roll = 'admin';
		$admin->save();
	}

	public function Update(Request $request)
	{
		$admin = User::find($request->id);
		$admin->name = $request->name;
		$admin->email = $request->email;
		$admin->codigo = $request->codigo;
		if ($request->password) {
			$admin->password = bcrypt($request->password);
		}
		$admin->save();
	}

	public function ListAdmin(Request $request)
	{
		$columns = ['id'];

		$length = $request->input('length');
		$column = $request->input('column'); //Index
		$dir = $request->input('dir');
		$searchValue = $request->input('search');

		$query = User::orderBy($columns[$column], $dir)->where('roll', 'admin')->where('email', '!=', 'Kenneth@utam.co');

		if ($searchValue) {
			$query->where(function ($query) use ($searchValue) {
				$query->where('codigo', 'LIKE', '%' . $searchValue . '%')
					->orWhere('name', 'LIKE', '%' . $searchValue . '%')
					->orWhere('email', 'LIKE', '%' . $searchValue . '%');
			});
		}

		$projects = $query->paginate($length);
		return ['data' => $projects, 'draw' => $request->input('draw')];
	}

	public function ListAcudiente(Request $request)
	{
		$columns = ['id'];

		$length = $request->input('length');
		$column = $request->input('column'); //Index
		$dir = $request->input('dir');
		$searchValue = $request->input('search');

		$query = User::orderBy($columns[$column], $dir)->where('roll', 'acudiente');

		if ($searchValue) {
			$query->where(function ($query) use ($searchValue) {
				$query->where('codigo', 'LIKE', '%' . $searchValue . '%')
					->orWhere('name_estudiante', 'LIKE', '%' . $searchValue . '%')
					->orWhere('name', 'LIKE', '%' . $searchValue . '%')
					->orWhere('email', 'LIKE', '%' . $searchValue . '%')
					->orWhere('saldo', 'LIKE', '%' . $searchValue . '%');
			});
		}

		$projects = $query->paginate($length);
		foreach ($projects as $item) {
			$item->deuda = HistoricoCartera::where('acudiente_id', $item->id)->where('status', 'activa')->sum('valor');
		}
		return ['data' => $projects, 'draw' => $request->input('draw')];
	}

	public function Restablecer($id)
	{
		$admin = User::find($id);
		$admin->password = bcrypt($admin->codigo);
		$admin->save();
	}

	public function Delete($id)
	{
		$admin = User::find($id)->delete();
	}

	public function UpdateGrado()
	{
		$acudientes = User::where('roll', 'acudiente')->get();
		foreach ($acudientes as $acudiente) {
			$estudiante = Estudiante::where('estado', null)->where('codigo_estuiante', $acudiente->codigo)->first();
			if ($estudiante) {
				if ($acudiente->grado != $estudiante->grado_cursar_estudiante) {
					$acudiente->grado = $estudiante->grado_cursar_estudiante;
					$acudiente->save();
				}
			}
		}
		return 'Good';
	}

	public function UpdateDatos()
	{
		$acudientes = User::where('roll', 'acudiente')->get();
		foreach ($acudientes as $acudiente) {
			$estudiante = Estudiante::where('estado', null)->where('codigo_estuiante', $acudiente->codigo)->first();
			if ($estudiante) {
				$mama = Madre::where('id_estudiante', $estudiante->id)->first();
				$padre = Padre::where('id_estudiante', $estudiante->id)->first();
				$acudientedb2 = Acudiente::where('id_estudiante', $estudiante->id)->first();

				// Validamor quien tiene que paga
				if ($mama['responsable'] == 'si') {
					$estudiante->prefijo = 'madre';
					$estudiante->acudiente_name = $mama->nombre_madre;
					$estudiante->acudiente_email = $mama->email_madre;
				} else if ($padre['responsable'] == 'si') {
					$estudiante->prefijo = 'padre';
					$estudiante->acudiente_name = $padre->nombre_padre;
					$estudiante->acudiente_email = $padre->email_padre;
				} else if ($acudientedb2['responsable'] == 'si') {
					$estudiante->prefijo = 'acudiente';
					$estudiante->acudiente_name = $acudientedb2->nombre_acudiente;
					$estudiante->acudiente_email = $acudientedb2->email_acudiente;
				} else {
					$estudiante->prefijo = 'acudiente';
					$estudiante->acudiente_name = $acudientedb2->nombre_acudiente;
					$estudiante->acudiente_email = $acudientedb2->email_acudiente;
				}

				$acudiente->name = $estudiante->acudiente_name;
				$acudiente->email = $estudiante->acudiente_email;
				$acudiente->name_estudiante = $estudiante->nombre_estudiante;
				$acudiente->save();
			}
		}
		return 'Good';
	}

	public function MatriculaAnual()
	{
		$matriculas = InformePago::where('_tramite', 'Matricula')->update(['matricula_estado' => 'Inactivo']);
		$matriculas2 = InformePago::where('_tramite', 'Matricula')->pluck('id');
		$pension_active = HistoricoCartera::whereIn('matricula_id', $matriculas2)->update(['status' => 'activa']);

		return 'Good';
	}

	public function Pension1Mes()
	{
		$fecha = Carbon::now();
		$mes_actual = $fecha->format('m');
		$año_actual = $fecha->format('Y');
		$pension_año = HistoricoCartera::where('servicio', 'Pension')->whereYear('created_at', $año_actual)->pluck('id');
		$pension_active = HistoricoCartera::whereIn('id', $pension_año)->whereMonth('created_at', $mes_actual)->update(['status' => 'activa']);
		$pension_activadas = HistoricoCartera::whereIn('id', $pension_año)->whereMonth('created_at', $mes_actual)->get();

		return $pension_activadas;
	}

	public function Pension5Mes()
	{
		$fecha = Carbon::now();
		$mes_actual = $fecha->format('m');
		$año_actual = $fecha->format('Y');
		$pension_año = HistoricoCartera::where('servicio', 'Pension')->whereYear('created_at', $año_actual)->pluck('id');
		$pension_activadas = HistoricoCartera::whereIn('id', $pension_año)->whereMonth('created_at', 9)->get();

		foreach ($pension_activadas as $item) {
			$total = $item->valor;
			$cincoporciento = 0;
			$cincoporciento = (5 / 100) * $total;
			$valorbase = ($total + $cincoporciento);
			$valor = intval($valorbase);



			$item->mora = 'Si';
			$item->status = 'activa';
			$item->valor = $valor;
			$item->save();
		}

		return $pension_activadas;
	}
}
