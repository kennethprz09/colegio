<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\MediasTecnicas;
use App\Docente;
use App\InformePago;
use File, Auth;

class MediasTecnicasController extends Controller {
	public function __construct() {
		$this->middleware('auth');
	}

	protected function ListDetalles($items) {
		foreach ($items as $item) {
			$item->inicia = $item->inicia_at->format('Y-m-d');
			$item->termina = $item->termina_at->format('Y-m-d');
			$item->inicia_l = $item->inicia_at->format('d-m-Y');
			$item->termina_l = $item->termina_at->format('d-m-Y');

			$item->valor = preg_replace('/[.]/', '', $item->valor);
			
			$docente = Docente::find($item->docente_id);
			$item->docente_email = $docente->email;
		}
	}

	public function List(Request $request) {
		$columns = [ 'id' ];

		$length = $request->input('length');
		$column = $request->input('column'); //Index
		$dir = $request->input('dir');
		$searchValue = $request->input('search');

		$query = MediasTecnicas::orderBy($columns[$column], $dir);

		if($searchValue) {
			$query->where(function($query) use ($searchValue) {
				$query->where('actividad', 'LIKE', '%' . $searchValue . '%')
				->orWhere('inicia_at', 'LIKE', '%' . $searchValue . '%')
				->orWhere('termina_at', 'LIKE', '%' . $searchValue . '%')
				->orWhere('valor', 'LIKE', '%' . $searchValue . '%');
			});
		}

		$projects = $query->paginate($length);
		$this->ListDetalles($projects);
		return ['data' => $projects, 'draw' => $request->input('draw')];
	}

	public function ListDocentes() {
		return Docente::all();
	}

	public function ListToAcudientes() {
		$totales = [];
		$pagados = InformePago::where('_tramite', 'medias_tecnicas')->where('acudiente_id', Auth::user()->id)->where('aprobado', 'A')->select('_uid')->get();
		$items = MediasTecnicas::whereNotIn('id', $pagados)->get();
		$this->ListDetalles($items);

		return $items;
	}

	public function ListToAcudientesInscritos() {
		$totales = [];
		$pagados = InformePago::where('_tramite', 'medias_tecnicas')->where('acudiente_id', Auth::user()->id)->where('aprobado', 'A')->select('_uid')->get();
		$items = MediasTecnicas::whereIn('id', $pagados)->get();
		$this->ListDetalles($items);
		
		return $items;
	}

	public function Save(Request $request) {
		$messages = [
			'after' => 'La fecha de terminación debe ser mayor a la de iniciación.',
		];
		$this->validate($request, [
			'actividad' => 'required',
			'inicia_at' => 'required',
			'termina_at' => 'required|after:inicia_at',
			'valor' => 'required',
			'docente_id' => 'required',
			'informacion' => 'required',
			'horario' => 'required',
		], $messages);

		$docente = Docente::find($request->docente_id);
		$objeto = new MediasTecnicas;
			$file = $request->file('imagen'); // Manipulando imagen
			$file_veri = file_exists($file);
			if ($file_veri == 'true') {
				$destinationPath = public_path().'/imagenes/medias_tecnicas';
				$filenameO = $file->getClientOriginalName();
				$filename = Str::random(12);
				$upload_success = $file->move($destinationPath, $filename . '-' . $filenameO);
				$objeto->imagen = $filename . '-' . $filenameO; // Guardando Imagen
			}


		$objeto->actividad = $request->actividad;
		$objeto->inicia_at = new \DateTime($request->inicia_at);
		$objeto->termina_at = new \DateTime($request->termina_at);
		$objeto->valor = $request->valor;
		$objeto->url_informacion = $request->informacion;
		$objeto->url_horarios = $request->horario;
		$objeto->docente_id = $request->docente_id;
		$objeto->docente = $docente->nombre;
		$objeto->save();

		// MediasTecnicas::create($request->all());
	}

	public function Update(Request $request) {
		$messages = [
			'after' => 'La fecha de terminación debe ser mayor a la de iniciación.',
		];
		$this->validate($request, [
			'id' => 'required',
			'actividad' => 'required',
			'inicia_at' => 'required',
			'termina_at' => 'required|after:inicia_at',
			'valor' => 'required',
			'docente_id' => 'required',
			'informacion' => 'required',
			'horario' => 'required',
		], $messages);

		$docente = Docente::find($request->docente_id);
		$objeto = MediasTecnicas::find($request->id);

        $file = $request->file('imagen'); // Manipulando imagen
        $file_veri = file_exists($file);
        if ($file_veri == 'true') {
            $fileE = public_path(). "/imagenes/medias_tecnicas/" . $objeto->imagen; // Borrando imagen antigua
            $investigacionE = file_exists($fileE);
            if ($investigacionE == 'true') {
                // si coincide borra la investigacion anterior
                File::delete($fileE);
            }

            $destinationPath = public_path().'/imagenes/medias_tecnicas';
            $filenameO = $file->getClientOriginalName();
			$filename = Str::random(12);
            $upload_success = $file->move($destinationPath, $filename . '-' . $filenameO);
            $objeto->imagen = $filename . '-' . $filenameO; // Guardando Imagen
        }

		$objeto->actividad = $request->actividad;
		$objeto->inicia_at = new \DateTime($request->inicia_at);
		$objeto->termina_at = new \DateTime($request->termina_at);
		$objeto->valor = $request->valor;
		$objeto->url_informacion = $request->informacion;
		$objeto->url_horarios = $request->horario;
		$objeto->docente_id = $request->docente_id;
		$objeto->docente = $docente->nombre;
		$objeto->save();

		// MediasTecnicas::find($request->id)->update($request->all());
	}

	public function Delete($id) {
		MediasTecnicas::find($id)->delete();
	}
	public function Pagado(Request $request) {
		$informe = InformePago::create($request->all());
		return $request;
	}
}
