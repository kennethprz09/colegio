<?php

namespace App\Exports;

use App\InformePago;
use App\SolicitudIngreso;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class InformePagosPublicExport implements FromView {
    public function view(): View {
        $items = InformePago::where('aprobado', 'A')->whereNull('acudiente_id')->get();

        foreach ($items as $item) {
            if ($item->_tramite == 'Inscripcion') {
                $acudiente = SolicitudIngreso::find($item->_uid);
                $item->acudiente_codigo = $acudiente->codigo_estudiante;
            }
        }
        
        return view('excel.InformePagosExport', [
            'items' => $items
        ]);
    }
}
