<?php

namespace App\Exports;

use App\InformePago;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class InformePagosExport implements FromView {
    public function view(): View {
        $items = InformePago::where('aprobado', 'A')->join('users', 'informe_pagos.acudiente_id', '=', 'users.id')
        ->select('informe_pagos.*', 'users.codigo as acudiente_codigo')->get();
        
        return view('excel.InformePagosExport', [
            'items' => $items
        ]);
    }
}
