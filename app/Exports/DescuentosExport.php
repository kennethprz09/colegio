<?php

namespace App\Exports;

use App\Descuentos;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class DescuentosExport implements FromView {
    public function view(): View {
        $items = Descuentos::all();
        
        return view('excel.DescuentosExport', [
            'items' => $items
        ]);
    }
}
