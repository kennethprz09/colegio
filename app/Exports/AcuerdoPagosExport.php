<?php

namespace App\Exports;

use App\AcuerdoPago;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class AcuerdoPagosExport implements FromView {
    public function view(): View {
        $items = AcuerdoPago::where('acuerdo_pagos.status', 'Autenticado')->join('users', 'acuerdo_pagos.acudiente_id', '=', 'users.id')
        ->select('acuerdo_pagos.*', 'users.codigo as acudiente_codigo')->get();
        
        return view('excel.AcuerdoPagosExport', [
            'items' => $items
        ]);
    }
}
