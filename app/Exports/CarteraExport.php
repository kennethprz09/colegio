<?php

namespace App\Exports;

use App\HistoricoCartera;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class CarteraExport implements FromView {
    public function view(): View {
        $items = HistoricoCartera::where('historico_carteras.status', 'activa')
        ->join('users', 'historico_carteras.acudiente_id', '=', 'users.id')
        ->select('historico_carteras.*', 'users.codigo as acudiente_codigo')->get();
        
        return view('excel.CarteraExport', [
            'items' => $items
        ]);
    }
}
