<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cursos extends Model
{
    protected $fillable = [
		'asignatura', 'tarifa', 'docente',  'docente_id',
	];

	protected $casts = [
		'inicia_at' => 'datetime',
		'termina_at' => 'datetime'
	];
}
