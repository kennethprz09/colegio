<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AcuerdoPago extends Model
{
    protected $fillable = [
		  'cantidad_cuotas', 'deuda_total', 'deuda_cuotas', 'acudiente_id', 'status', 'finalizado', 'pdf',
    ];
    
    protected $casts = [
		'fecha_at' => 'datetime',
	];
}
