<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nivelacion extends Model
{
	protected $fillable = [
		'valor', 'email',
	];

    protected $casts = [
		'inicia_at' => 'datetime',
		'termina_at' => 'datetime'
	];
}
