<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cafeteria extends Model
{
    protected $fillable = [
		'descripcion', 'pago', 'valor',
	];
}
