(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[44],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/acudiente/profile.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/acudiente/profile.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
var appLoading = document.getElementById("loading-bg");
var options = {
  closeButton: true,
  debug: false,
  newestOnTop: false,
  progressBar: true,
  positionClass: "toast-top-right",
  preventDuplicates: false,
  onclick: null,
  showDuration: "300",
  hideDuration: "1000",
  timeOut: "5000",
  extendedTimeOut: "1000",
  showEasing: "swing",
  hideEasing: "linear",
  showMethod: "fadeIn",
  hideMethod: "fadeOut"
};
/* harmony default export */ __webpack_exports__["default"] = ({
  mounted: function mounted() {
    this.List();
  },
  watch: {
    saldo: function saldo(value) {
      var fomarteado = 0;
      var num = value.replace(/\./g, "");

      if (!isNaN(num)) {
        num = num.toString().split("").reverse().join("").replace(/(?=\d*\.?)(\d{3})/g, "$1.");
        num = num.split("").reverse().join("").replace(/^[\.]/, "");
        fomarteado = num;
      }

      this.saldo = fomarteado;
    }
  },
  data: function data() {
    return {
      user: {
        password: null
      },
      saldo: 0
    };
  },
  methods: {
    List: function List() {
      var _this = this;

      appLoading.style.display = "block";
      axios.get("/api/user").then(function (response) {
        _this.user = response.data;
        appLoading.style.display = "none";
      });
    },
    Update_pass: function Update_pass(item) {
      var _this2 = this;

      appLoading.style.display = "block";
      axios.put("/api/acudiente/password", item).then(function (response) {
        _this2.List();

        _this2.$toastr.success("Contraseña actualizada!", null, options);
      })["catch"](function (e) {
        appLoading.style.display = "none";
        var errors = e.response.data.errors;
        var noti = _this2.$toastr.error;
        $.each(errors, function (i) {
          noti(errors[i][0], "Errores", options);
        });
      });
    },
    PayObjeto: function PayObjeto(value) {
      var precio = value.replace(".", ""); // Envía los datos al CPV

      function enviarCpv(reg) {
        var url = "https://www.pagosvirtualesavvillas.com.co/personal/pagos/pagar";
        var form = "";
        $.each(reg, function (key, value) {
          form += '<input type="hidden" name="' + key + '" value="' + value + '">';
        });
        $('<form action="' + url + '" method="POST">' + form + "</form>").appendTo($(document.body)).submit();
      } // Cifra el texto con la clave indicada


      function cifrar(texto, claveHttps) {
        var hash = CryptoJS.HmacSHA256(texto, claveHttps);
        return CryptoJS.enc.Base64.stringify(hash);
      } // Calcula el hash


      function setHash(reg, claveHttps) {
        var texto = [reg.convenioId, reg.transaccionConvenioId || "", reg.referenciaPago1, reg.referenciaPago2 || "", reg.referenciaPago3 || "", reg.referenciaPago4 || "", reg.valor || "", reg.urlRespuesta].join("-");
        reg.hash = cifrar(texto, claveHttps);
      } // Calcula el hash para consultar el estado de la transacción


      function getHashConsulta(reg, claveHttps) {
        var texto = [reg.convenioId, reg.transaccionConvenioId || ""].join("-");
        return cifrar(texto, claveHttps);
      }

      if (this.user.grado <= 5) {
        var convenioId = 12564;
        var clave = "CATOLICOPRIM20";
      }

      if (this.user.grado == "PJ") {
        var convenioId = 12564;
        var clave = "CATOLICOPRIM20";
      }

      if (this.user.grado == "JD") {
        var convenioId = 12564;
        var clave = "CATOLICOPRIM20";
      }

      if (this.user.grado == "TR") {
        var convenioId = 12564;
        var clave = "CATOLICOPRIM20";
      }

      if (this.user.grado >= 6) {
        var convenioId = 12565;
        var clave = "CATOLICOBACHI20";
      } // Procesa el evento al hacer clic en el botón de pago
      // function botonPagoCpv() {


      var referencia = "Colegio_" + Math.floor(Math.random() * 999999 + 1);
      var reg = {};
      reg.convenioId = convenioId;
      reg.transaccionConvenioId = referencia; // Id de la transacción para el convenio Alfanumérico hasta 40 caracteres.

      reg.referenciaPago1 = this.user.codigo;
      reg.descripcion = "Recarga de saldo del sistema";
      reg.valor = precio.replace(".", "");
      reg.urlRespuesta = "https://pasareladepagos.colegiocatolicocali.edu.co/respuestaCpv?&transaccionId={transaccionId}&uuid={uuid}&aprobado={aprobado}&transaccionConvenioId={transaccionConvenioId}&ref1={ref1}&valor={valor}&valor_subtotal={valor}&_tramite=Recarga&acudiente_id=" + this.user.id; // Esta función se debe agregar al final para calcular el hash

      setHash(reg, clave);
      enviarCpv(reg, "#formCpv"); // El hash necesesario para realizar la consulta se genera

      var hashConsulta = getHashConsulta(reg, clave); // console.log("El hash a usar para consultar el estado de la transacción es: " + hashConsulta);

      return false;
    }
  },
  computed: {
    user: function user() {
      return this.$store.state.ActiveUser;
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/acudiente/profile.vue?vue&type=template&id=e4544646&":
/*!***************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/acudiente/profile.vue?vue&type=template&id=e4544646& ***!
  \***************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "col-12 px-5" }, [
    _c("div", { staticClass: "row" }, [
      _vm._m(0),
      _vm._v(" "),
      _c("div", { staticClass: "form-group col-6" }, [
        _c("label", [_vm._v("Bolsillo")]),
        _vm._v(" "),
        _c("div", { staticClass: "input-group" }, [
          _vm._m(1),
          _vm._v(" "),
          _c("input", {
            directives: [
              {
                name: "model",
                rawName: "v-model",
                value: _vm.saldo,
                expression: "saldo"
              }
            ],
            staticClass: "form-control",
            attrs: { type: "text", name: "recarga" },
            domProps: { value: _vm.saldo },
            on: {
              input: function($event) {
                if ($event.target.composing) {
                  return
                }
                _vm.saldo = $event.target.value
              }
            }
          })
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "form-group col-6 m-auto" }, [
        _c(
          "button",
          {
            staticClass: "btn btn-primary btn-sm boton",
            attrs: { type: "submit", disabled: !_vm.saldo },
            on: {
              click: function($event) {
                return _vm.PayObjeto(_vm.saldo)
              }
            }
          },
          [_vm._v("\n        Recarga tu bolsillo\n      ")]
        )
      ])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "row" }, [
      _c("div", { staticClass: "form-group col-6" }, [
        _c("label", { staticClass: "control-label" }, [_vm._v("Código")]),
        _vm._v(" "),
        _c("input", {
          staticClass: "form-control",
          attrs: { type: "text", disabled: "" },
          domProps: { value: _vm.user.codigo }
        })
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "form-group col-6" }, [
        _c("label", { staticClass: "control-label" }, [_vm._v("Grado")]),
        _vm._v(" "),
        _c("input", {
          staticClass: "form-control",
          attrs: { type: "text", disabled: "" },
          domProps: { value: _vm.user.grado }
        })
      ])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "row" }, [
      _c("div", { staticClass: "form-group col-6" }, [
        _c("label", { staticClass: "control-label" }, [
          _vm._v("Nombre estudiante")
        ]),
        _vm._v(" "),
        _c("input", {
          staticClass: "form-control",
          attrs: { type: "text", disabled: "" },
          domProps: { value: _vm.user.name_estudiante }
        })
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "form-group col-6" }, [
        _c("label", { staticClass: "control-label" }, [
          _vm._v("Correo electrónico")
        ]),
        _vm._v(" "),
        _c("input", {
          staticClass: "form-control",
          attrs: { type: "text", disabled: "" },
          domProps: { value: _vm.user.email }
        })
      ])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "row" }, [
      _c("div", { staticClass: "form-group col-6" }, [
        _c("label", { staticClass: "control-label" }, [
          _vm._v("Nueva contraseña")
        ]),
        _vm._v(" "),
        _c("input", {
          directives: [
            {
              name: "model",
              rawName: "v-model",
              value: _vm.user.password,
              expression: "user.password"
            }
          ],
          staticClass: "form-control",
          attrs: { type: "password" },
          domProps: { value: _vm.user.password },
          on: {
            input: function($event) {
              if ($event.target.composing) {
                return
              }
              _vm.$set(_vm.user, "password", $event.target.value)
            }
          }
        })
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "form-group col-6 m-auto" }, [
        _c(
          "button",
          {
            staticClass: "btn btn-primary btn-sm boton",
            attrs: { type: "submit", disabled: !_vm.user.password },
            on: {
              click: function($event) {
                return _vm.Update_pass(_vm.user)
              }
            }
          },
          [_vm._v("\n        Actualizar\n      ")]
        )
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-12 text-center" }, [
      _c("i", {
        staticClass: "fas fa-info-circle",
        staticStyle: { "font-size": "30px" }
      }),
      _vm._v(" "),
      _c("span", { staticClass: "pl-2" }, [
        _vm._v(
          "¡Ahorra! una forma de administrar los pagos de la educación de tu hijo."
        )
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "input-group-prepend" }, [
      _c("span", { staticClass: "input-group-text md-addon" }, [_vm._v("$")])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/views/acudiente/profile.vue":
/*!**************************************************!*\
  !*** ./resources/js/views/acudiente/profile.vue ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _profile_vue_vue_type_template_id_e4544646___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./profile.vue?vue&type=template&id=e4544646& */ "./resources/js/views/acudiente/profile.vue?vue&type=template&id=e4544646&");
/* harmony import */ var _profile_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./profile.vue?vue&type=script&lang=js& */ "./resources/js/views/acudiente/profile.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _profile_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _profile_vue_vue_type_template_id_e4544646___WEBPACK_IMPORTED_MODULE_0__["render"],
  _profile_vue_vue_type_template_id_e4544646___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/acudiente/profile.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/acudiente/profile.vue?vue&type=script&lang=js&":
/*!***************************************************************************!*\
  !*** ./resources/js/views/acudiente/profile.vue?vue&type=script&lang=js& ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_profile_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./profile.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/acudiente/profile.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_profile_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/acudiente/profile.vue?vue&type=template&id=e4544646&":
/*!*********************************************************************************!*\
  !*** ./resources/js/views/acudiente/profile.vue?vue&type=template&id=e4544646& ***!
  \*********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_profile_vue_vue_type_template_id_e4544646___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./profile.vue?vue&type=template&id=e4544646& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/acudiente/profile.vue?vue&type=template&id=e4544646&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_profile_vue_vue_type_template_id_e4544646___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_profile_vue_vue_type_template_id_e4544646___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);