(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[46],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/admin/pago_matriculas/detalles.vue?vue&type=script&lang=js&":
/*!************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/admin/pago_matriculas/detalles.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
var appLoading = document.getElementById("loading-bg");
var options = {
  closeButton: true,
  debug: false,
  newestOnTop: false,
  progressBar: true,
  positionClass: "toast-top-right",
  preventDuplicates: false,
  onclick: null,
  showDuration: "300",
  hideDuration: "1000",
  timeOut: "5000",
  extendedTimeOut: "1000",
  showEasing: "swing",
  hideEasing: "linear",
  showMethod: "fadeIn",
  hideMethod: "fadeOut"
};
/* harmony default export */ __webpack_exports__["default"] = ({
  props: ["matriculaId"],
  mounted: function mounted() {
    this.List();
  },
  data: function data() {
    return {
      matricula: {},
      acudiente: {}
    };
  },
  methods: {
    List: function List() {
      var _this = this;

      appLoading.style.display = "block";
      var url = "/api/admin/pagos-matricula/" + this.matriculaId;
      axios.post(url).then(function (response) {
        appLoading.style.display = "none";
        _this.matricula = response.data;
        _this.acudiente = response.data.user;
      });
    },
    SubirPdf: function SubirPdf(status) {
      var _this2 = this;

      swal({
        closeOnClickOutside: false,
        closeOnEsc: false,
        buttons: {
          cancel: {
            text: "Cancelar",
            visible: true,
            closeModal: true,
            className: "btn-outline-blue-grey"
          },
          confirm: "Subir",
          "default": {
            value: false,
            visible: false
          }
        },
        content: {
          element: "input",
          attributes: {
            id: "archivo_pdf",
            placeholder: "Sube el PDF",
            type: "file",
            accept: ".pdf"
          }
        }
      }).then(function (value) {
        if (value) {
          appLoading.style.display = "block";
          var url = "/api/admin/pagos-matricula/pdf/up";
          var datos = new FormData();
          datos.append("id", _this2.matriculaId);
          datos.append("status", status);
          datos.append("pdf", $("#archivo_pdf")[0].files[0]);
          axios.post(url, datos).then(function (response) {
            _this2.List();

            swal("Archivo subido exitosamente!", {
              button: false
            });
          });
        } else {
          swal("No fue subido ningun archivo!", {
            button: false
          });
        }
      });
    }
  },
  computed: {//
  },
  filters: {
    addmiles: function addmiles(nStr) {
      nStr += "";
      var x = nStr.split(".");
      var x1 = x[0];
      var x2 = x.length > 1 ? "." + x[1] : "";
      var rgx = /(\d+)(\d{3})/;

      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, "$1" + "." + "$2");
      }

      return x1 + x2;
    },
    año_format: function aO_format(fecha) {
      if (fecha == null) {
        return "";
      } else {
        var today = new Date(fecha);
        var dd = String(today.getDate()).padStart(2, "0");
        var mm = String(today.getMonth() + 1).padStart(2, "0"); //January is 0!

        var yyyy = today.getFullYear();
        today = yyyy;
        return today;
      }
    },
    año_1_format: function aO_1_format(fecha) {
      if (fecha == null) {
        return "";
      } else {
        var today = new Date(fecha);
        var dd = String(today.getDate()).padStart(2, "0");
        var mm = String(today.getMonth() + 1).padStart(2, "0"); //January is 0!

        var yyyy = today.getFullYear();
        today = yyyy + 1;
        return today;
      }
    },
    fecha_format: function fecha_format(fecha) {
      if (fecha == null) {
        return "";
      } else {
        var today = new Date(fecha);
        var dd = String(today.getDate()).padStart(2, "0");
        var mm = String(today.getMonth() + 1).padStart(2, "0"); //January is 0!

        var yyyy = today.getFullYear();
        today = dd + "-" + mm + "-" + yyyy;
        return today;
      }
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/admin/pago_matriculas/detalles.vue?vue&type=template&id=08877ccf&":
/*!****************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/admin/pago_matriculas/detalles.vue?vue&type=template&id=08877ccf& ***!
  \****************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("section", [
    _c("div", { staticClass: "container-fluid" }, [
      _vm._m(0),
      _vm._v(" "),
      _c("h4", { staticClass: "blue-text font-weight-bold" }, [
        _vm._v("Pago matricúla")
      ]),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "bg-light col-12 d-flex justify-content-between py-4" },
        [
          _c("div", { staticClass: "p-0 m-auto" }, [
            _c("span", { staticClass: "font-weight-bold" }, [
              _vm._v(
                "\n          " +
                  _vm._s(_vm.acudiente.name_estudiante) +
                  " / Codígo " +
                  _vm._s(_vm.acudiente.codigo) +
                  "\n        "
              )
            ])
          ]),
          _vm._v(" "),
          _vm._m(1),
          _vm._v(" "),
          _c("div", { staticClass: "p-0 m-auto" }, [
            _c("span", { staticClass: "font-weight-bold" }, [
              _vm._v(
                " $ " + _vm._s(_vm._f("addmiles")(_vm.matricula.valor)) + " "
              )
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "p-0 m-auto text-center" }, [
            _c("span", { staticClass: "font-weight-normal" }, [
              _vm._v("\n          Año\n          "),
              _c("br"),
              _vm._v(
                "\n          " +
                  _vm._s(_vm._f("año_format")(_vm.matricula.created_at)) +
                  " -\n          " +
                  _vm._s(_vm._f("año_1_format")(_vm.matricula.created_at)) +
                  "\n        "
              )
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "p-0 m-auto text-center" }, [
            _c("span", { staticClass: "font-weight-normal" }, [
              _vm._v("\n          Fecha de pago\n          "),
              _c("br"),
              _vm._v(
                "\n          " +
                  _vm._s(_vm._f("fecha_format")(_vm.matricula.created_at)) +
                  "\n        "
              )
            ])
          ])
        ]
      ),
      _vm._v(" "),
      _c("br"),
      _vm._v(" "),
      _c("div", { staticClass: "col-12 py-4 mb-4" }, [
        _c("h4", { staticClass: "font-weight-bold mb-0" }, [
          _vm._v("Adjuntar documentos legalización matrícula")
        ]),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "col-12 d-flex justify-content-around pt-4 px-0" },
          [
            _c(
              "div",
              {
                staticClass: "border-dark card card-image",
                staticStyle: { height: "200px", width: "160px" }
              },
              [
                _c(
                  "div",
                  { staticClass: "pr-2 text-right" },
                  [
                    _vm.matricula.matricula_pagare
                      ? [
                          _c("i", { staticClass: "fa fa-check green-text" }),
                          _vm._v(" "),
                          _c(
                            "a",
                            {
                              attrs: {
                                href:
                                  "/matriculas-pdf/" +
                                  _vm.matricula.matricula_pagare,
                                target: "_blank"
                              }
                            },
                            [_c("i", { staticClass: "fa fa-eye blue-text" })]
                          )
                        ]
                      : _c("i", { staticClass: "fa fa-times red-text" })
                  ],
                  2
                ),
                _vm._v(" "),
                _c("div", { staticClass: "m-auto" }, [
                  _c(
                    "h1",
                    {
                      staticClass: "h1 text-body",
                      staticStyle: { "font-size": "3vw", cursor: "pointer" },
                      on: {
                        click: function($event) {
                          return _vm.SubirPdf("matricula_pagare")
                        }
                      }
                    },
                    [_vm._v("\n              PDF\n            ")]
                  )
                ]),
                _vm._v(" "),
                _c("span", { staticClass: "h6 text-center" }, [
                  _vm._v("Pagare")
                ])
              ]
            ),
            _vm._v(" "),
            _c(
              "div",
              {
                staticClass: "border-dark card card-image",
                staticStyle: { height: "200px", width: "160px" }
              },
              [
                _c(
                  "div",
                  { staticClass: "pr-2 text-right" },
                  [
                    _vm.matricula.matricula_contrato
                      ? [
                          _c("i", { staticClass: "fa fa-check green-text" }),
                          _vm._v(" "),
                          _c(
                            "a",
                            {
                              attrs: {
                                href:
                                  "/matriculas-pdf/" +
                                  _vm.matricula.matricula_contrato,
                                target: "_blank"
                              }
                            },
                            [_c("i", { staticClass: "fa fa-eye blue-text" })]
                          )
                        ]
                      : _c("i", { staticClass: "fa fa-times red-text" })
                  ],
                  2
                ),
                _vm._v(" "),
                _c("div", { staticClass: "m-auto" }, [
                  _c(
                    "h1",
                    {
                      staticClass: "h1 text-body",
                      staticStyle: { "font-size": "3vw", cursor: "pointer" },
                      on: {
                        click: function($event) {
                          return _vm.SubirPdf("matricula_contrato")
                        }
                      }
                    },
                    [_vm._v("\n              PDF\n            ")]
                  )
                ]),
                _vm._v(" "),
                _c("span", { staticClass: "h6 text-center" }, [
                  _vm._v("Contrato")
                ])
              ]
            ),
            _vm._v(" "),
            _c(
              "div",
              {
                staticClass: "border-dark card card-image",
                staticStyle: { height: "200px", width: "160px" }
              },
              [
                _c(
                  "div",
                  { staticClass: "pr-2 text-right" },
                  [
                    _vm.matricula.matricula_acta
                      ? [
                          _c("i", { staticClass: "fa fa-check green-text" }),
                          _vm._v(" "),
                          _c(
                            "a",
                            {
                              attrs: {
                                href:
                                  "/matriculas-pdf/" +
                                  _vm.matricula.matricula_acta,
                                target: "_blank"
                              }
                            },
                            [_c("i", { staticClass: "fa fa-eye blue-text" })]
                          )
                        ]
                      : _c("i", { staticClass: "fa fa-times red-text" })
                  ],
                  2
                ),
                _vm._v(" "),
                _c("div", { staticClass: "m-auto" }, [
                  _c(
                    "h1",
                    {
                      staticClass: "h1 text-body",
                      staticStyle: { "font-size": "3vw", cursor: "pointer" },
                      on: {
                        click: function($event) {
                          return _vm.SubirPdf("matricula_acta")
                        }
                      }
                    },
                    [_vm._v("\n              PDF\n            ")]
                  )
                ]),
                _vm._v(" "),
                _c("span", { staticClass: "h6 text-center" }, [
                  _vm._v("Acto de corresponsabilidad")
                ])
              ]
            ),
            _vm._v(" "),
            _c(
              "div",
              {
                staticClass: "border-dark card card-image",
                staticStyle: { height: "200px", width: "160px" }
              },
              [
                _c(
                  "div",
                  { staticClass: "pr-2 text-right" },
                  [
                    _vm.matricula.matricula_paz
                      ? [
                          _c("i", { staticClass: "fa fa-check green-text" }),
                          _vm._v(" "),
                          _c(
                            "a",
                            {
                              attrs: {
                                href:
                                  "/matriculas-pdf/" +
                                  _vm.matricula.matricula_paz,
                                target: "_blank"
                              }
                            },
                            [_c("i", { staticClass: "fa fa-eye blue-text" })]
                          )
                        ]
                      : _c("i", { staticClass: "fa fa-times red-text" })
                  ],
                  2
                ),
                _vm._v(" "),
                _c("div", { staticClass: "m-auto" }, [
                  _c(
                    "h1",
                    {
                      staticClass: "h1 text-body",
                      staticStyle: { "font-size": "3vw", cursor: "pointer" },
                      on: {
                        click: function($event) {
                          return _vm.SubirPdf("matricula_paz")
                        }
                      }
                    },
                    [_vm._v("\n              PDF\n            ")]
                  )
                ]),
                _vm._v(" "),
                _c("span", { staticClass: "h6 text-center" }, [
                  _vm._v("Paz y salvo")
                ])
              ]
            )
          ]
        )
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-12 d-flex mb-2 pb-2 border-bottom" }, [
      _c("i", {
        staticClass: "fal fa-file-alt w-auto",
        staticStyle: { color: "#0f62ac", "font-size": "100px !important" }
      }),
      _vm._v(" "),
      _c(
        "h5",
        {
          staticClass: "ml-2 mt-5",
          staticStyle: { "font-weight": "550 !important" }
        },
        [_vm._v("Detalle de cartera")]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "p-0 m-auto" }, [
      _c("small", [_vm._v("Valor matríicula")])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/views/admin/pago_matriculas/detalles.vue":
/*!***************************************************************!*\
  !*** ./resources/js/views/admin/pago_matriculas/detalles.vue ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _detalles_vue_vue_type_template_id_08877ccf___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./detalles.vue?vue&type=template&id=08877ccf& */ "./resources/js/views/admin/pago_matriculas/detalles.vue?vue&type=template&id=08877ccf&");
/* harmony import */ var _detalles_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./detalles.vue?vue&type=script&lang=js& */ "./resources/js/views/admin/pago_matriculas/detalles.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _detalles_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _detalles_vue_vue_type_template_id_08877ccf___WEBPACK_IMPORTED_MODULE_0__["render"],
  _detalles_vue_vue_type_template_id_08877ccf___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/admin/pago_matriculas/detalles.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/admin/pago_matriculas/detalles.vue?vue&type=script&lang=js&":
/*!****************************************************************************************!*\
  !*** ./resources/js/views/admin/pago_matriculas/detalles.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_detalles_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./detalles.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/admin/pago_matriculas/detalles.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_detalles_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/admin/pago_matriculas/detalles.vue?vue&type=template&id=08877ccf&":
/*!**********************************************************************************************!*\
  !*** ./resources/js/views/admin/pago_matriculas/detalles.vue?vue&type=template&id=08877ccf& ***!
  \**********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_detalles_vue_vue_type_template_id_08877ccf___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./detalles.vue?vue&type=template&id=08877ccf& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/admin/pago_matriculas/detalles.vue?vue&type=template&id=08877ccf&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_detalles_vue_vue_type_template_id_08877ccf___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_detalles_vue_vue_type_template_id_08877ccf___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);