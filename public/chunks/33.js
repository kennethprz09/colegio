(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[33],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/acudiente/pension/index.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/acudiente/pension/index.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _components_data_table_index_vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @/components/data-table/index.vue */ "./resources/js/components/data-table/index.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

var appLoading = document.getElementById("loading-bg");
var options = {
  closeButton: true,
  debug: false,
  newestOnTop: false,
  progressBar: true,
  positionClass: "toast-top-right",
  preventDuplicates: false,
  onclick: null,
  showDuration: "300",
  hideDuration: "1000",
  timeOut: "5000",
  extendedTimeOut: "1000",
  showEasing: "swing",
  hideEasing: "linear",
  showMethod: "fadeIn",
  hideMethod: "fadeOut"
};
/* harmony default export */ __webpack_exports__["default"] = ({
  mounted: function mounted() {
    this.Listar();
  },
  components: {
    Fulltable: _components_data_table_index_vue__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  data: function data() {
    return {
      pension_asignada: null,
      matricula_activa: null,
      pago_pension: 10,
      pago_año: false,
      meses: 3,
      deudas: [],
      deuda: false
    };
  },
  methods: {
    Listar: function Listar() {
      var _this = this;

      appLoading.style.display = "block";
      var url = "/api/acudiente/pension/tarifa";
      axios.post(url).then(function (response) {
        appLoading.style.display = "none";
        _this.pension_asignada = response.data.semejantes;
        _this.deudas = response.data.deudas;
        _this.matricula_activa = response.data.matricula_activa;
      })["catch"](function (e) {
        appLoading.style.display = "none";
        var errors = e.response.data.errors;
        var noti = _this.$toastr.error;
        $.each(errors, function (i) {
          noti(errors[i][0], "Errores", options);
        });
      });
    },
    CuponDescuento: function CuponDescuento(item) {
      var _this2 = this;

      swal({
        title: "¿Tienes un código de descuento?",
        content: {
          element: "input",
          attributes: {
            placeholder: "Ingresar código de descuento (Opcional)"
          }
        },
        button: {
          text: "Procesar pago",
          className: "btn-primary"
        }
      }).then(function (codigo) {
        if (codigo) {
          var tramite = "Pension";
          appLoading.style.display = "block";
          var url = "/api/acudiente/descuento/" + codigo + "/" + tramite;
          axios.post(url).then(function (response) {
            appLoading.style.display = "none";

            if (response.data == "Bad") {
              _this2.$toastr.warning("Codigo Invalido!", null, options);
            } else {
              _this2.PreguntaPago(item, response.data);
            }
          })["catch"](function (e) {
            appLoading.style.display = "none";
            var errors = e.response.data.errors;
            var noti = _this2.$toastr.error;
            $.each(errors, function (i) {
              noti(errors[i][0], "Errores", options);
            });
          });
        } else {
          _this2.PreguntaPago(item, null);
        }
      });
    },
    PreguntaPago: function PreguntaPago(item, descuento) {
      var _this3 = this;

      var saldo = 0;
      var total = null;
      total = item.valor.replace(".", "");
      var btn = true;

      if (descuento) {
        btn = false;
        console.log(btn);
        var monto = descuento.porcentaje;
        var total = total - monto;
        $(".swal-button--pasarela").css("display", "none");
      }

      if (total == 0) {
        this.Pagar_saldo(item, descuento);
      } else {
        swal({
          title: "¿Usar saldo disponible o enviar a pasarela de pago?",
          buttons: {
            saldo: {
              text: "Saldo",
              value: "Saldo",
              visible: true,
              className: "btn-outline-blue-grey",
              closeModal: true
            },
            pasarela: {
              text: "Pasarela",
              value: "Pasarela",
              visible: btn,
              className: "btn-outline-blue-grey",
              closeModal: true
            },
            defeat: {
              visible: false,
              closeModal: true,
              value: "Cerrar"
            }
          }
        }).then(function (value) {
          if (value == "Saldo") {
            // Descontar saldo
            if (_this3.user.saldo > 0) {
              saldo = _this3.user.saldo.replace(".", "");
            }

            if (_this3.user.saldo < 0) {
              saldo = _this3.user.saldo.replace(".", "");
            }

            if (parseFloat(saldo) >= parseFloat(total)) {
              _this3.Pagar_saldo(item, descuento);
            } else {
              swal("Saldo insuficiente!", {
                button: false
              });
            }

            $(".swal-button--pasarela").css("display", "block");
          }

          if (value == "Pasarela") {
            // Enviar a pasarela
            _this3.PayObjeto(item, descuento);
          }
        });
      }
    },
    PayObjeto: function PayObjeto(item, descuento) {
      // Envía los datos al CPV
      function enviarCpv(reg) {
        var url = "https://www.pagosvirtualesavvillas.com.co/personal/pagos/pagar";
        var form = "";
        $.each(reg, function (key, value) {
          form += '<input type="hidden" name="' + key + '" value="' + value + '">';
        });
        $('<form action="' + url + '" method="POST">' + form + "</form>").appendTo($(document.body)).submit();
      } // Cifra el texto con la clave indicada


      function cifrar(texto, claveHttps) {
        var hash = CryptoJS.HmacSHA256(texto, claveHttps);
        return CryptoJS.enc.Base64.stringify(hash);
      } // Calcula el hash


      function setHash(reg, claveHttps) {
        var texto = [reg.convenioId, reg.transaccionConvenioId || "", reg.referenciaPago1, reg.referenciaPago2 || "", reg.referenciaPago3 || "", reg.referenciaPago4 || "", reg.valor || "", reg.urlRespuesta].join("-");
        reg.hash = cifrar(texto, claveHttps);
      } // Calcula el hash para consultar el estado de la transacción


      function getHashConsulta(reg, claveHttps) {
        var texto = [reg.convenioId, reg.transaccionConvenioId || ""].join("-");
        return cifrar(texto, claveHttps);
      } // Procesa el evento al hacer clic en el botón de pago
      // function botonPagoCpv() {


      if (this.user.grado <= 5) {
        var convenioId = 12564;
        var clave = "CATOLICOPRIM20";
      }

      if (this.user.grado == "PJ") {
        var convenioId = 12564;
        var clave = "CATOLICOPRIM20";
      }

      if (this.user.grado == "JD") {
        var convenioId = 12564;
        var clave = "CATOLICOPRIM20";
      }

      if (this.user.grado == "TR") {
        var convenioId = 12564;
        var clave = "CATOLICOPRIM20";
      }

      if (this.user.grado >= 6) {
        var convenioId = 12565;
        var clave = "CATOLICOBACHI20";
      }

      var total = item.valor.replace(".", "");
      var url = "https://pasareladepagos.colegiocatolicocali.edu.co/respuestaCpv?&transaccionId={transaccionId}&uuid={uuid}&aprobado={aprobado}&transaccionConvenioId={transaccionConvenioId}&ref1={ref1}&valor={valor}" + "&_tramite=Pension&_uid=" + item.id + "&valor_subtotal=" + item.valor.replace(".", "") + "&acudiente_id=" + this.user.id + "&opcion=CarteraInactiva";
      var referencia = "Colegio_" + Math.floor(Math.random() * 999999 + 1);
      var reg = {};
      reg.convenioId = convenioId;
      reg.transaccionConvenioId = referencia; // Id de la transacción para el convenio Alfanumérico hasta 40 caracteres.

      reg.referenciaPago1 = this.user.codigo;
      reg.descripcion = "Pago de Pension test";
      reg.valor = total;
      reg.urlRespuesta = url; // Esta función se debe agregar al final para calcular el hash

      setHash(reg, clave);
      enviarCpv(reg, "#formCpv"); // El hash necesesario para realizar la consulta se genera

      var hashConsulta = getHashConsulta(reg, clave); // console.log("El hash a usar para consultar el estado de la transacción es: " + hashConsulta);

      return false;
    },
    Pagar_saldo: function Pagar_saldo(item, descuento) {
      var _this4 = this;

      appLoading.style.display = "block";
      var url = "/api/acudiente/saldo-pago";
      var monto = null;
      var codigo = null;
      var total = null;
      var opcion = null;
      total = item.valor.replace(".", "");
      opcion = "CarteraInactiva";

      if (descuento) {
        monto = descuento.porcentaje;
        codigo = descuento.codigo;
        total = total - monto;
      }

      axios.post(url, {
        aprobado: "A",
        valor: total,
        valor_subtotal: item.valor.replace(".", ""),
        _tramite: "Pension",
        _uid: item.id,
        servicio_id: item.id,
        acudiente_id: this.user.id,
        descuento: monto,
        cupon: codigo,
        opcion: opcion
      }).then(function (response) {
        appLoading.style.display = "none";

        _this4.$toastr.success("Pago realizado con éxito!", null, options);

        _this4.Listar();

        _this4.Actualizar_saldo();
      })["catch"](function (e) {
        appLoading.style.display = "none";
        var errors = e.response.data.errors;
        var noti = _this4.$toastr.error;
        $.each(errors, function (i) {
          noti(errors[i][0], "Errores", options);
        });
      });
    },
    // Pago multiple
    PreguntaPagoMulti: function PreguntaPagoMulti(meses) {
      var _this5 = this;

      swal({
        title: "¿Usar saldo disponible o enviar a pasarela de pago?",
        buttons: {
          saldo: {
            text: "Saldo",
            value: "Saldo",
            visible: true,
            className: "btn-outline-blue-grey",
            closeModal: true
          },
          pasarela: {
            text: "Pasarela",
            value: "Pasarela",
            visible: true,
            className: "btn-outline-blue-grey",
            closeModal: true
          },
          defeat: {
            visible: false,
            closeModal: true,
            value: "Cerrar"
          }
        }
      }).then(function (value) {
        if (meses <= 9) {
          var porcentaje = 3;
        } else if (meses >= 10) {
          var porcentaje = 5;
        }

        var saldo = 0;
        var total = parseInt(_this5.deudas[0].valor) * meses;
        var descuento = porcentaje / 100 * total;
        var total_descontado = parseInt(total) - parseInt(descuento);

        if (value == "Saldo") {
          // Descontar saldo
          if (_this5.user.saldo > 0) {
            saldo = _this5.user.saldo.replace(".", "");
          }

          if (_this5.user.saldo < 0) {
            saldo = _this5.user.saldo.replace(".", "");
          }

          if (parseFloat(saldo) >= parseFloat(total_descontado)) {
            _this5.Pagar_saldoMulti(meses, total, total_descontado, porcentaje);
          } else {
            swal("Saldo insuficiente!", {
              button: false
            });
          }
        }

        if (value == "Pasarela") {
          // Enviar a pasarela
          _this5.PayObjetoMulti(meses, total, total_descontado, porcentaje);
        }
      });
    },
    PayObjetoMulti: function PayObjetoMulti(meses, subtotal, total, porcentaje) {
      // Envía los datos al CPV
      function enviarCpv(reg) {
        var url = "https://www.pagosvirtualesavvillas.com.co/personal/pagos/pagar";
        var form = "";
        $.each(reg, function (key, value) {
          form += '<input type="hidden" name="' + key + '" value="' + value + '">';
        });
        $('<form action="' + url + '" method="POST">' + form + "</form>").appendTo($(document.body)).submit();
      } // Cifra el texto con la clave indicada


      function cifrar(texto, claveHttps) {
        var hash = CryptoJS.HmacSHA256(texto, claveHttps);
        return CryptoJS.enc.Base64.stringify(hash);
      } // Calcula el hash


      function setHash(reg, claveHttps) {
        var texto = [reg.convenioId, reg.transaccionConvenioId || "", reg.referenciaPago1, reg.referenciaPago2 || "", reg.referenciaPago3 || "", reg.referenciaPago4 || "", reg.valor || "", reg.urlRespuesta].join("-");
        reg.hash = cifrar(texto, claveHttps);
      } // Calcula el hash para consultar el estado de la transacción


      function getHashConsulta(reg, claveHttps) {
        var texto = [reg.convenioId, reg.transaccionConvenioId || ""].join("-");
        return cifrar(texto, claveHttps);
      }

      if (this.user.grado <= 5) {
        var convenioId = 12564;
        var clave = "CATOLICOPRIM20";
      }

      if (this.user.grado == "PJ") {
        var convenioId = 12564;
        var clave = "CATOLICOPRIM20";
      }

      if (this.user.grado == "JD") {
        var convenioId = 12564;
        var clave = "CATOLICOPRIM20";
      }

      if (this.user.grado == "TR") {
        var convenioId = 12564;
        var clave = "CATOLICOPRIM20";
      }

      if (this.user.grado >= 6) {
        var convenioId = 12565;
        var clave = "CATOLICOBACHI20";
      } // Procesa el evento al hacer clic en el botón de pago


      var referencia = "Colegio_" + Math.floor(Math.random() * 999999 + 1);
      var reg = {};
      reg.convenioId = convenioId;
      reg.transaccionConvenioId = referencia; // Id de la transacción para el convenio Alfanumérico hasta 40 caracteres.

      reg.referenciaPago1 = this.user.codigo;
      reg.descripcion = "Pago multiple de pension";
      reg.valor = total;
      reg.urlRespuesta = "https://pasareladepagos.colegiocatolicocali.edu.co/respuestaCpv?&transaccionId={transaccionId}&uuid={uuid}&aprobado={aprobado}&transaccionConvenioId={transaccionConvenioId}&ref1={ref1}&valor={valor}&_tramite=Pension Multiple&meses=" + meses + "&descuento=" + porcentaje + "&valor_subtotal=" + subtotal + "&acudiente_id=" + this.user.id; // Esta función se debe agregar al final para calcular el hash

      setHash(reg, clave);
      enviarCpv(reg, "#formCpv"); // El hash necesesario para realizar la consulta se genera

      var hashConsulta = getHashConsulta(reg, clave); // console.log("El hash a usar para consultar el estado de la transacción es: " + hashConsulta);

      return false;
    },
    Pagar_saldoMulti: function Pagar_saldoMulti(meses, subtotal, total, porcentaje) {
      var _this6 = this;

      appLoading.style.display = "block";
      var url = "/api/acudiente/saldo-pago";
      axios.post(url, {
        aprobado: "A",
        valor: total,
        valor_subtotal: subtotal,
        descuento: porcentaje,
        _tramite: "Pension Multiple",
        meses: meses,
        acudiente_id: this.user.id
      }).then(function (response) {
        appLoading.style.display = "none";

        _this6.$toastr.success("Pago realizado con éxito!", null, options);

        _this6.Listar();

        _this6.Actualizar_saldo();
      })["catch"](function (e) {
        appLoading.style.display = "none";
        var errors = e.response.data.errors;
        var noti = _this6.$toastr.error;
        $.each(errors, function (i) {
          noti(errors[i][0], "Errores", options);
        });
      });
    },
    Actualizar_saldo: function Actualizar_saldo() {
      appLoading.style.display = "block";
      this.$store.dispatch("updateSaldo").then(function (response) {
        appLoading.style.display = "none";
      });
    }
  },
  computed: {
    user: function user() {
      return this.$store.state.ActiveUser;
    },
    calculuManual: function calculuManual() {
      if (this.meses <= 9) {
        var porcentaje = 3;
      } else if (this.meses >= 10) {
        var porcentaje = 5;
      }

      var total = parseInt(this.deudas[0].valor) * this.meses;
      var descuento = porcentaje / 100 * total;
      var total_descontado = parseInt(total) - parseInt(descuento);
      return parseInt(total_descontado);
    },
    selectmeses: function selectmeses() {
      var activas = false;
      var arreglo = [];
      var meses = [];
      var arregloLocal = [3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
      this.deudas.forEach(function (item) {
        if (item.status == "inactiva") {
          meses.push(item);
        }

        if (item.status == "preactiva") {
          meses.push(item);
        }

        if (item.status == "activa") {
          activas = true;
        }
      });
      arregloLocal.forEach(function (item) {
        if (item <= meses.length) {
          arreglo.push(item);
        }
      });

      if (meses.length <= 9) {
        this.pago_año = true;
      }

      if (this.deudas.length != meses.length) {
        this.pago_año = true;
      }

      if (activas) {
        return [];
      }

      return arreglo;
    }
  },
  filters: {
    addmiles: function addmiles(nStr) {
      nStr += "";
      var x = nStr.split(".");
      var x1 = x[0];
      var x2 = x.length > 1 ? "." + x[1] : "";
      var rgx = /(\d+)(\d{3})/;

      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, "$1" + "." + "$2");
      }

      return x1 + x2;
    },
    mes_format: function mes_format(fecha) {
      if (fecha == null) {
        return "";
      } else {
        var convertir = function convertir(mes) {
          var numeroMes = parseInt(mes) - parseInt(1);

          if (!isNaN(numeroMes) && numeroMes >= 0 && numeroMes <= 12) {
            return meses[numeroMes];
          }
        };

        var today = new Date(fecha);
        var dd = String(today.getDate()).padStart(2, "0");
        var mm = String(today.getMonth() + 1).padStart(2, "0"); //January is 0!

        var yyyy = today.getFullYear();
        var meses = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];
        today = convertir(mm);
        return today;
      }
    },
    formater_fecha: function formater_fecha(fecha) {
      if (fecha == null) {
        return "";
      } else {
        return moment(fecha).format("DD-MM-YYYY");
      }
    }
  },
  watch: {
    deudas: function deudas(arreglo) {
      var index = 0;
      arreglo.forEach(function (item) {
        if (item.status == "activa") {
          item.local_index = index;
          index = index + 1;
        }
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/acudiente/pension/index.vue?vue&type=style&index=0&id=9ea55df6&scoped=true&lang=css&":
/*!************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/acudiente/pension/index.vue?vue&type=style&index=0&id=9ea55df6&scoped=true&lang=css& ***!
  \************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\ntbody > .text-center[data-v-9ea55df6] {\r\n  display: none !important;\n}\n.pasarela-none[data-v-9ea55df6] {\r\n  display: none !important;\n}\n.btn-sm[data-v-9ea55df6] {\r\n  padding-right: 10px !important;\r\n  padding-bottom: 7px !important;\r\n  padding-left: 10px !important;\r\n  font-family: Arial !important;\r\n  font-weight: 600 !important;\r\n  border-radius: 10px !important;\n}\n.padre[data-v-9ea55df6] {\r\n  height: 88vh;\r\n  /*IMPORTANTE*/\r\n  display: flex;\r\n  justify-content: center;\r\n  align-items: center;\n}\n.hijo[data-v-9ea55df6] {\r\n  width: 430px;\n}\r\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/acudiente/pension/index.vue?vue&type=style&index=0&id=9ea55df6&scoped=true&lang=css&":
/*!****************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/acudiente/pension/index.vue?vue&type=style&index=0&id=9ea55df6&scoped=true&lang=css& ***!
  \****************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../node_modules/css-loader??ref--6-1!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--6-2!../../../../../node_modules/vue-loader/lib??vue-loader-options!./index.vue?vue&type=style&index=0&id=9ea55df6&scoped=true&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/acudiente/pension/index.vue?vue&type=style&index=0&id=9ea55df6&scoped=true&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/acudiente/pension/index.vue?vue&type=template&id=9ea55df6&scoped=true&":
/*!*********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/acudiente/pension/index.vue?vue&type=template&id=9ea55df6&scoped=true& ***!
  \*********************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("section", [
    _vm.matricula_activa == null
      ? _c("div", { staticClass: "padre" }, [_vm._m(0)])
      : _c("div", { staticClass: "container-fluid" }, [
          _c("div", { staticClass: "col-12 d-flex" }, [
            _c(
              "div",
              {
                staticClass: "card card-image",
                staticStyle: {
                  background: "url(/img/pension-fondo.png) round",
                  height: "462px"
                }
              },
              [
                _vm._m(1),
                _vm._v(" "),
                _c("div", { staticClass: "d-flex justify-content-end mr-5" }, [
                  _c(
                    "span",
                    {
                      staticClass: "text-white font-weight-bold",
                      staticStyle: {
                        "font-size": "6vw",
                        "margin-right": "15vw"
                      }
                    },
                    [
                      this.pension_asignada
                        ? [
                            _vm._v(
                              "\n              " +
                                _vm._s(this.pension_asignada.porcentaje) +
                                "%\n            "
                            )
                          ]
                        : [_vm._v(" 0% ")]
                    ],
                    2
                  ),
                  _vm._v(" "),
                  _c("div", { staticClass: "mt-5" }, [
                    _c(
                      "button",
                      {
                        staticClass: "btn btn-primary boton",
                        attrs: { disabled: _vm.pago_año },
                        on: {
                          click: function($event) {
                            return _vm.PreguntaPagoMulti(_vm.deudas.length)
                          }
                        }
                      },
                      [
                        _vm._v(
                          "\n              Pagar todo el año\n            "
                        )
                      ]
                    )
                  ])
                ])
              ]
            )
          ]),
          _vm._v(" "),
          _vm._m(2),
          _vm._v(" "),
          _c("br"),
          _vm._v(" "),
          _vm.selectmeses.length >= 1
            ? _c(
                "div",
                {
                  staticClass:
                    "border-bottom col-12 d-flex justify-content-between mb-2 pb-2"
                },
                [
                  _vm._m(3),
                  _vm._v(" "),
                  _vm._m(4),
                  _vm._v(" "),
                  _c("div", { staticClass: "p-0 m-auto" }, [
                    _c(
                      "select",
                      {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.meses,
                            expression: "meses"
                          }
                        ],
                        staticClass: "browser-default custom-select",
                        staticStyle: { "font-size": "13px", width: "8vw" },
                        on: {
                          change: function($event) {
                            var $$selectedVal = Array.prototype.filter
                              .call($event.target.options, function(o) {
                                return o.selected
                              })
                              .map(function(o) {
                                var val = "_value" in o ? o._value : o.value
                                return val
                              })
                            _vm.meses = $event.target.multiple
                              ? $$selectedVal
                              : $$selectedVal[0]
                          }
                        }
                      },
                      [
                        _c(
                          "option",
                          { attrs: { selected: "", disabled: "" } },
                          [_vm._v("Seleccione")]
                        ),
                        _vm._v(" "),
                        _vm._l(_vm.selectmeses, function(item) {
                          return _c(
                            "option",
                            { key: item, domProps: { value: item } },
                            [
                              _vm._v(
                                "\n            " +
                                  _vm._s(item) +
                                  "\n            "
                              ),
                              _c("span", [_vm._v("Meses")])
                            ]
                          )
                        })
                      ],
                      2
                    )
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "p-0 m-auto" }, [
                    _c("span", { staticClass: "font-weight-normal" }, [
                      _vm._v(
                        "Valor: $ " +
                          _vm._s(_vm._f("addmiles")(_vm.calculuManual))
                      )
                    ])
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "p-0 m-auto" }, [
                    _c(
                      "button",
                      {
                        staticClass: "btn btn-primary boton",
                        on: {
                          click: function($event) {
                            return _vm.PreguntaPagoMulti(_vm.meses)
                          }
                        }
                      },
                      [_vm._v("\n          Pagar\n        ")]
                    )
                  ])
                ]
              )
            : _vm._e(),
          _vm._v(" "),
          _c("br"),
          _vm._v(" "),
          _c("br"),
          _vm._v(" "),
          _c("div", { staticClass: "col-12" }, [
            _c("table", { staticClass: "table" }, [
              _vm._m(5),
              _vm._v(" "),
              _c(
                "tbody",
                _vm._l(_vm.deudas, function(item, index) {
                  return _c("tr", { key: index }, [
                    _c("td", [
                      _vm._v(_vm._s(_vm._f("mes_format")(item.created_at)))
                    ]),
                    _vm._v(" "),
                    _c("td", [
                      _vm._v(_vm._s(_vm._f("formater_fecha")(item.created_at)))
                    ]),
                    _vm._v(" "),
                    _c("td", [_vm._v(_vm._s(_vm._f("addmiles")(item.valor)))]),
                    _vm._v(" "),
                    _c("td", [
                      item.mora == "Si"
                        ? _c("span", [_vm._v(" Si ")])
                        : _c("span", [_vm._v(" No ")])
                    ]),
                    _vm._v(" "),
                    _c(
                      "td",
                      [
                        item.status == "Pagada"
                          ? _c(
                              "button",
                              {
                                staticClass: "btn btn-success btn-sm boton",
                                attrs: { type: "button" }
                              },
                              [
                                _vm._v(
                                  "\n                Pagada\n              "
                                )
                              ]
                            )
                          : item.status == "activa"
                          ? [
                              item.local_index == 0
                                ? _c(
                                    "button",
                                    {
                                      staticClass:
                                        "btn btn-sm boton btn-danger",
                                      attrs: { type: "button" },
                                      on: {
                                        click: function($event) {
                                          return _vm.CuponDescuento(item)
                                        }
                                      }
                                    },
                                    [
                                      _vm._v(
                                        "\n                  Pagar\n                "
                                      )
                                    ]
                                  )
                                : _c(
                                    "button",
                                    {
                                      staticClass:
                                        "btn btn-sm boton btn-danger",
                                      attrs: { type: "button" }
                                    },
                                    [
                                      _vm._v(
                                        "\n                  Pagar\n                "
                                      )
                                    ]
                                  )
                            ]
                          : item.status == "preactiva"
                          ? _c(
                              "button",
                              {
                                staticClass:
                                  "btn btn-outline-blue-grey btn-sm boton",
                                attrs: { type: "button" },
                                on: {
                                  click: function($event) {
                                    return _vm.CuponDescuento(item)
                                  }
                                }
                              },
                              [
                                _vm._v(
                                  "\n                Pagar\n              "
                                )
                              ]
                            )
                          : item.status == "inactiva"
                          ? _c(
                              "button",
                              {
                                staticClass:
                                  "btn btn-sm btn-outline-blue-grey boton",
                                attrs: { type: "button" }
                              },
                              [
                                _vm._v(
                                  "\n                Pendiente\n              "
                                )
                              ]
                            )
                          : _vm._e()
                      ],
                      2
                    )
                  ])
                }),
                0
              )
            ])
          ])
        ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "hijo" }, [
      _c("i", {
        staticClass: "fas fa-info-circle",
        staticStyle: { "font-size": "30px" }
      }),
      _vm._v(" "),
      _c("span", { staticClass: "pl-2" }, [
        _vm._v(" Usted no tiene matrícula activa actualmente.")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "text-right text-white" }, [
      _c(
        "h1",
        {
          staticClass: "h1 text-white",
          staticStyle: {
            "margin-right": "2vw",
            "margin-top": "2vh",
            "font-size": "8vw"
          }
        },
        [_vm._v("\n            Aprovecha!\n          ")]
      ),
      _vm._v(" "),
      _c(
        "h2",
        {
          staticClass: "mb-0",
          staticStyle: { "margin-right": "0.5vw", "font-size": "1.5vw" }
        },
        [
          _vm._v(
            "\n            Despreocúpate de la pensión de tu hijo y ahorra con un rendimiento\n            "
          ),
          _c("br"),
          _vm._v("\n            equivalente al\n          ")
        ]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-12 text-center" }, [
      _c("i", {
        staticClass: "fas fa-info-circle",
        staticStyle: { "font-size": "30px" }
      }),
      _vm._v(" "),
      _c("span", { staticClass: "pl-2" }, [
        _vm._v(
          "\n        Nota: La pensión solo se puede configurar una vez al iniciar el año si no por\n        defecto está a 10 meses.\n      "
        )
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "p-0 m-auto" }, [
      _c("span", { staticClass: "font-weight-normal" }, [
        _vm._v("Pagar mas de 3 meses")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "p-0 m-auto" }, [
      _c("span", { staticClass: "font-weight-normal" }, [
        _vm._v("Ahorra en el valor final pagando más de ")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", { staticClass: "grey lighten-2" }, [
      _c("tr", [
        _c("th", { attrs: { scope: "col" } }, [_vm._v("Mes")]),
        _vm._v(" "),
        _c("th", { attrs: { scope: "col" } }, [_vm._v("Fecha de pago")]),
        _vm._v(" "),
        _c("th", { attrs: { scope: "col" } }, [_vm._v("Valor")]),
        _vm._v(" "),
        _c("th", { attrs: { scope: "col" } }, [_vm._v("Mora")]),
        _vm._v(" "),
        _c("th", { attrs: { scope: "col" } })
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/views/acudiente/pension/index.vue":
/*!********************************************************!*\
  !*** ./resources/js/views/acudiente/pension/index.vue ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _index_vue_vue_type_template_id_9ea55df6_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./index.vue?vue&type=template&id=9ea55df6&scoped=true& */ "./resources/js/views/acudiente/pension/index.vue?vue&type=template&id=9ea55df6&scoped=true&");
/* harmony import */ var _index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./index.vue?vue&type=script&lang=js& */ "./resources/js/views/acudiente/pension/index.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _index_vue_vue_type_style_index_0_id_9ea55df6_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./index.vue?vue&type=style&index=0&id=9ea55df6&scoped=true&lang=css& */ "./resources/js/views/acudiente/pension/index.vue?vue&type=style&index=0&id=9ea55df6&scoped=true&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _index_vue_vue_type_template_id_9ea55df6_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _index_vue_vue_type_template_id_9ea55df6_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "9ea55df6",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/acudiente/pension/index.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/acudiente/pension/index.vue?vue&type=script&lang=js&":
/*!*********************************************************************************!*\
  !*** ./resources/js/views/acudiente/pension/index.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./index.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/acudiente/pension/index.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/acudiente/pension/index.vue?vue&type=style&index=0&id=9ea55df6&scoped=true&lang=css&":
/*!*****************************************************************************************************************!*\
  !*** ./resources/js/views/acudiente/pension/index.vue?vue&type=style&index=0&id=9ea55df6&scoped=true&lang=css& ***!
  \*****************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_id_9ea55df6_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/style-loader!../../../../../node_modules/css-loader??ref--6-1!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--6-2!../../../../../node_modules/vue-loader/lib??vue-loader-options!./index.vue?vue&type=style&index=0&id=9ea55df6&scoped=true&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/acudiente/pension/index.vue?vue&type=style&index=0&id=9ea55df6&scoped=true&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_id_9ea55df6_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_id_9ea55df6_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_id_9ea55df6_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_id_9ea55df6_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_id_9ea55df6_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/views/acudiente/pension/index.vue?vue&type=template&id=9ea55df6&scoped=true&":
/*!***************************************************************************************************!*\
  !*** ./resources/js/views/acudiente/pension/index.vue?vue&type=template&id=9ea55df6&scoped=true& ***!
  \***************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_9ea55df6_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./index.vue?vue&type=template&id=9ea55df6&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/acudiente/pension/index.vue?vue&type=template&id=9ea55df6&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_9ea55df6_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_9ea55df6_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);