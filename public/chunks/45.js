(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[45],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/admin/historico_cartera/detalles.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/admin/historico_cartera/detalles.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _components_data_table_index_vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @/components/data-table/index.vue */ "./resources/js/components/data-table/index.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

var appLoading = document.getElementById("loading-bg");
var options = {
  closeButton: true,
  debug: false,
  newestOnTop: false,
  progressBar: true,
  positionClass: "toast-top-right",
  preventDuplicates: false,
  onclick: null,
  showDuration: "300",
  hideDuration: "1000",
  timeOut: "5000",
  extendedTimeOut: "1000",
  showEasing: "swing",
  hideEasing: "linear",
  showMethod: "fadeIn",
  hideMethod: "fadeOut"
};
/* harmony default export */ __webpack_exports__["default"] = ({
  props: ["acuerdoId"],
  mounted: function mounted() {
    this.Listar();
  },
  components: {
    Fulltable: _components_data_table_index_vue__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  data: function data() {
    return {
      acudiente: {},
      columns: [{
        label: "Servicio"
      }, {
        label: "Descripcion"
      }, {
        label: "Fecha de pago"
      }, {
        label: "Mora"
      }, {
        label: "Valor"
      }]
    };
  },
  methods: {
    Listar: function Listar() {
      var _this = this;

      appLoading.style.display = "block";
      var url = "/api/admin/historico_cartera/user/" + this.acuerdoId;
      axios.post(url).then(function (response) {
        appLoading.style.display = "none";
        _this.acudiente = response.data;

        _this.$refs.fullTable.getProjects();
      });
    }
  },
  computed: {//
  },
  filters: {
    addmiles: function addmiles(nStr) {
      nStr += "";
      var x = nStr.split(".");
      var x1 = x[0];
      var x2 = x.length > 1 ? "." + x[1] : "";
      var rgx = /(\d+)(\d{3})/;

      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, "$1" + "." + "$2");
      }

      return x1 + x2;
    },
    mm_format: function mm_format(fecha) {
      if (fecha == null) {
        return "";
      } else {
        moment.locale("es");
        return moment(fecha).format("MMM");
      }
    },
    formater_fecha: function formater_fecha(fecha) {
      if (fecha == null) {
        return "";
      } else {
        var today = new Date(fecha);
        var dd = String(today.getDate()).padStart(2, "0");
        var mm = String(today.getMonth() + 1).padStart(2, "0"); //January is 0!

        var yyyy = today.getFullYear();
        today = dd + "-" + mm + "-" + yyyy;
        return today;
      }
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/admin/historico_cartera/detalles.vue?vue&type=template&id=37fd3af6&":
/*!******************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/admin/historico_cartera/detalles.vue?vue&type=template&id=37fd3af6& ***!
  \******************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("section", [
    _c(
      "div",
      { staticClass: "container-fluid" },
      [
        _vm._m(0),
        _vm._v(" "),
        _c(
          "div",
          {
            staticClass: "py-3 d-flex",
            staticStyle: { background: "#e9ecef" }
          },
          [
            _c("div", { staticClass: "col px-0 text-center py-2" }, [
              _c("span", [_vm._v(_vm._s(_vm.acudiente.name_estudiante))])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "col px-0 text-center py-2" }, [
              _c("span", [_vm._v("Codígo " + _vm._s(_vm.acudiente.codigo))])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "col px-0 text-center py-2" }, [
              _c("span", [_vm._v(_vm._s(_vm.acudiente.email))])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "col px-0 text-center py-2" }, [
              _c("span", [
                _vm._v("\n          Valor deuda total\n          "),
                _c("br"),
                _vm._v(
                  "\n          $ " +
                    _vm._s(_vm._f("addmiles")(_vm.acudiente.deuda)) +
                    "\n        "
                )
              ])
            ])
          ]
        ),
        _vm._v(" "),
        _c("fulltable", {
          ref: "fullTable",
          attrs: {
            Plist: "/api/admin/historico_cartera/" + _vm.acuerdoId,
            Pcolumns: _vm.columns,
            checkall: false
          },
          scopedSlots: _vm._u([
            {
              key: "tbody-full",
              fn: function(ref) {
                var data = ref.data
                return _vm._l(data, function(item) {
                  return _c("tr", { key: item.id }, [
                    _c("td", [
                      _vm._v(
                        "\n            " +
                          _vm._s(item.servicio) +
                          "\n            "
                      ),
                      item.servicio == "Pension"
                        ? _c("span", [
                            _vm._v(
                              "\n              " +
                                _vm._s(_vm._f("mm_format")(item.created_at)) +
                                "\n            "
                            )
                          ])
                        : _vm._e()
                    ]),
                    _vm._v(" "),
                    _c("td", [_vm._v(_vm._s(item.descripcion || "N/A"))]),
                    _vm._v(" "),
                    _c("td", [
                      _vm._v(_vm._s(_vm._f("formater_fecha")(item.created_at)))
                    ]),
                    _vm._v(" "),
                    _c("td", [
                      item.mora
                        ? _c("span", [_vm._v("Si")])
                        : _c("span", [_vm._v("No")])
                    ]),
                    _vm._v(" "),
                    _c("td", [
                      _vm._v("$ " + _vm._s(_vm._f("addmiles")(item.valor)))
                    ])
                  ])
                })
              }
            }
          ])
        })
      ],
      1
    )
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-12 d-flex mb-2 pb-2 border-bottom" }, [
      _c("i", {
        staticClass: "fal fa-file-alt w-auto",
        staticStyle: { color: "#0f62ac", "font-size": "100px !important" }
      }),
      _vm._v(" "),
      _c(
        "h5",
        {
          staticClass: "ml-2 mt-5",
          staticStyle: { "font-weight": "550 !important" }
        },
        [_vm._v("Detalle de cartera")]
      )
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/views/admin/historico_cartera/detalles.vue":
/*!*****************************************************************!*\
  !*** ./resources/js/views/admin/historico_cartera/detalles.vue ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _detalles_vue_vue_type_template_id_37fd3af6___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./detalles.vue?vue&type=template&id=37fd3af6& */ "./resources/js/views/admin/historico_cartera/detalles.vue?vue&type=template&id=37fd3af6&");
/* harmony import */ var _detalles_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./detalles.vue?vue&type=script&lang=js& */ "./resources/js/views/admin/historico_cartera/detalles.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _detalles_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _detalles_vue_vue_type_template_id_37fd3af6___WEBPACK_IMPORTED_MODULE_0__["render"],
  _detalles_vue_vue_type_template_id_37fd3af6___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/admin/historico_cartera/detalles.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/admin/historico_cartera/detalles.vue?vue&type=script&lang=js&":
/*!******************************************************************************************!*\
  !*** ./resources/js/views/admin/historico_cartera/detalles.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_detalles_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./detalles.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/admin/historico_cartera/detalles.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_detalles_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/admin/historico_cartera/detalles.vue?vue&type=template&id=37fd3af6&":
/*!************************************************************************************************!*\
  !*** ./resources/js/views/admin/historico_cartera/detalles.vue?vue&type=template&id=37fd3af6& ***!
  \************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_detalles_vue_vue_type_template_id_37fd3af6___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./detalles.vue?vue&type=template&id=37fd3af6& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/admin/historico_cartera/detalles.vue?vue&type=template&id=37fd3af6&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_detalles_vue_vue_type_template_id_37fd3af6___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_detalles_vue_vue_type_template_id_37fd3af6___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);