(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[36],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/admin/certificados_solicitados/index.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/admin/certificados_solicitados/index.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _components_data_table_index_vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @/components/data-table/index.vue */ "./resources/js/components/data-table/index.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

var appLoading = document.getElementById("loading-bg");
var options = {
  closeButton: true,
  debug: false,
  newestOnTop: false,
  progressBar: true,
  positionClass: "toast-top-right",
  preventDuplicates: false,
  onclick: null,
  showDuration: "300",
  hideDuration: "1000",
  timeOut: "5000",
  extendedTimeOut: "1000",
  showEasing: "swing",
  hideEasing: "linear",
  showMethod: "fadeIn",
  hideMethod: "fadeOut"
};
/* harmony default export */ __webpack_exports__["default"] = ({
  mounted: function mounted() {
    this.Listar();
  },
  components: {
    Fulltable: _components_data_table_index_vue__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  data: function data() {
    return {
      certificado: null,
      columns: [{
        label: "Nombre"
      }, {
        label: "Codigo"
      }, {
        label: "Fecha solicitud"
      }, {
        label: "Tipo de doc"
      }, {
        label: "Motivo"
      }, {
        label: "Estado"
      }, {
        label: "Acción"
      }]
    };
  },
  methods: {
    onFileSelected: function onFileSelected(event) {
      this.certificado = event.target.files[0];
    },
    Listar: function Listar() {
      this.$refs.fullTable.getProjects();
    },
    RetiroListo: function RetiroListo(id) {
      var _this = this;

      swal({
        title: "Estas seguro de continuar?",
        icon: "warning",
        buttons: true
      }).then(function (willDelete) {
        if (willDelete) {
          appLoading.style.display = "block";
          var url = "/api/admin/informe_pagos/certificados/" + id;
          axios.post(url).then(function (response) {
            _this.Listar();

            appLoading.style.display = "none";
          })["catch"](function (e) {
            appLoading.style.display = "none";

            if (e.response) {
              var errors = e.response.data.errors;
              var noti = _this.$toastr.error;
              $.each(errors, function (i) {
                noti(errors[i][0], "Errores", options);
              });
            }
          });
        }
      });
    },
    SaveCertificado: function SaveCertificado(id) {
      var _this2 = this;

      appLoading.style.display = "block";
      var url = "/api/admin/informe_pagos/certificados/pdf/" + id;
      var datos = new FormData();
      datos.append("pdf", this.certificado);
      datos.append("_method", "post");
      axios.post(url, datos).then(function (response) {
        appLoading.style.display = "none";

        _this2.Listar();
      })["catch"](function (e) {
        appLoading.style.display = "none";
        var errors = e.response.data.errors;
        var noti = _this2.$toastr.error;
        $.each(errors, function (i) {
          noti(errors[i][0], "Errores", options);
        });
      });
    }
  },
  filters: {
    formater_fecha: function formater_fecha(fecha) {
      if (fecha == null) {
        return "";
      } else {
        var today = new Date(fecha);
        var dd = String(today.getDate()).padStart(2, "0");
        var mm = String(today.getMonth() + 1).padStart(2, "0"); //January is 0!

        var yyyy = today.getFullYear();
        today = dd + "-" + mm + "-" + yyyy;
        return today;
      }
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/admin/certificados_solicitados/index.vue?vue&type=style&index=0&id=a18c137e&scoped=true&lang=css&":
/*!*************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/admin/certificados_solicitados/index.vue?vue&type=style&index=0&id=a18c137e&scoped=true&lang=css& ***!
  \*************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.dropdown-menu[data-v-a18c137e] {\r\n  left: -13vw !important;\n}\ntbody > .text-center[data-v-a18c137e] {\r\n  display: none !important;\n}\n.btn-sm[data-v-a18c137e] {\r\n  padding-right: 10px !important;\r\n  padding-bottom: 7px !important;\r\n  padding-left: 10px !important;\r\n  font-family: Arial !important;\r\n  font-weight: 600 !important;\r\n  border-radius: 10px !important;\n}\r\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/admin/certificados_solicitados/index.vue?vue&type=style&index=0&id=a18c137e&scoped=true&lang=css&":
/*!*****************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/admin/certificados_solicitados/index.vue?vue&type=style&index=0&id=a18c137e&scoped=true&lang=css& ***!
  \*****************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../node_modules/css-loader??ref--6-1!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--6-2!../../../../../node_modules/vue-loader/lib??vue-loader-options!./index.vue?vue&type=style&index=0&id=a18c137e&scoped=true&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/admin/certificados_solicitados/index.vue?vue&type=style&index=0&id=a18c137e&scoped=true&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/admin/certificados_solicitados/index.vue?vue&type=template&id=a18c137e&scoped=true&":
/*!**********************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/admin/certificados_solicitados/index.vue?vue&type=template&id=a18c137e&scoped=true& ***!
  \**********************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("section", [
    _c(
      "div",
      { staticClass: "container-fluid" },
      [
        _vm._m(0),
        _vm._v(" "),
        _c("fulltable", {
          ref: "fullTable",
          attrs: {
            Plist: "/api/admin/informe_pagos/certificados",
            Pcolumns: _vm.columns,
            checkall: false
          },
          scopedSlots: _vm._u([
            {
              key: "tbody-full",
              fn: function(ref) {
                var data = ref.data
                return _vm._l(data, function(item) {
                  return _c("tr", { key: item.id }, [
                    _c("td", [_vm._v(_vm._s(item.name))]),
                    _vm._v(" "),
                    _c("td", [_vm._v(_vm._s(item.codigo))]),
                    _vm._v(" "),
                    _c("td", [
                      _vm._v(_vm._s(_vm._f("formater_fecha")(item.created_at)))
                    ]),
                    _vm._v(" "),
                    _c("td", [_vm._v(_vm._s(item.concepto))]),
                    _vm._v(" "),
                    _c("td", [_vm._v(_vm._s(item.certificado_motivo))]),
                    _vm._v(" "),
                    _c("td", { staticClass: "text-center" }, [
                      item.certificado_estado == "Procesado"
                        ? _c(
                            "button",
                            { staticClass: "btn btn-sm btn-success" },
                            [
                              _vm._v(
                                "\n              " +
                                  _vm._s(item.certificado_estado) +
                                  "\n            "
                              )
                            ]
                          )
                        : _c(
                            "button",
                            { staticClass: "btn btn-sm btn-danger" },
                            [
                              _vm._v(
                                "\n              " +
                                  _vm._s(item.certificado_estado) +
                                  "\n            "
                              )
                            ]
                          )
                    ]),
                    _vm._v(" "),
                    item.certificado_estado != "Procesado"
                      ? _c("td", { staticClass: "text-center" }, [
                          item.canal == "Retiro en caja"
                            ? _c(
                                "button",
                                {
                                  staticClass: "btn btn-sm btn-primary boton",
                                  on: {
                                    click: function($event) {
                                      return _vm.RetiroListo(item.id)
                                    }
                                  }
                                },
                                [
                                  _vm._v(
                                    "\n              Listo para retirar\n            "
                                  )
                                ]
                              )
                            : _c("div", { staticClass: "dropdown" }, [
                                _c(
                                  "a",
                                  {
                                    staticClass:
                                      "btn btn-sm btn-primary boton dropdown-toggle",
                                    attrs: {
                                      href: "#",
                                      role: "button",
                                      id: "dropdownMenuLink",
                                      "data-toggle": "dropdown",
                                      "aria-haspopup": "true",
                                      "aria-expanded": "false"
                                    }
                                  },
                                  [
                                    _vm._v(
                                      "\n                Subir certificado\n              "
                                    )
                                  ]
                                ),
                                _vm._v(" "),
                                _c("div", { staticClass: "dropdown-menu" }, [
                                  _c("div", { staticClass: "px-4 py-3" }, [
                                    _c("div", { staticClass: "form-group" }, [
                                      _c("label", [_vm._v("Certificado")]),
                                      _vm._v(" "),
                                      _c("input", {
                                        staticClass: "form-control",
                                        attrs: { type: "file", accept: ".pdf" },
                                        on: { change: _vm.onFileSelected }
                                      })
                                    ]),
                                    _vm._v(" "),
                                    _c(
                                      "button",
                                      {
                                        staticClass: "boton btn btn-primary",
                                        on: {
                                          click: function($event) {
                                            return _vm.SaveCertificado(item.id)
                                          }
                                        }
                                      },
                                      [
                                        _vm._v(
                                          "\n                    Subir\n                  "
                                        )
                                      ]
                                    )
                                  ])
                                ])
                              ])
                        ])
                      : _c("td", { staticClass: "text-center" }, [
                          item.canal == "Retiro en caja"
                            ? _c("span", [_vm._v("Listo para retirar")])
                            : _c("span", [_vm._v("Certificado cargado")])
                        ])
                  ])
                })
              }
            }
          ])
        })
      ],
      1
    )
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-12 d-flex mb-2 pb-2 border-bottom" }, [
      _c("i", {
        staticClass: "fal fa-file-alt w-auto",
        staticStyle: { color: "#0f62ac", "font-size": "100px !important" }
      }),
      _vm._v(" "),
      _c(
        "h5",
        {
          staticClass: "ml-2 mt-5",
          staticStyle: { "font-weight": "550 !important" }
        },
        [_vm._v("\n        Certificados solicitados\n      ")]
      )
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/views/admin/certificados_solicitados/index.vue":
/*!*********************************************************************!*\
  !*** ./resources/js/views/admin/certificados_solicitados/index.vue ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _index_vue_vue_type_template_id_a18c137e_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./index.vue?vue&type=template&id=a18c137e&scoped=true& */ "./resources/js/views/admin/certificados_solicitados/index.vue?vue&type=template&id=a18c137e&scoped=true&");
/* harmony import */ var _index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./index.vue?vue&type=script&lang=js& */ "./resources/js/views/admin/certificados_solicitados/index.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _index_vue_vue_type_style_index_0_id_a18c137e_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./index.vue?vue&type=style&index=0&id=a18c137e&scoped=true&lang=css& */ "./resources/js/views/admin/certificados_solicitados/index.vue?vue&type=style&index=0&id=a18c137e&scoped=true&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _index_vue_vue_type_template_id_a18c137e_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _index_vue_vue_type_template_id_a18c137e_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "a18c137e",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/admin/certificados_solicitados/index.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/admin/certificados_solicitados/index.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************!*\
  !*** ./resources/js/views/admin/certificados_solicitados/index.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./index.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/admin/certificados_solicitados/index.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/admin/certificados_solicitados/index.vue?vue&type=style&index=0&id=a18c137e&scoped=true&lang=css&":
/*!******************************************************************************************************************************!*\
  !*** ./resources/js/views/admin/certificados_solicitados/index.vue?vue&type=style&index=0&id=a18c137e&scoped=true&lang=css& ***!
  \******************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_id_a18c137e_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/style-loader!../../../../../node_modules/css-loader??ref--6-1!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--6-2!../../../../../node_modules/vue-loader/lib??vue-loader-options!./index.vue?vue&type=style&index=0&id=a18c137e&scoped=true&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/admin/certificados_solicitados/index.vue?vue&type=style&index=0&id=a18c137e&scoped=true&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_id_a18c137e_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_id_a18c137e_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_id_a18c137e_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_id_a18c137e_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_style_index_0_id_a18c137e_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/views/admin/certificados_solicitados/index.vue?vue&type=template&id=a18c137e&scoped=true&":
/*!****************************************************************************************************************!*\
  !*** ./resources/js/views/admin/certificados_solicitados/index.vue?vue&type=template&id=a18c137e&scoped=true& ***!
  \****************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_a18c137e_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./index.vue?vue&type=template&id=a18c137e&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/admin/certificados_solicitados/index.vue?vue&type=template&id=a18c137e&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_a18c137e_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_index_vue_vue_type_template_id_a18c137e_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);