(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[43],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/pages/StatusSolicitud.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/pages/StatusSolicitud.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
var appLoading = document.getElementById("loading-bg");
/* harmony default export */ __webpack_exports__["default"] = ({
  props: ["codigounico"],
  created: function created() {
    appLoading.style.display = "block";
    this.VerSolicitud(this.codigounico);
  },
  data: function data() {
    return {
      solicitud: {}
    };
  },
  methods: {
    VerSolicitud: function VerSolicitud(codigounico) {
      var _this = this;

      var url = "/api/solicitar/certificado/" + codigounico;
      axios.post(url).then(function (response) {
        appLoading.style.display = "none";
        _this.solicitud = response.data;
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/pages/StatusSolicitud.vue?vue&type=style&index=0&id=e154b910&scoped=true&lang=css&":
/*!**********************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/pages/StatusSolicitud.vue?vue&type=style&index=0&id=e154b910&scoped=true&lang=css& ***!
  \**********************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.bodyapp[data-v-e154b910] {\n  background-image: url(\"/img/fondo.png\") !important;\n  background-repeat: no-repeat !important;\n  background-size: 80% !important;\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/pages/StatusSolicitud.vue?vue&type=style&index=0&id=e154b910&scoped=true&lang=css&":
/*!**************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/pages/StatusSolicitud.vue?vue&type=style&index=0&id=e154b910&scoped=true&lang=css& ***!
  \**************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./StatusSolicitud.vue?vue&type=style&index=0&id=e154b910&scoped=true&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/pages/StatusSolicitud.vue?vue&type=style&index=0&id=e154b910&scoped=true&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/pages/StatusSolicitud.vue?vue&type=template&id=e154b910&scoped=true&":
/*!*******************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/pages/StatusSolicitud.vue?vue&type=template&id=e154b910&scoped=true& ***!
  \*******************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "bodyapp" }, [
    _c("div", { staticClass: "container" }, [
      _c("div", { staticClass: "container-fluid" }, [
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-6" }),
          _vm._v(" "),
          _c("div", { staticClass: "col-6 text-center" }, [
            _vm._m(0),
            _vm._v(" "),
            _c("br"),
            _vm._v(" "),
            _c(
              "h1",
              { staticStyle: { "font-weight": "bold", color: "#ff8d00" } },
              [
                _vm._v(
                  "\n            Solicitud: " +
                    _vm._s(_vm.solicitud.status) +
                    "\n          "
                )
              ]
            ),
            _vm._v(" "),
            _c(
              "h3",
              {
                staticClass: "mb-0",
                staticStyle: {
                  "font-size": "1.6vw",
                  color: "#2196f3",
                  "font-weight": "400"
                }
              },
              [
                _vm._v(
                  "\n            Certificado solicitado: " +
                    _vm._s(_vm.solicitud.tipo_documento_name) +
                    "\n          "
                )
              ]
            ),
            _vm._v(" "),
            _c(
              "h3",
              {
                staticClass: "mb-3",
                staticStyle: {
                  "font-size": "1.6vw",
                  color: "#2196f3",
                  "font-weight": "400"
                }
              },
              [
                _vm._v(
                  "\n            Código de solicitud: " +
                    _vm._s(_vm.solicitud.codigounico) +
                    "\n          "
                )
              ]
            ),
            _vm._v(" "),
            _c(
              "h3",
              {
                staticClass: "mb-3",
                staticStyle: {
                  "font-size": "1.6vw",
                  color: "#2196f3",
                  "font-weight": "400"
                }
              },
              [
                _vm._v(
                  "\n            Nombre de convenio para pago en línea: COLEGIO CATOLICO CALI BACHILLERATO\n          "
                )
              ]
            ),
            _vm._v(" "),
            _c(
              "h3",
              {
                staticClass: "mb-0",
                staticStyle: {
                  "font-size": "1vw",
                  color: "#2196f3",
                  "font-weight": "400"
                }
              },
              [
                _vm._v(
                  "\n            Favor tome nota del código y el siguiente link para hacer seguimiento a su\n            solicitud.\n            "
                ),
                _c("br"),
                _vm._v(
                  "\n            https://pasareladepagos.colegiocatolicocali.edu.co/solicitud-" +
                    _vm._s(_vm.solicitud.codigounico) +
                    "\n          "
                )
              ]
            ),
            _vm._v(" "),
            _c("br"),
            _vm._v(" "),
            _c("br"),
            _vm._v(" "),
            _vm.solicitud.status == "Pendiente de pago"
              ? _c(
                  "a",
                  {
                    staticClass: "btn btn-primary",
                    staticStyle: {
                      "background-image": "linear-gradient(#2196f3, #3f51b5)",
                      "font-size": "23px",
                      border: "none",
                      padding: "13px 25px"
                    },
                    attrs: {
                      target: "_blank",
                      href:
                        "https://www.pagosvirtualesavvillas.com.co/personal/pagos/",
                      disabled: ""
                    }
                  },
                  [
                    _vm._v(
                      "\n            Precio a pagar $ " +
                        _vm._s(_vm.solicitud.valor_pagar) +
                        "\n          "
                    )
                  ]
                )
              : _vm._e(),
            _vm._v(" "),
            _c("br"),
            _vm._v(" "),
            _c("br"),
            _vm._v(" "),
            _vm._m(1),
            _vm._v(" "),
            _c("br"),
            _vm._v(" "),
            _c("small", { staticStyle: { color: "#2196f3" } }, [
              _vm._v(
                "Cra 49 # 9b-42 Cali - Col. | Tel: 552 0795 - 315 549 7690"
              )
            ]),
            _vm._v(" "),
            _c("br"),
            _vm._v(" "),
            _c("br"),
            _vm._v(" "),
            _c("br")
          ])
        ])
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("a", { attrs: { href: "http://colegiocatolicocali.edu.co/" } }, [
      _c("img", {
        staticClass: "img-fluid",
        staticStyle: {
          width: "10vw",
          display: "block",
          margin: "auto",
          "margin-top": "3vw"
        },
        attrs: { src: "/img/logo.png" }
      })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("a", { attrs: { href: "http://colegiocatolicocali.edu.co/" } }, [
      _c("small", { staticStyle: { color: "#2196f3" } }, [
        _vm._v("WWW.COLEGIOCOTOLICOCALI.EDU.CO")
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/views/pages/StatusSolicitud.vue":
/*!******************************************************!*\
  !*** ./resources/js/views/pages/StatusSolicitud.vue ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _StatusSolicitud_vue_vue_type_template_id_e154b910_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./StatusSolicitud.vue?vue&type=template&id=e154b910&scoped=true& */ "./resources/js/views/pages/StatusSolicitud.vue?vue&type=template&id=e154b910&scoped=true&");
/* harmony import */ var _StatusSolicitud_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./StatusSolicitud.vue?vue&type=script&lang=js& */ "./resources/js/views/pages/StatusSolicitud.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _StatusSolicitud_vue_vue_type_style_index_0_id_e154b910_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./StatusSolicitud.vue?vue&type=style&index=0&id=e154b910&scoped=true&lang=css& */ "./resources/js/views/pages/StatusSolicitud.vue?vue&type=style&index=0&id=e154b910&scoped=true&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _StatusSolicitud_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _StatusSolicitud_vue_vue_type_template_id_e154b910_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _StatusSolicitud_vue_vue_type_template_id_e154b910_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "e154b910",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/pages/StatusSolicitud.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/pages/StatusSolicitud.vue?vue&type=script&lang=js&":
/*!*******************************************************************************!*\
  !*** ./resources/js/views/pages/StatusSolicitud.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_StatusSolicitud_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./StatusSolicitud.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/pages/StatusSolicitud.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_StatusSolicitud_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/pages/StatusSolicitud.vue?vue&type=style&index=0&id=e154b910&scoped=true&lang=css&":
/*!***************************************************************************************************************!*\
  !*** ./resources/js/views/pages/StatusSolicitud.vue?vue&type=style&index=0&id=e154b910&scoped=true&lang=css& ***!
  \***************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_StatusSolicitud_vue_vue_type_style_index_0_id_e154b910_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader!../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./StatusSolicitud.vue?vue&type=style&index=0&id=e154b910&scoped=true&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/pages/StatusSolicitud.vue?vue&type=style&index=0&id=e154b910&scoped=true&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_StatusSolicitud_vue_vue_type_style_index_0_id_e154b910_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_StatusSolicitud_vue_vue_type_style_index_0_id_e154b910_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_StatusSolicitud_vue_vue_type_style_index_0_id_e154b910_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_StatusSolicitud_vue_vue_type_style_index_0_id_e154b910_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_StatusSolicitud_vue_vue_type_style_index_0_id_e154b910_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/views/pages/StatusSolicitud.vue?vue&type=template&id=e154b910&scoped=true&":
/*!*************************************************************************************************!*\
  !*** ./resources/js/views/pages/StatusSolicitud.vue?vue&type=template&id=e154b910&scoped=true& ***!
  \*************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_StatusSolicitud_vue_vue_type_template_id_e154b910_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./StatusSolicitud.vue?vue&type=template&id=e154b910&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/pages/StatusSolicitud.vue?vue&type=template&id=e154b910&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_StatusSolicitud_vue_vue_type_template_id_e154b910_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_StatusSolicitud_vue_vue_type_template_id_e154b910_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);