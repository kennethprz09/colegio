(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[34],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/admin/acuerdo_pagos/detalles.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/admin/acuerdo_pagos/detalles.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _components_data_table_index_vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @/components/data-table/index.vue */ "./resources/js/components/data-table/index.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

var appLoading = document.getElementById("loading-bg");
var options = {
  closeButton: true,
  debug: false,
  newestOnTop: false,
  progressBar: true,
  positionClass: "toast-top-right",
  preventDuplicates: false,
  onclick: null,
  showDuration: "300",
  hideDuration: "1000",
  timeOut: "5000",
  extendedTimeOut: "1000",
  showEasing: "swing",
  hideEasing: "linear",
  showMethod: "fadeIn",
  hideMethod: "fadeOut"
};
/* harmony default export */ __webpack_exports__["default"] = ({
  props: ["acuerdoId"],
  mounted: function mounted() {
    this.List();
  },
  components: {
    Fulltable: _components_data_table_index_vue__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  data: function data() {
    return {
      acuerdo: {},
      file_pdf: null,
      itemChecks: [],
      columns: [{
        label: "Mes"
      }, {
        label: "Fecha de pago"
      }, {
        label: "Valor"
      }, {
        label: "Estado"
      }]
    };
  },
  methods: {
    List: function List() {
      var _this = this;

      appLoading.style.display = "block";
      var url = "/api/admin/acuerdo_pagos/" + this.acuerdoId;
      axios.post(url).then(function (response) {
        appLoading.style.display = "none";
        _this.acuerdo = response.data;
      });
    },
    Aprobar_acuerdo: function Aprobar_acuerdo(id) {
      var _this2 = this;

      swal({
        title: "Estas seguro de aprobar el acuerdo actual?",
        icon: "warning",
        dangerMode: true,
        buttons: {
          cancel: "Cancelar",
          defeat: "Si, aprobar"
        }
      }).then(function (willDelete) {
        if (willDelete) {
          appLoading.style.display = "block";
          var url = "/api/admin/acuerdo_pagos/aprobar/" + id;
          axios.put(url).then(function (response) {
            _this2.List();
          });
        }
      });
    },
    Autenticar_acuerdo: function Autenticar_acuerdo(id) {
      var _this3 = this;

      swal({
        title: "Estas seguro de autenticar el acuerdo actual?",
        icon: "warning",
        dangerMode: true,
        buttons: {
          cancel: "Cancelar",
          defeat: "Si, autenticar"
        }
      }).then(function (willDelete) {
        if (willDelete) {
          appLoading.style.display = "block";
          var url = "/api/admin/acuerdo_pagos/autenticar/" + id;
          axios.put(url).then(function (response) {
            _this3.List();
          });
        }
      });
    },
    Rechazar_acuerdo: function Rechazar_acuerdo(id) {
      var _this4 = this;

      swal({
        title: "Estas seguro de rechazar el acuerdo actual?",
        icon: "warning",
        dangerMode: true,
        buttons: {
          cancel: "Cancelar",
          defeat: "Si, rechazar"
        }
      }).then(function (willDelete) {
        if (willDelete) {
          appLoading.style.display = "block";
          var url = "/api/admin/acuerdo_pagos/rechazar/" + id;
          axios.put(url).then(function (response) {
            _this4.List();
          });
        }
      });
    },
    Finalizar_acuerdo: function Finalizar_acuerdo(id) {
      var _this5 = this;

      swal({
        title: "Estas seguro de finalizar el acuerdo actual?",
        icon: "warning",
        dangerMode: true,
        buttons: {
          cancel: "Cancelar",
          defeat: "Si, finalizar"
        }
      }).then(function (willDelete) {
        if (willDelete) {
          appLoading.style.display = "block";
          var url = "/api/admin/acuerdo_pagos/finalizar/" + id;
          axios.put(url).then(function (response) {
            _this5.$router.push("/admin/acuerdo_pagos");
          });
        }
      });
    },
    Borrar_acuerdo: function Borrar_acuerdo(id) {
      var _this6 = this;

      swal({
        title: "Estas seguro de borrar el acuerdo actual?",
        text: "Si es el caso recordar pasar los pagos a pendientes antes de continua ",
        icon: "warning",
        dangerMode: true,
        buttons: {
          cancel: "Cancelar",
          defeat: "Si, borrar"
        }
      }).then(function (willDelete) {
        if (willDelete) {
          appLoading.style.display = "block";
          var url = "/api/admin/acuerdo_pagos/finalizar/" + id;
          axios.put(url).then(function (response) {
            _this6.$router.push("/admin/acuerdo_pagos");
          });
        }
      });
    },
    onFileSelected: function onFileSelected(event) {
      this.file_pdf = event.target.files[0];
    },
    subirPdf: function subirPdf(file_pdf) {
      var _this7 = this;

      appLoading.style.display = "block";
      var url = "/api/admin/acuerdo_pagos/pdf/" + this.acuerdoId;
      var datos = new FormData();
      datos.append("pdf", file_pdf);
      axios.post(url, datos).then(function (response) {
        _this7.List();
      });
    }
  },
  computed: {
    btn_status: function btn_status() {
      if (this.acuerdo.status == "Pendiente") {
        return false;
      }

      return true;
    },
    btn_status_pdf: function btn_status_pdf() {
      if (this.acuerdo.pdf == null) {
        return false;
      }

      return true;
    },
    btn_pdf: function btn_pdf() {
      if (this.acuerdo.status == "Aprobado") {
        return false;
      }

      return true;
    }
  },
  filters: {
    addmiles: function addmiles(nStr) {
      nStr += "";
      var x = nStr.split(".");
      var x1 = x[0];
      var x2 = x.length > 1 ? "." + x[1] : "";
      var rgx = /(\d+)(\d{3})/;

      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, "$1" + "." + "$2");
      }

      return x1 + x2;
    },
    dia_format: function dia_format(fecha) {
      if (fecha == null) {
        return "";
      } else {
        var today = moment(fecha).format("DD");
        return today;
      }
    },
    mm_format: function mm_format(fecha) {
      if (fecha == null) {
        return "";
      } else {
        var convertir = function convertir(mes) {
          var numeroMes = parseInt(mes);

          if (!isNaN(numeroMes) && numeroMes >= 1 && numeroMes <= 12) {
            return meses[numeroMes];
          }
        };

        var today = new Date(fecha);
        var dd = String(today.getDate()).padStart(2, "0");
        var mm = String(today.getMonth() + 1).padStart(2, "0"); //January is 0!

        var yyyy = today.getFullYear();
        var meses = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];
        today = convertir(mm);
        return today;
      }
    },
    formater_fecha: function formater_fecha(fecha) {
      if (fecha == null) {
        return "";
      } else {
        var today = new Date(fecha);
        var dd = String(today.getDate()).padStart(2, "0");
        var mm = String(today.getMonth() + 1).padStart(2, "0"); //January is 0!

        var yyyy = today.getFullYear();
        today = dd + "-" + mm + "-" + yyyy;
        return today;
      }
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/admin/acuerdo_pagos/detalles.vue?vue&type=style&index=0&lang=css&":
/*!*****************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/admin/acuerdo_pagos/detalles.vue?vue&type=style&index=0&lang=css& ***!
  \*****************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n#tablemensaje {\r\n  display: none !important;\n}\r\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/admin/acuerdo_pagos/detalles.vue?vue&type=style&index=0&lang=css&":
/*!*********************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/admin/acuerdo_pagos/detalles.vue?vue&type=style&index=0&lang=css& ***!
  \*********************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../node_modules/css-loader??ref--6-1!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--6-2!../../../../../node_modules/vue-loader/lib??vue-loader-options!./detalles.vue?vue&type=style&index=0&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/admin/acuerdo_pagos/detalles.vue?vue&type=style&index=0&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/admin/acuerdo_pagos/detalles.vue?vue&type=template&id=04a93bc4&":
/*!**************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/admin/acuerdo_pagos/detalles.vue?vue&type=template&id=04a93bc4& ***!
  \**************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("section", [
    _c(
      "div",
      { staticClass: "container-fluid" },
      [
        _vm._m(0),
        _vm._v(" "),
        _c(
          "div",
          {
            staticClass: "py-3 d-flex",
            staticStyle: { background: "#e9ecef" }
          },
          [
            _c("div", { staticClass: "col px-0 text-center py-2" }, [
              _c("span", [_vm._v(_vm._s(_vm.acuerdo.acudiente_name))])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "col px-0 text-center py-2" }, [
              _c("span", [
                _vm._v("Codígo " + _vm._s(_vm.acuerdo.acudiente_codigo))
              ])
            ]),
            _vm._v(" "),
            _vm.acuerdo.status == "Aprobado" ||
            _vm.acuerdo.status == "Autenticado"
              ? _c("div", { staticClass: "col px-0 text-center py-2" }, [
                  _vm.acuerdo.pdf == null
                    ? _c("div", { staticClass: "dropdown" }, [
                        _c(
                          "button",
                          {
                            staticClass:
                              "btn btn-primary btn-sm boton dropdown-toggle",
                            attrs: {
                              id: "dropdownMenuLink",
                              "data-toggle": "dropdown",
                              "aria-haspopup": "true",
                              "aria-expanded": "false"
                            }
                          },
                          [_vm._v("\n            Subir pdf\n          ")]
                        ),
                        _vm._v(" "),
                        _c("div", { staticClass: "dropdown-menu" }, [
                          _c("div", { staticClass: "py-3" }, [
                            _c("div", { staticClass: "input-group" }, [
                              _c("div", { staticClass: "custom-file" }, [
                                _c("input", {
                                  staticClass: "custom-file-input",
                                  attrs: {
                                    type: "file",
                                    id: "inputGroupFile01",
                                    "aria-describedby": "inputGroupFileAddon01",
                                    accept: ".pdf"
                                  },
                                  on: { change: _vm.onFileSelected }
                                }),
                                _vm._v(" "),
                                _c(
                                  "label",
                                  {
                                    staticClass: "custom-file-label",
                                    attrs: { for: "inputGroupFile01" }
                                  },
                                  [_vm._v("Acuerdo de pago")]
                                )
                              ])
                            ]),
                            _vm._v(" "),
                            _c(
                              "button",
                              {
                                staticClass:
                                  "boton btn btn-primary btn-sm float-right",
                                on: {
                                  click: function($event) {
                                    return _vm.subirPdf(_vm.file_pdf)
                                  }
                                }
                              },
                              [
                                _vm._v(
                                  "\n                Subir\n              "
                                )
                              ]
                            )
                          ])
                        ])
                      ])
                    : _c(
                        "a",
                        {
                          staticClass: "btn btn-primary btn-sm boton",
                          attrs: {
                            type: "button",
                            target: "_blank",
                            href: "/acuerdos/" + _vm.acuerdo.pdf
                          }
                        },
                        [_vm._v("\n          Ver Pdf\n        ")]
                      )
                ])
              : _vm._e(),
            _vm._v(" "),
            _c(
              "div",
              { staticClass: "col px-0 text-center py-2" },
              [
                !_vm.btn_status
                  ? [
                      _c(
                        "button",
                        {
                          staticClass: "btn btn-primary btn-sm boton",
                          attrs: { type: "button", disabled: _vm.btn_status },
                          on: {
                            click: function($event) {
                              return _vm.Aprobar_acuerdo(_vm.acuerdo.id)
                            }
                          }
                        },
                        [_vm._v("\n            Aprobar\n          ")]
                      ),
                      _vm._v(" "),
                      _c(
                        "button",
                        {
                          staticClass: "btn btn-warning btn-sm boton",
                          attrs: { type: "button", disabled: _vm.btn_status },
                          on: {
                            click: function($event) {
                              return _vm.Rechazar_acuerdo(_vm.acuerdo.id)
                            }
                          }
                        },
                        [_vm._v("\n            Rechazar\n          ")]
                      )
                    ]
                  : [
                      _vm.acuerdo.status == "Aprobado"
                        ? _c(
                            "button",
                            {
                              staticClass: "btn btn-primary btn-sm boton",
                              attrs: {
                                type: "button",
                                disabled: !_vm.btn_status_pdf
                              },
                              on: {
                                click: function($event) {
                                  return _vm.Autenticar_acuerdo(_vm.acuerdo.id)
                                }
                              }
                            },
                            [_vm._v("\n            Autenticar\n          ")]
                          )
                        : _vm._e(),
                      _vm._v(" "),
                      _vm.acuerdo.status != "Autenticado"
                        ? _c(
                            "button",
                            {
                              staticClass: "btn btn-warning btn-sm boton",
                              attrs: {
                                type: "button",
                                disabled: !_vm.btn_status
                              },
                              on: {
                                click: function($event) {
                                  return _vm.Finalizar_acuerdo(_vm.acuerdo.id)
                                }
                              }
                            },
                            [_vm._v("\n            Finalizar\n          ")]
                          )
                        : _vm._e()
                    ]
              ],
              2
            ),
            _vm._v(" "),
            _c(
              "button",
              {
                staticClass: "btn btn-danger btn-submit-dwqa boton",
                attrs: { type: "button" },
                on: {
                  click: function($event) {
                    return _vm.Borrar_acuerdo(_vm.acuerdo.id)
                  }
                }
              },
              [_vm._v("\n        Borrar\n      ")]
            )
          ]
        ),
        _vm._v(" "),
        _c("br"),
        _vm._v(" "),
        _c(
          "div",
          {
            staticClass: "py-3 d-flex",
            staticStyle: { background: "#e9ecef" }
          },
          [
            _c("div", { staticClass: "col px-0 text-center py-2" }, [
              _c("label", [_vm._v("Deuda total de cartera")]),
              _vm._v(" "),
              _c("br"),
              _vm._v(" "),
              _c("span", [
                _vm._v(
                  "$ " + _vm._s(_vm._f("addmiles")(_vm.acuerdo.deuda_total))
                )
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "col px-0 text-center py-2" }, [
              _c("label", [_vm._v("Cuotas")]),
              _vm._v(" "),
              _c("br"),
              _vm._v(" "),
              _c("span", [_vm._v(_vm._s(_vm.acuerdo.cantidad_cuotas))])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "col px-0 text-center py-2" }, [
              _c("label", [_vm._v("Dia de pago")]),
              _vm._v(" "),
              _c("br"),
              _vm._v(" "),
              _c("span", [
                _vm._v(
                  _vm._s(_vm._f("dia_format")(_vm.acuerdo.fecha_at)) +
                    " de cada mes"
                )
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "col px-0 text-center py-2" }, [
              _c("label", [_vm._v("Valor de cada cuota")]),
              _vm._v(" "),
              _c("br"),
              _vm._v(" "),
              _c("span", [
                _vm._v(
                  "$ " + _vm._s(_vm._f("addmiles")(_vm.acuerdo.deuda_cuotas))
                )
              ])
            ])
          ]
        ),
        _vm._v(" "),
        _c(
          "fulltable",
          {
            ref: "fullTable",
            attrs: {
              Plist: null,
              Pcolumns: _vm.columns,
              Pcheck: _vm.itemChecks,
              checkall: false,
              filtro: false,
              buscador: false
            },
            on: {
              clearchecks: function($event) {
                _vm.itemChecks = []
              }
            }
          },
          [
            _c(
              "template",
              { slot: "tbody-full" },
              _vm._l(_vm.acuerdo.cuotas, function(item) {
                return _c("tr", { key: item.id }, [
                  _c("td", [_vm._v(_vm._s(_vm._f("mm_format")(item.mes)))]),
                  _vm._v(" "),
                  _c("td", [
                    _vm._v(_vm._s(_vm._f("formater_fecha")(item.mes)))
                  ]),
                  _vm._v(" "),
                  _c("td", [_vm._v(_vm._s(_vm._f("addmiles")(item.valor)))]),
                  _vm._v(" "),
                  _c("td", [_vm._v(_vm._s(item.estatus))])
                ])
              }),
              0
            )
          ],
          2
        ),
        _vm._v(" "),
        _c("br"),
        _vm._v(" "),
        _vm.acuerdo.status == "Autenticado"
          ? _c("div", [
              _c(
                "h5",
                {
                  staticClass: "ml-2 mt-5",
                  staticStyle: { "font-weight": "550 !important" }
                },
                [_vm._v("Deudas del acuerdo:")]
              ),
              _vm._v(" "),
              _c("table", { staticClass: "table" }, [
                _vm._m(1),
                _vm._v(" "),
                _c(
                  "tbody",
                  _vm._l(_vm.acuerdo.deudas, function(item) {
                    return _c("tr", { key: item.id }, [
                      _c("td", [_vm._v(_vm._s(item.servicio))]),
                      _vm._v(" "),
                      _c("td", [_vm._v(_vm._s(item.descripcion || "N/A"))]),
                      _vm._v(" "),
                      _c("td", [
                        _vm._v(
                          _vm._s(_vm._f("formater_fecha")(item.created_at))
                        )
                      ]),
                      _vm._v(" "),
                      _c("td", [
                        _vm._v("$ " + _vm._s(_vm._f("addmiles")(item.valor)))
                      ])
                    ])
                  }),
                  0
                )
              ])
            ])
          : _vm._e()
      ],
      1
    )
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-12 d-flex mb-2 pb-2 border-bottom" }, [
      _c("i", {
        staticClass: "fal fa-file-alt w-auto",
        staticStyle: { color: "#0f62ac", "font-size": "100px !important" }
      }),
      _vm._v(" "),
      _c(
        "h5",
        {
          staticClass: "ml-2 mt-5",
          staticStyle: { "font-weight": "550 !important" }
        },
        [_vm._v("Detalle de cartera")]
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", { staticClass: "grey lighten-2" }, [
      _c("tr", [
        _c("th", { attrs: { scope: "col" } }, [_vm._v("Servicio")]),
        _vm._v(" "),
        _c("th", { attrs: { scope: "col" } }, [_vm._v("Descripcion")]),
        _vm._v(" "),
        _c("th", { attrs: { scope: "col" } }, [_vm._v("Fecha")]),
        _vm._v(" "),
        _c("th", { attrs: { scope: "col" } }, [_vm._v("Valor")])
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/views/admin/acuerdo_pagos/detalles.vue":
/*!*************************************************************!*\
  !*** ./resources/js/views/admin/acuerdo_pagos/detalles.vue ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _detalles_vue_vue_type_template_id_04a93bc4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./detalles.vue?vue&type=template&id=04a93bc4& */ "./resources/js/views/admin/acuerdo_pagos/detalles.vue?vue&type=template&id=04a93bc4&");
/* harmony import */ var _detalles_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./detalles.vue?vue&type=script&lang=js& */ "./resources/js/views/admin/acuerdo_pagos/detalles.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _detalles_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./detalles.vue?vue&type=style&index=0&lang=css& */ "./resources/js/views/admin/acuerdo_pagos/detalles.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _detalles_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _detalles_vue_vue_type_template_id_04a93bc4___WEBPACK_IMPORTED_MODULE_0__["render"],
  _detalles_vue_vue_type_template_id_04a93bc4___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/admin/acuerdo_pagos/detalles.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/admin/acuerdo_pagos/detalles.vue?vue&type=script&lang=js&":
/*!**************************************************************************************!*\
  !*** ./resources/js/views/admin/acuerdo_pagos/detalles.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_detalles_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./detalles.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/admin/acuerdo_pagos/detalles.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_detalles_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/admin/acuerdo_pagos/detalles.vue?vue&type=style&index=0&lang=css&":
/*!**********************************************************************************************!*\
  !*** ./resources/js/views/admin/acuerdo_pagos/detalles.vue?vue&type=style&index=0&lang=css& ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_detalles_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/style-loader!../../../../../node_modules/css-loader??ref--6-1!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--6-2!../../../../../node_modules/vue-loader/lib??vue-loader-options!./detalles.vue?vue&type=style&index=0&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/admin/acuerdo_pagos/detalles.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_detalles_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_detalles_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_detalles_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_detalles_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_detalles_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/views/admin/acuerdo_pagos/detalles.vue?vue&type=template&id=04a93bc4&":
/*!********************************************************************************************!*\
  !*** ./resources/js/views/admin/acuerdo_pagos/detalles.vue?vue&type=template&id=04a93bc4& ***!
  \********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_detalles_vue_vue_type_template_id_04a93bc4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./detalles.vue?vue&type=template&id=04a93bc4& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/admin/acuerdo_pagos/detalles.vue?vue&type=template&id=04a93bc4&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_detalles_vue_vue_type_template_id_04a93bc4___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_detalles_vue_vue_type_template_id_04a93bc4___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);