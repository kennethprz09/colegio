(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[48],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/pages/respuestaCpv.vue?vue&type=script&lang=js&":
/*!************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/pages/respuestaCpv.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
var appLoading = document.getElementById("loading-bg");
var options = {
  closeButton: true,
  debug: false,
  newestOnTop: false,
  progressBar: true,
  positionClass: "toast-top-right",
  preventDuplicates: false,
  onclick: null,
  showDuration: "300",
  hideDuration: "1000",
  timeOut: "5000",
  extendedTimeOut: "1000",
  showEasing: "swing",
  hideEasing: "linear",
  showMethod: "fadeIn",
  hideMethod: "fadeOut"
};
/* harmony default export */ __webpack_exports__["default"] = ({
  mounted: function mounted() {
    appLoading.style.display = "block";
    this.SaveObjeto(this.datos);
  },
  beforeMount: function beforeMount() {
    appLoading.style.display = "block";
  },
  data: function data() {
    function parametroURL(_par) {
      var _p = null;
      if (location.search) location.search.substr(1).split("&").forEach(function (pllv) {
        var s = pllv.split("="),
            //separamos llave/valor
        ll = s[0],
            v = s[1] && decodeURIComponent(s[1]); //valor hacemos encode para prevenir url encode

        if (ll == _par) {
          //solo nos interesa si es el nombre del parametro a buscar
          if (_p == null) {
            _p = v; //si es nula, quiere decir que no tiene valor, solo textual
          } else if (Array.isArray(_p)) {
            _p.push(v); //si ya es arreglo, agregamos este valor

          } else {
            _p = [_p, v]; //si no es arreglo, lo convertimos y agregamos este valor
          }
        }
      });
      return _p;
    }

    return {
      datos: {
        transaccionId: parametroURL("transaccionId"),
        uuid: parametroURL("uuid"),
        aprobado: parametroURL("aprobado"),
        transaccionConvenioId: parametroURL("transaccionConvenioId"),
        ref1: parametroURL("ref1"),
        valor: parametroURL("valor"),
        valor_subtotal: parametroURL("valor_subtotal"),
        descuento: parametroURL("descuento"),
        cupon: parametroURL("cupon"),
        _uid: parametroURL("_uid"),
        _tramite: parametroURL("_tramite"),
        acudiente_id: parametroURL("acudiente_id"),
        meses: parametroURL("meses"),
        servicio_id: parametroURL("servicio_id"),
        certificado_motivo: parametroURL("certificado_motivo"),
        opcion: parametroURL("opcion")
      }
    };
  },
  methods: {
    SaveObjeto: function SaveObjeto(item) {
      var _this = this;

      appLoading.style.display = "block";

      if (item._tramite == "Matricula") {
        var url = "/api/matricula/pagado";
      }

      if (item._tramite == "Inscripcion") {
        var url = "/api/postulacion/pagado";
      }

      if (item._tramite == "Extracurricular") {
        var url = "/api/extracurricular/pagado";
      }

      if (item._tramite == "Pension") {
        var url = "/api/pension/pagado";
      }

      if (item._tramite == "Pension Multiple") {
        var url = "/api/pensionmultiple/pagado";
      }

      if (item._tramite == "Certificado") {
        var url = "/api/certificado/pagado";
        item.certificado_estado = "Pendiente";
      }

      if (item._tramite == "Cursos") {
        var url = "/api/cursos/pagado";
      }

      if (item._tramite == "Nivelacion") {
        var url = "/api/cursos/pagado";
      }

      if (item._tramite == "Recarga") {
        var url = "/api/recarga/pagado";
      }

      if (item._tramite == "Otro") {
        var url = "/api/otros/pagado";
      }

      if (item._tramite == "Transporte") {
        var url = "/api/transporte/pagado";
      }

      if (item._tramite == "Cafeteria") {
        var url = "/api/cafeteria/pagado";
      }

      if (item._tramite == "Acuerdo") {
        var url = "/api/acuerdo/pagado";
      }

      axios.post(url, item).then(function (response) {
        appLoading.style.display = "block"; // console.log(response.data);

        if (_this.datos.aprobado == "A") {
          _this.$toastr.success("Pago realizado con éxito!", null, options);
        } else if (_this.datos.aprobado == "R") {
          _this.$toastr.warning("Su Pago a sido Rechazado!", null, options);
        } else if (_this.datos.aprobado == "P") {
          if (_this.datos.transaccionId == "null") {
            _this.$toastr.success("Su Pago fue Cancelado!", null, options);
          } else {
            _this.$toastr.success("Su Pago quedo Pendiente!", null, options);
          }
        }

        if (item._tramite == "Inscripcion") {
          if (_this.datos.aprobado == "A") {
            _this.$router.push("/gracias-" + response.data);
          } else {
            _this.$router.push({
              name: "login"
            });
          }
        }

        if (item._tramite == "Extracurricular") {
          _this.$router.push({
            name: "Suscribcion de Extracurriculares"
          });
        }

        if (item._tramite == "Acuerdo") {
          _this.$router.push({
            name: "Acuerdo de pago"
          });
        }

        if (item._tramite == "Pension") {
          _this.$router.push({
            name: "Pago de Pension"
          });
        }

        if (item._tramite == "Pension Multiple") {
          _this.$router.push({
            name: "Pago de Pension"
          });
        }

        if (item._tramite == "Certificado") {
          _this.$router.push({
            name: "Inicio"
          });
        }

        if (item._tramite == "Cursos") {
          _this.$router.push({
            name: "Inicio"
          });
        }

        if (item._tramite == "Transporte") {
          _this.$router.push({
            name: "Inicio"
          });
        }

        if (item._tramite == "Cafeteria") {
          _this.$router.push({
            name: "Inicio"
          });
        }

        if (item._tramite == "Nivelacion") {
          _this.$router.push({
            name: "Inicio"
          });
        }

        if (item._tramite == "Recarga") {
          _this.$router.push({
            name: "Perfil"
          });
        }

        if (item._tramite == "Matricula") {
          _this.$router.push({
            name: "Matricula"
          });
        }

        if (item._tramite == "Otro") {
          _this.$router.push({
            name: "Pagos pendientes"
          });
        }
      })["catch"](function (e) {
        appLoading.style.display = "none";
        var errors = e.response.data.errors;
        var noti = _this.$toastr.error;
        $.each(errors, function (i) {
          noti(errors[i][0], "Errores", options);
        });
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/pages/respuestaCpv.vue?vue&type=template&id=5f08b11a&":
/*!****************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/pages/respuestaCpv.vue?vue&type=template&id=5f08b11a& ***!
  \****************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("section", [_vm._v("Procesando...")])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/views/pages/respuestaCpv.vue":
/*!***************************************************!*\
  !*** ./resources/js/views/pages/respuestaCpv.vue ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _respuestaCpv_vue_vue_type_template_id_5f08b11a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./respuestaCpv.vue?vue&type=template&id=5f08b11a& */ "./resources/js/views/pages/respuestaCpv.vue?vue&type=template&id=5f08b11a&");
/* harmony import */ var _respuestaCpv_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./respuestaCpv.vue?vue&type=script&lang=js& */ "./resources/js/views/pages/respuestaCpv.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _respuestaCpv_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _respuestaCpv_vue_vue_type_template_id_5f08b11a___WEBPACK_IMPORTED_MODULE_0__["render"],
  _respuestaCpv_vue_vue_type_template_id_5f08b11a___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/pages/respuestaCpv.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/pages/respuestaCpv.vue?vue&type=script&lang=js&":
/*!****************************************************************************!*\
  !*** ./resources/js/views/pages/respuestaCpv.vue?vue&type=script&lang=js& ***!
  \****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_respuestaCpv_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./respuestaCpv.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/pages/respuestaCpv.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_respuestaCpv_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/pages/respuestaCpv.vue?vue&type=template&id=5f08b11a&":
/*!**********************************************************************************!*\
  !*** ./resources/js/views/pages/respuestaCpv.vue?vue&type=template&id=5f08b11a& ***!
  \**********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_respuestaCpv_vue_vue_type_template_id_5f08b11a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./respuestaCpv.vue?vue&type=template&id=5f08b11a& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/pages/respuestaCpv.vue?vue&type=template&id=5f08b11a&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_respuestaCpv_vue_vue_type_template_id_5f08b11a___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_respuestaCpv_vue_vue_type_template_id_5f08b11a___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);