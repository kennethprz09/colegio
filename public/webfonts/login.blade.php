    <!DOCTYPE html>
    <html lang="en">
        <head>
        <meta charset="UTF-8">
        <title>Saltra | console</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet">
        <link rel="stylesheet" href="{{asset('css/style-login.css')}}">
        </head>
    <body>

        
        {{-- IMGEN 2 DEL LOGIN --}}
        <div class="img-login-2">
        <img src="{{asset('img/1x/flechas.png')}}">
        </div>


        {{-- NAVEGACIO DEL LOGIN  --}}
        <div class="navegacion">
        <div class="container-fluid">
        <div class="row">
        <div class="col-6">
        {{-- }LOGO --}}
        <div class="logo">
        <img src="{{asset('img/1x/logo-white.png')}}" alt="logo" class="img-fluid">
        </div>

        </div>


        {{-- LINK --}}
        <div class="col-6">
        <div class="link">
        <a href=""><span>Quienes somos</span></a>
        <a href=""><span>Documentación</span></a>
        <a href=""><span>Ayuda</span></a>
        <a href=""><span>Soporte</span></a>
        </div>
        </div>
        </div>
        </div>
        </div>


        {{-- CONTENEDOR PRINCIPAL --}}

        <div class="container-fluid">
        <div class="row">


        <div class="col-6" style="padding-left: 25px;">

        <div class="texto-grande">
        <h1>
        Potencia tus aplicaciones con nuestra API simple.
        </h1> 
        </div>

        <div class="texto-medio">
        <h3>
        Guías detalladas. Explora las soluciones para la industria.
        </h3>
        </div>
          @if (count($errors)>0)

              <div class="alert alert-danger alert-dismissible bounceInRight animated" 
              style="background: #F44336;
              color: white;
              position: fixed;
              z-index: 1300;
              right: 0;
              margin: auto;
              font-size: 13px;
              top: 47px;
              border:0px;">
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                  <strong>Disculpe <i class="fa fa-frown-o"></i></strong>

                  <ul>
                  @foreach($errors->all() as $error)
                    <li>{{$error}} </li>
                  @endforeach
                  </ul>

             </div>
         @endif

        {{-- AREA DEL FORMULARIO --}}
        <div class="formulario">
        <form method="POST" action="{{ route('login') }}">
        {{ csrf_field() }}

        <div class="form-group">
        <input type="text" class="form-control email" name="email" placeholder="Ingresa tu correo">
        </div>

        <div class="form-group">
        <input class="form-control password" type="password" name="password" placeholder="Ingresa tu contraseña">
        </div>

        <button type="submit" class="btn btn-danger boton">Iniciar Sesión</button>

        <div class="custom-control custom-checkbox recordar">
        <input type="checkbox" class="custom-control-input" id="defaultUnchecked">
        <label class="custom-control-label" for="defaultUnchecked" style="color: white;">Recordar sesión
        </label>
        </div>

        <div class="form-group text-olvidaste">
        <h1>¿Olvidaste tu contraseña?</h1>
        </div>
        

        </form>
        </div>

    
        </div>
        {{-- fin de col-6 - form --}}


        <div class="col-6">
        
        {{-- IMGAGEN 2 DEL LOGN --}}
        <div class="img-login-1">
        <img src="{{asset('img/1x/img-login.png')}}" alt="">
        </div>

        </div>

        </div> 
        </div>
      


    {{-- SCRIPT SALTRA console  --}}

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>


    </body>
    </html>
