<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email');
            $table->timestamp('email_verified_at')->nullable();
            $table->string('codigo')->nullable();
            $table->string('grado')->nullable();
            $table->string('name_estudiante')->nullable();
            $table->string('password');
            $table->string('saldo')->default('0');
            $table->enum('roll', ['admin', 'acudiente'])->default('acudiente');
            $table->string('nivelacion')->default('No');
            $table->string('pension')->default('10');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
