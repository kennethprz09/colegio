<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSolicitudCertificadosPublicosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solicitud_certificados_publicos', function (Blueprint $table) {
            $table->id();
            $table->string('nombres');
            $table->string('apellidos');
            $table->string('tipo_doc');
            $table->string('numero');
            $table->string('direccion');
            $table->string('ciudad');
            $table->string('telefono');
            $table->string('correo');
            $table->string('responsabilidad_fiscal');
            $table->string('actividad_economica');
            $table->string('otro_cual')->nullable();
            $table->string('nombres_estudiante');
            $table->string('apellidos_estudiante');
            $table->string('tipo_doc_estudiente');
            $table->string('numero_estudiente');
            $table->string('telefono_estudiante');
            $table->string('correo_estudiante');
            $table->string('tipo_documento');
            $table->string('cantidad');
            $table->string('año_promocion');
            $table->string('motivo');
            $table->string('datos_certificacion');
            $table->string('canal_envio');
            $table->string('certificado_publico')->default(1);
            $table->string('valor_pagar')->nullable();
            $table->string('codigounico')->nullable();
            $table->string('status')->default('Pendiente');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('solicitud_certificados_publicos');
    }
}
