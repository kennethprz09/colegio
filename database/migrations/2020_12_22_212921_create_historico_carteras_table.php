<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHistoricoCarterasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('historico_carteras', function (Blueprint $table) {
            $table->id();
            $table->string('servicio')->nullable();
            $table->string('valor')->nullable();
            $table->string('mora')->nullable();
            $table->string('otro')->default('No');
            $table->string('descripcion')->nullable();
            $table->string('acudiente_id')->nullable();
            $table->string('matricula_id')->nullable();
            $table->string('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('historico_carteras');
    }
}
