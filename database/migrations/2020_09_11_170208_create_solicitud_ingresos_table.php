<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSolicitudIngresosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solicitud_ingresos', function (Blueprint $table) {
            $table->id();
            // Paso 1
            $table->string('step1_nombre_completo')->nullable();
            $table->string('step1_primer_apellido')->nullable();
            $table->string('step1_segundo_apellido')->nullable();
            $table->string('step1_genero')->nullable();
            $table->string('step1_ide')->nullable();
            $table->string('step1_identificacion')->nullable();
            $table->string('step1_direccion_1')->nullable();
            $table->string('step1_direccion_1_1')->nullable();
            $table->string('step1_direccion_2')->nullable();
            $table->string('step1_direccion_3')->nullable();
            $table->string('step1_barrio')->nullable();
            $table->string('step1_nacimiento_at')->nullable();
            $table->string('step1_lugar_nacimiento')->nullable();
            $table->string('step1_telefono')->nullable();
            $table->string('step1_email')->nullable();
            $table->string('step1_institucion_previa')->nullable();
            $table->string('step1_grado')->nullable();
            $table->string('step1_motivo_cambio')->nullable();
            
            // Paso 2
            $table->string('step2_estado_padre')->nullable();
            $table->string('step2_nombre_padre')->nullable();
            $table->string('step2_ide_padre')->nullable();
            $table->string('step2_identificacion_padre')->nullable();
            $table->string('step2_expedicion_padre')->nullable();
            $table->string('step2_email_padre')->nullable();
            $table->string('step2_profesion_padre')->nullable();
            $table->string('step2_empresa_padre')->nullable();
            $table->string('step2_cargo_padre')->nullable();
            $table->string('step2_ingresos_padre')->nullable();
            $table->string('step2_telefono_padre')->nullable();
            $table->string('step2_direccion_padre')->nullable();

            $table->string('step2_estado_madre')->nullable();
            $table->string('step2_nombre_madre')->nullable();
            $table->string('step2_ide_madre')->nullable();
            $table->string('step2_identificacion_madre')->nullable();
            $table->string('step2_expedicion_madre')->nullable();
            $table->string('step2_email_madre')->nullable();
            $table->string('step2_profesion_madre')->nullable();
            $table->string('step2_empresa_madre')->nullable();
            $table->string('step2_cargo_madre')->nullable();
            $table->string('step2_ingresos_madre')->nullable();
            $table->string('step2_telefono_madre')->nullable();
            $table->string('step2_direccion_madre')->nullable();

            // Paso 3
            $table->string('step3_hermanos')->nullable();
            $table->string('step3_cantidad')->nullable();
            $table->string('step3_lugar_ocupado')->nullable();
            $table->string('step3_responsable')->nullable();
            $table->string('step3_medio_conocido')->nullable();
            $table->string('step3_estado_otro')->nullable();
            $table->string('step3_parentesco_otro')->nullable();
            $table->string('step3_nombre_otro')->nullable();
            $table->string('step3_ide_otro')->nullable();
            $table->string('step3_identificacion_otro')->nullable();
            $table->string('step3_expedicion_otro')->nullable();
            $table->string('step3_email_otro')->nullable();
            $table->string('step3_profesion_otro')->nullable();
            $table->string('step3_empresa_otro')->nullable();
            $table->string('step3_cargo_otro')->nullable();
            $table->string('step3_ingresos_otro')->nullable();
            $table->string('step3_telefono_otro')->nullable();
            $table->string('step3_direccion_1_otro')->nullable();
            $table->string('step3_direccion_1_1_otro')->nullable();
            $table->string('step3_direccion_2_otro')->nullable();
            $table->string('step3_direccion_3_otro')->nullable();
            $table->string('step3_factura')->nullable();
            $table->string('step3_medio_conocido_mas')->nullable();

            // Factura electronica
            $table->string('step3_tipo_persona_factura')->nullable();
            $table->string('step3_nombre_factura')->nullable();
            $table->string('step3_tipo_id_factura')->nullable();
            $table->string('step3_tipo_id_cual_factura')->nullable();
            $table->string('step3_identificacion_factura')->nullable();
            $table->string('step3_email_factura')->nullable();
            $table->string('step3_tel_fijo_factura')->nullable();
            $table->string('step3_tel_celular_factura')->nullable();
            $table->string('step3_pais_factura')->nullable();
            $table->string('step3_ciudad_factura')->nullable();
            $table->string('step3_departamento_factura')->nullable();
            $table->string('step3_direccion_factura')->nullable();
            
            // General
            $table->string('codigo_estudiante')->nullable();
            $table->string('status')->default('Pendiente');
            $table->string('nivelacion')->nullable();
            $table->string('status_nivelacion')->nullable();
            $table->string('compromiso')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('solicitud_ingresos');
    }
}