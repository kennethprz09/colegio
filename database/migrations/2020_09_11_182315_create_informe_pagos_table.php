<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInformePagosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('informe_pagos', function (Blueprint $table) {
            $table->id();
            $table->string('transaccionId')->nullable();
            $table->string('uuid')->nullable();
            $table->string('aprobado')->nullable();
            $table->string('transaccionConvenioId')->nullable();
            $table->string('ref1')->nullable();
            $table->string('valor')->nullable();
            $table->string('valor_subtotal')->nullable();
            $table->string('descuento')->nullable();
            $table->string('cupon')->nullable();
            $table->string('_uid')->nullable();
            $table->string('_tramite')->nullable();
            $table->string('acudiente_id')->nullable();
            $table->string('tipo')->default('Web');
            $table->string('meses')->nullable();
            $table->string('servicio_id')->nullable();
            $table->string('tipo_pago')->nullable();
            $table->string('certificado_estado')->nullable();
            $table->string('certificado_motivo')->nullable();
            $table->string('certificado_archivo')->nullable();
            $table->string('matricula_estado')->nullable();
            $table->string('matricula_pagare')->nullable();
            $table->string('matricula_contrato')->nullable();
            $table->string('matricula_acta')->nullable();
            $table->string('matricula_paz')->nullable();
            $table->string('factura_pdf')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('informe_pagos');
    }
}