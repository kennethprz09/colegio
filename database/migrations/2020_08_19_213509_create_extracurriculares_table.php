<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExtracurricularesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('extracurriculares', function (Blueprint $table) {
            $table->id();
            $table->string('actividad');
            $table->string('valor');
            $table->string('url_informacion')->nullable();
            $table->string('url_horarios')->nullable();
            $table->string('docente');
            $table->string('docente_id');
            $table->string('imagen');
            $table->timestamp('inicia_at')->nullable();
            $table->timestamp('termina_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('extracurriculares');
    }
}
