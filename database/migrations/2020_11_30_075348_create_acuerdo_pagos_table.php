<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAcuerdoPagosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('acuerdo_pagos', function (Blueprint $table) {
            $table->id();
            $table->string('cantidad_cuotas');
            $table->string('deuda_cuotas');
            $table->string('deuda_total');
            $table->timestamp('fecha_at')->nullable();
            $table->string('acudiente_id');
            $table->string('status')->default('Pendiente');
            $table->string('finalizado')->default('si');
            $table->string('pdf')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('acuerdo_pagos');
    }
}