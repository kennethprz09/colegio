<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMatriculasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matriculas', function (Blueprint $table) {
            $table->id();
            $table->string('desde')->nullable();
            $table->string('hasta')->nullable();

            $table->timestamp('inicia_at_or')->nullable();
            $table->timestamp('termina_at_or')->nullable();
            $table->string('valor_or')->nullable();

            $table->timestamp('inicia_at_ex')->nullable();
            $table->timestamp('termina_at_ex')->nullable();
            $table->string('valor_ex')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('matriculas');
    }
}