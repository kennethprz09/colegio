<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);
        
        DB::table('users')->insert([
            'name' => "Kenneth",
            'email' => 'Kenneth@utam.co',
            'codigo' => '26047252',
            'password' => bcrypt('qwerty'),
            'roll' => 'admin',
            'created_at' => new \DateTime(),
        ]);

        DB::table('users')->insert([
            'name' => "Yessika",
            'email' => 'Yessika@utam.co',
            'codigo' => '26048187',
            'password' => bcrypt('qwerty'),
            'created_at' => new \DateTime(),
        ]);

        DB::table('configs')->insert([
            'precio_ingreso' => '30000',
            'created_at' => new \DateTime(),
        ]);

        DB::table('nivelacions')->insert([
            'valor' => '900000',
            'email' => 'Juan@colegiocatolicocali.edu.co',
            'inicia_at' => new \DateTime(),
            'termina_at' => new \DateTime(),
            'created_at' => new \DateTime(),
        ]);
    }
}
