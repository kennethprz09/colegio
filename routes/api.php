<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\HistoricoCartera;
use App\Extracurriculares;
use App\Certificados;
use App\Cursos;
use App\Transporte;
use App\Cafeteria;
use App\SolicitudCertificadosPublicos;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\InformePagosExport;
use App\Exports\InformePagosPublicExport;
use App\Exports\CarteraExport;
use App\Exports\AcuerdoPagosExport;
use App\Exports\DescuentosExport;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
	$request->user()->deuda = HistoricoCartera::where('acudiente_id', Auth::User()->id)->where('status', 'activa')->sum('valor');
    return $request->user();
});

Route::group(['middleware' => 'cors'], function() {
	Route::post('/login', 'auth\LoginController@login');
	
	Route::group(['middleware' => 'auth:api'], function() {
		Route::post('/logout', 'auth\LoginController@logout');

		// Admin
			// Matriculas
				Route::get('/admin/matriculas', 'MatriculasController@List');
				Route::post('/admin/matriculas', 'MatriculasController@Save');
				Route::post('/admin/matriculas/update', 'MatriculasController@Update');
				Route::delete('/admin/matriculas/{id}', 'MatriculasController@Delete');
			// Fin Matriculas

			// Extracurriculares
				Route::get('/admin/extracurriculares', 'ExtracurricularesController@List');
				Route::post('/admin/extracurriculares/docentes', 'ExtracurricularesController@ListDocentes');
				Route::post('/admin/extracurriculares', 'ExtracurricularesController@Save');
				Route::post('/admin/extracurriculares/update', 'ExtracurricularesController@Update');
				Route::delete('/admin/extracurriculares/{id}', 'ExtracurricularesController@Delete');
			// Fin Extracurriculares

			// Pension
				Route::get('/admin/pension', 'PensionController@List');
				Route::post('/admin/pension', 'PensionController@Save');
				Route::post('/admin/pension/update', 'PensionController@Update');
				Route::delete('/admin/pension/{id}', 'PensionController@Delete');
			// Fin Pension

			// Acuerdo de pago
				Route::get('/admin/acuerdo_pagos', 'AcuerdoPagoController@List');
				Route::post('/admin/acuerdo_pagos/{id}', 'AcuerdoPagoController@Show');
				Route::put('/admin/acuerdo_pagos/aprobar/{id}', 'AcuerdoPagoController@Aprobar');
				Route::put('/admin/acuerdo_pagos/autenticar/{id}', 'AcuerdoPagoController@Autenticar');
				Route::put('/admin/acuerdo_pagos/rechazar/{id}', 'AcuerdoPagoController@Rechazar');
				Route::put('/admin/acuerdo_pagos/finalizar/{id}', 'AcuerdoPagoController@Finalizar');
				Route::post('/admin/acuerdo_pagos/pdf/{id}', 'AcuerdoPagoController@Pdf');
			// Fin Acuerdo de pago

			// Cursos
				Route::get('/admin/cursos', 'CursosController@List');
				Route::post('/admin/cursos/docentes', 'CursosController@ListDocentes');
				Route::post('/admin/cursos', 'CursosController@Save');
				Route::post('/admin/cursos/update', 'CursosController@Update');
				Route::delete('/admin/cursos/{id}', 'CursosController@Delete');
				Route::post('/admin/cursos/nivelacion', 'CursosController@ListNivelacion');
				Route::post('/admin/cursos/nivelacion/update', 'CursosController@NivelacionUpdate');
			// Fin Cursos

			// Medias tecnicas
				Route::get('/admin/medias_tecnicas', 'MediasTecnicasController@List');
				Route::post('/admin/medias_tecnicas/docentes', 'MediasTecnicasController@ListDocentes');
				Route::post('/admin/medias_tecnicas', 'MediasTecnicasController@Save');
				Route::post('/admin/medias_tecnicas/update', 'MediasTecnicasController@Update');
				Route::delete('/admin/medias_tecnicas/{id}', 'MediasTecnicasController@Delete');
			// Fin Medias tecnicas
			
			// Certificados
				Route::get('/admin/certificados', 'CertificadosController@List');
				Route::post('/admin/certificados', 'CertificadosController@Save');
				Route::post('/admin/certificados/update', 'CertificadosController@Update');
				Route::delete('/admin/certificados/{id}', 'CertificadosController@Delete');
			// Fin Certificados

			// Transporte
				Route::get('/admin/transporte', 'TransporteController@List');
				Route::post('/admin/transporte', 'TransporteController@Save');
				Route::post('/admin/transporte/update', 'TransporteController@Update');
				Route::delete('/admin/transporte/{id}', 'TransporteController@Delete');
			// Fin Transporte

			// Cafeteria
				Route::get('/admin/cafeteria', 'CafeteriaController@List');
				Route::post('/admin/cafeteria', 'CafeteriaController@Save');
				Route::post('/admin/cafeteria/update', 'CafeteriaController@Update');
				Route::delete('/admin/cafeteria/{id}', 'CafeteriaController@Delete');
			// Fin Cafeteria

			// Informe de pagos
				Route::get('/admin/informe_pagos', 'InformePagoController@List');
				Route::get('/admin/informe_pagos/publicos', 'InformePagoController@ListPublicos');
			// Fin Informe de pagos

			// Historico de cartera
				Route::get('/admin/historico_cartera', 'HistoricoCarteraController@List');
				Route::get('/admin/historico_cartera/{codigo}', 'HistoricoCarteraController@ListAdminDetalles');
				Route::post('/admin/historico_cartera/user/{codigo}', 'HistoricoCarteraController@ListAdminDetallesUser');
				Route::post('/admin/historico_cartera/recordatorio/acudiente', 'HistoricoCarteraController@Recordatorio');
			// Fin Historico de cartera

			// Certificados solicitados
				Route::get('/admin/informe_pagos/certificados', 'InformePagoController@Listcertificados');
				Route::post('/admin/informe_pagos/certificados/{id}', 'InformePagoController@CertificadosListo');
				Route::post('/admin/informe_pagos/certificados/pdf/{id}', 'InformePagoController@CertificadosPdf');
			// Fin Certificados solicitados

			// Certificados solicitados Publicos
				Route::get('/admin/certificadospublicos/list', 'SolicitudCertificadosPublicosController@List');
			// Fin Certificados solicitados Publicos

			// Solicitudes de ingreso
				Route::get('/admin/solicitud_ingreso', 'SolicitudIngresoController@List');
				Route::get('/admin/solicitud_ingreso-compromiso', 'SolicitudIngresoController@ListCompromiso');
				Route::post('/admin/solicitud_ingreso/state', 'SolicitudIngresoController@UpdateState');
				Route::post('/postulacion/precio/update', 'ConfigController@PrecioUpdate');
				Route::post('/postulacion/solicitud_ingreso/manual', 'SolicitudIngresoController@PostulacionManual');
			// Fin Solicitudes de ingreso

			// Pagos de matricula
				Route::get('/admin/pagos-matricula', 'MatriculasController@ListMatriculados');
				Route::post('/admin/pagos-matricula/{id}', 'MatriculasController@ShowMatriculado');
				Route::post('/admin/pagos-matricula/pdf/up', 'MatriculasController@MatriculadoPdf');
			// Fin Pagos de matricula

			// Pagos en caja
				Route::get('/admin/informe_pagos_manual', 'InformePagoController@ListManual');
				Route::post('/admin/informe_pagos_manual/buscar/{codigo}', 'InformePagoController@BuscarDatos');
				Route::post('/admin/informe_pagos_manual/filtrar/{servicio}', 'InformePagoController@FiltrarDatos');
				Route::post('/admin/historico_cartera/{codigo}', 'HistoricoCarteraController@ListAdmin');
				Route::put('/admin/informe_pagos_manual/pagar_detalles', 'InformePagoController@PagoManualDetalles');
				Route::post('/admin/informe_pagos_manual/pagar', 'InformePagoController@PagoManual');
				Route::post('/admin/informe_pagos_manual/pagar/matricula', 'InformePagoController@PagoMatricula');
				Route::post('/admin/informe_pagos_manual/pagar/pension', 'InformePagoController@PagoPension');
			// Fin Pagos en caja

			// Otros pagos
				Route::get('/admin/otros_pagos', 'HistoricoCarteraController@ListOtros');
				Route::post('/admin/otros_pagos', 'HistoricoCarteraController@OtrosSave');
				Route::put('/admin/otros_pagos', 'HistoricoCarteraController@OtrosUpdate');
				Route::delete('/admin/otros_pagos/{id}', 'HistoricoCarteraController@OtrosDelete');
			// Fin Otros pagos

			// Descuentos
				Route::get('/admin/descuentos', 'DescuentosController@List');
				Route::post('/admin/descuentos/estudiantes', 'DescuentosController@ListEstudiantes');
				Route::post('/admin/descuentos', 'DescuentosController@Save');
				Route::post('/admin/descuentos/update', 'DescuentosController@Update');
				Route::post('/admin/descuentos/update/{id}', 'DescuentosController@StatusChange');
				Route::delete('/admin/descuentos/{id}', 'DescuentosController@Delete');
			// Fin Descuentos

			// Docentes
				Route::get('/admin/docentes', 'DocenteController@List');
				Route::post('/admin/docentes', 'DocenteController@Save');
				Route::post('/admin/docentes/update', 'DocenteController@Update');
				Route::delete('/admin/docentes/{id}', 'DocenteController@Delete');
			// Fin Docentes

			// Usuarios
				Route::get('/admin/usuarios/admin', 'UsuariosController@ListAdmin');
				Route::get('/admin/usuarios/acudiente', 'UsuariosController@ListAcudiente');
				Route::post('/admin/usuarios', 'UsuariosController@Save');
				Route::post('/admin/usuarios/update', 'UsuariosController@Update');
				Route::delete('/admin/usuarios/{id}', 'UsuariosController@Delete'); 
				Route::put('/admin/usuarios/{id}', 'UsuariosController@Restablecer');
				Route::put('/admin/usuarios/update/grados', 'UsuariosController@UpdateGrado');
				Route::put('/admin/usuarios/update/datos', 'UsuariosController@UpdateDatos');
				Route::post('/admin/usuarios/update/matricula', 'UsuariosController@MatriculaAnual');
				Route::post('/admin/usuarios/update/pension1', 'UsuariosController@Pension1Mes');
				Route::post('/admin/usuarios/update/pension5', 'UsuariosController@Pension5Mes');
			// Fin Usuarios
		// Fin Admin

		// Acudiente
			// Certificados
				Route::post('/acudiente/certificados/privados', 'CertificadosController@ListCertificados');
			// Fin Certificados

			// Cursos y nivelacion
				Route::post('/acudiente/cursos', 'CursosController@ListManual');
				Route::post('/acudiente/nivelacion', 'CursosController@ListNivelacion');
				Route::post('/acudiente/transporte', 'TransporteController@ListManual');
				Route::post('/acudiente/cafeteria', 'CafeteriaController@ListManual');
			// Fin Cursos y nivelacion

			// Extracurriculares
				Route::post('/acudiente/extracurriculares', 'ExtracurricularesController@ListToAcudientes');
				Route::post('/acudiente/extracurriculares/incritos', 'ExtracurricularesController@ListToAcudientesInscritos');
			// Fin Extracurriculares

			// Medias tecnicas
				Route::post('/acudiente/medias_tecnicas', 'MediasTecnicasController@ListToAcudientes');
				Route::post('/acudiente/medias_tecnicas/incritos', 'MediasTecnicasController@ListToAcudientesInscritos');
			// Fin Medias tecnicas

			// Pension
				Route::get('/acudiente/pension', 'PensionController@ListPagos');
				Route::post('/acudiente/pension/tarifa', 'PensionController@Tarifa');
			// Fin Pension

			// Matricula
				Route::post('/acudiente/matricula/tarifa', 'MatriculasController@Tarifa');
				Route::post('/acudiente/matricula/pension', 'PensionController@ConfiguracionPension');
			// Fin Matricula

			// Pagos pendientes
				Route::get('/acudiente/historico_cartera', 'HistoricoCarteraController@ListAcudiente');
			// Fin Pagos pendientes

			// Acuerdo de pago
				Route::post('/acudiente/acuerdo', 'AcuerdoPagoController@new_acuerdo');
				Route::post('/acudiente/acuerdo/detalles', 'AcuerdoPagoController@Show_acudiente');
			// Fin Acuerdo de pago

			// Facturas
				Route::get('/acudiente/facturas', 'InformePagoController@ListAcudiente');
			// Fin Facturas

			// Facturas
				Route::get('/acudiente/documentos', 'InformePagoController@ListAcudienteDocs');
			// Fin Facturas

			// Descuentos
				Route::post('/acudiente/descuento/{codigo}/{tramite}', 'DescuentosController@BuscarDescuento');
			// Fin Descuentos

			// Perfil
				Route::put('/acudiente/password', 'auth\LoginController@Update_pass');
			// Fin Perfil

			// Saldo
				Route::post('/acudiente/saldo-pago', 'InformePagoController@SaldoPagoAcudiente');
			// Fin Saldo
		// Fin Acudiente

		// Excels
		Route::get('/informe_pagos/excel', function () {
			return Excel::download(new InformePagosExport, 'InformePagos.xlsx');
		});

		Route::get('/informe_pagos/publico/excel', function () {
			return Excel::download(new InformePagosPublicExport, 'InformePagosPublicos.xlsx');
		});

		Route::get('/historico_cartera/excel', function () {
			return Excel::download(new CarteraExport, 'Cartera.xlsx');
		});

		Route::get('/acuerdo_pagos/excel', function () {
			return Excel::download(new AcuerdoPagosExport, 'AcuerdoPagos.xlsx');
		});

		Route::get('/descuentos/excel', function () {
			return Excel::download(new DescuentosExport, 'CuponesdeDescuentos.xlsx');
		});
	});

	// Publica
		Route::post('/certificados/list', 'ConfigController@ListCertificados');
		Route::post('/postulacion/precio', 'ConfigController@PrecioIngreso');
		Route::post('/postulacion', 'SolicitudIngresoController@Save');
		Route::post('/postulacion/pagado', 'SolicitudIngresoController@Pagado');
		Route::post('/extracurricular/pagado', 'ExtracurricularesController@Pagado');
		Route::post('/certificado/pagado', 'CertificadosController@Pagado');
		Route::post('/matricula/pagado', 'MatriculasController@Pagado');
		Route::post('/pension/pagado', 'PensionController@Pagado');
		Route::post('/pensionmultiple/pagado', 'PensionController@PagadoMultiple');
		Route::post('/cursos/pagado', 'CursosController@Pagado');
		Route::post('/recarga/pagado', 'InformePagoController@Pagado');
		Route::post('/otros/pagado', 'HistoricoCarteraController@Pagado');
		Route::post('/transporte/pagado', 'TransporteController@Pagado');
		Route::post('/cafeteria/pagado', 'CafeteriaController@Pagado');
		Route::post('/acuerdo/pagado', 'AcuerdoPagoController@Pagado');
		Route::get('/documento/view/{id}-pdf', function ($id) {
			$formatterES = new \NumberFormatter("es", \NumberFormatter::SPELLOUT);
			$data = App\InformePago::find($id);
			
			if ($data) {
				$user = App\User::find($data->acudiente_id);
				if ($user) {
					$estudiante = App\Externos\Estudiante::where('estado', null)->where('codigo_estuiante', $user->codigo)->first();  // get estudiantes
					if(!$estudiante){
						return 'Bad';
					}
					$mama = App\Externos\Madre::where('id_estudiante', $estudiante->id)->first();
					$padre = App\Externos\Padre::where('id_estudiante', $estudiante->id)->first();
					$acudiente = App\Externos\Acudiente::where('id_estudiante', $estudiante->id)->first();
			
					// Validamor quien tiene que paga 
					if ($mama['responsable'] == 'si') {
						$estudiante->prefijo = 'madre';
						$estudiante->acudiente_name = $mama->nombre_madre;
						$estudiante->acudiente_email = $mama->email_madre;
			
						$estudiante->acudiente_estado = $mama->estado_madre;
						$estudiante->ide = $mama->tipo_dni;
						$estudiante->numero_acudiente_expedicion = $mama->dni_madre;
						$estudiante->profesion = $mama->profesion_madre;
						$estudiante->empresa = $mama->nombre_empresa_madre;
						$estudiante->cargo = $mama->cargo_madre;
						$estudiante->telefono = $mama->telefono_madre;
					}
					else if ($padre['responsable'] == 'si') {
						$estudiante->prefijo = 'padre';
						$estudiante->acudiente_name = $padre->nombre_padre;
						$estudiante->acudiente_email = $padre->email_padre;
			
						$estudiante->acudiente_estado = $padre->estado_padre;
						$estudiante->ide = $padre->tipo_dni;
						$estudiante->numero_acudiente_expedicion = $padre->dni_padre;
						$estudiante->profesion = $padre->profesion_padre;
						$estudiante->empresa = $padre->nombre_empresa_padre;
						$estudiante->cargo = $padre->cargo_padre;
						$estudiante->telefono = $padre->telefono_padre;
					}
					else if ($acudiente['responsable'] == 'si') {
						$estudiante->prefijo = 'acudiente';
						$estudiante->acudiente_name = $acudiente->nombre_acudiente;
						$estudiante->acudiente_email = $acudiente->email_acudiente;
			
						$estudiante->acudiente_estado = $acudiente->estado_acudiente;
						$estudiante->ide = $acudiente->tipo_dni;
						$estudiante->numero_acudiente_expedicion = $acudiente->dni_acudiente;
						$estudiante->profesion = $acudiente->profesion_acudiente;
						$estudiante->empresa = $acudiente->nombre_empresa_acudiente;
						$estudiante->cargo = $acudiente->cargo_acudiente;
						$estudiante->telefono = $acudiente->telefono_acudiente;
					}
					else {
						$estudiante->prefijo = 'acudiente';
						$estudiante->acudiente_name = $acudiente->nombre_acudiente;
						$estudiante->acudiente_email = $acudiente->email_acudiente;
			
						$estudiante->acudiente_estado = $acudiente->estado_acudiente;
						$estudiante->ide = $acudiente->tipo_dni;
						$estudiante->numero_acudiente_expedicion = $acudiente->dni_acudiente;
						$estudiante->profesion = $acudiente->profesion_acudiente;
						$estudiante->empresa = $acudiente->nombre_empresa_acudiente;
						$estudiante->cargo = $acudiente->cargo_acudiente;
						$estudiante->telefono = $acudiente->telefono_acudiente;
					}
				
					$data->name_estudiante = $estudiante->nombre_estudiante;
					$data->apellido = $estudiante->apellido1_estudiante;
					$data->email = $user->email;
					$data->grado = $user->grado;
					$data->codigo = $user->codigo;
					$data->direccion = $estudiante->direccion;
					$data->telefono = $estudiante->telefono;
				}
				if ($data->_tramite == "Inscripcion") {
					$acudiente = App\SolicitudIngreso::find($data->_uid);
					if ($acudiente) {
						$data->name_estudiante = $acudiente->step1_nombre_completo;
						$data->apellido = $acudiente->step1_primer_apellido;
						$data->email = $acudiente->step1_email;
						$data->codigo = $acudiente->codigo_estudiante;
						$data->grado = $acudiente->step1_grado;
	
						$data->direccion = $acudiente->step1_telefono;
						$data->telefono = $acudiente->step1_barrio;
						$data->nombre = $data->_tramite;
					}
				}
				if ($data->_tramite == "Pension") {
					$pension = HistoricoCartera::find($data->_uid);
					setlocale(LC_ALL, 'es_ES');
					if ($pension) {
						$fecha = Carbon::parse($pension->created_at);
						$fecha->format("F"); // Inglés.
						$data->created_f = $fecha->formatLocalized('%B');// mes en idioma español
						$data->mora = $pension->mora;
					}
				}
				if ($data->_tramite == "Extracurricular") {
					$extra = Extracurriculares::find($data->_uid);
					if ($extra) {
						$data->nombre = $extra->actividad;
					}
				}
				if ($data->_tramite == "Otro") {
					$extra = HistoricoCartera::find($data->_uid);
					if ($extra) {
						$data->nombre = $extra->descripcion;
					}
				}
				if ($data->_tramite == "Certificado") {
					$certificado = Certificados::find($data->_uid);
					if ($certificado) {
						$data->nombre = $certificado->concepto;
					}
				}
				if ($data->_tramite == "Certificado publico") {
					$certificado = Certificados::find($data->_uid);
					if ($certificado) {
						$data->nombre = $certificado->concepto;
						$data->apellido = $certificado->apellidos;
					}
					
					$solicitud = SolicitudCertificadosPublicos::find($data->servicio_id);
					if ($solicitud) {
						$data->name_estudiante = $solicitud->nombres_estudiante;
						$data->email = $solicitud->correo;
						$data->codigo = null;
						$data->grado = null;
	
						$data->direccion = $solicitud->direccion;
						$data->telefono = $solicitud->telefono;
					}
				}
				if ($data->_tramite == "Curso") {
					$curso = Cursos::find($data->_uid);
					if ($curso) {
						$data->nombre = $curso->asignatura;
					}
				}
				if ($data->_tramite == "Transporte") {
					$transporte = Transporte::find($data->_uid);
					if ($transporte) {
						$data->nombre = $transporte->descripcion;
					}
				}
				if ($data->_tramite == "Cafeteria") {
					$cafeteria = Cafeteria::find($data->_uid);
					if ($cafeteria) {
						$data->nombre = $cafeteria->descripcion;
					}
				}
				if ($data->_tramite == "Matricula") {
					$data->nombre = "Matricula";
				}
				if ($data->_tramite == "Nivelacion") {
					$data->nombre = "Curso de superación";
				}
				if ($data->_tramite == "Acuerdo") {
					$data->nombre = "Cuota de acuerdo";
				}
				if ($data->_tramite == "Saldo a favor") {
					$data->nombre = $data->_tramite;
				}
				if ($data->_tramite == "Recarga") {
					$data->nombre = 'Recarga de bolsillo';
				}
				
				$pdf = \PDF::loadView('pdf.factura', ['data' => $data, 'precio' => $formatterES->format($data->valor) ]);
				return $pdf->stream('Factura-'.$data->codigo. '.pdf');
			} else {
				return redirect('/acudiente/facturas-pagos');
			}
		});
		Route::post('/solicitar/certificado', 'SolicitudCertificadosPublicosController@Solictar');
		Route::put('/solicitar/certificado', 'SolicitudCertificadosPublicosController@SolictarUpdate');
		Route::put('/solicitar/certificado/pagado', 'SolicitudCertificadosPublicosController@PagadoPresencial');
		Route::post('/solicitar/certificado/{codigounico}', 'SolicitudCertificadosPublicosController@VerSolicitud');
		Route::post('/solicitar/list', 'SolicitudCertificadosPublicosController@List');
	// Fin Publica
});