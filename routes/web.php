<?php

use Illuminate\Support\Facades\Route;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\CarteraImport;
use App\Exports\InformePagosExport;
use App\Exports\InformePagosPublicExport;
use App\Exports\CarteraExport;
use App\Exports\AcuerdoPagosExport;
use App\Exports\DescuentosExport;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     Excel::import(new CarteraImport, public_path().'/importar/PENSIONES.xlsx');

//     return 'Good';
// });

// Route::get('/', function () {
//     $acudientes = App\User::where('roll', 'acudiente')->get();
    
//     foreach($acudientes as $item) {
//         $item->password = bcrypt('qwerty1012');
//         $item->save();
//     }

//     return 'Good';
// });

Route::get('/{any}', function () {
    return view('welcome');
})->where('any', '.*');

Route::get('/api/informe_pagos/excel', function () {
    return Excel::download(new InformePagosExport, 'InformePagos.xlsx');
});

Route::get('/api/informe_pagos/publico/excel', function () {
    return Excel::download(new InformePagosPublicExport, 'InformePagosPublicos.xlsx');
});

Route::get('/api/historico_cartera/excel', function () {
    return Excel::download(new CarteraExport, 'Cartera.xlsx');
});

Route::get('/api/acuerdo_pagos/excel', function () {
    return Excel::download(new AcuerdoPagosExport, 'AcuerdoPagos.xlsx');
});

Route::get('/api/descuentos/excel', function () {
    return Excel::download(new DescuentosExport, 'CuponesdeDescuentos.xlsx');
});

// Route::get('/', function () {
// 	$estudiante = App\Externos\Estudiante::where('estado', '!=', 'retirado')->orderBy('id', 'DESC')->get();  // get estudiantes
// 	$errores = [];
// 	foreach ($estudiante as $item) {
// 		$mama = App\Externos\Madre::where('id_estudiante', $item->id)->first();
// 		$padre = App\Externos\Padre::where('id_estudiante', $item->id)->first();
// 		$acudiente = App\Externos\Acudiente::where('id_estudiante', $item->id)->first();
		
// 		// Validamor quien tiene que paga
// 		if ($mama['responsable'] == 'si') {
// 			$item->prefijo = 'madre';
// 			$item->acudiente_name = $mama->nombre_madre;
// 			$item->acudiente_email = $mama->email_madre;
// 		}
// 		else if ($padre['responsable'] == 'si') {
// 			$item->prefijo = 'padre';
// 			$item->acudiente_name = $padre->nombre_padre;
// 			$item->acudiente_email = $padre->email_padre;
// 		}
// 		else if ($acudiente['responsable'] == 'si') {
// 			$item->prefijo = 'acudiente';
// 			$item->acudiente_name = $acudiente->nombre_acudiente;
// 			$item->acudiente_email = $acudiente->email_acudiente;
// 		}
// 		else {
// 			$item->prefijo = 'acudiente';
// 			$item->acudiente_name = $acudiente->nombre_acudiente;
// 			$item->acudiente_email = $acudiente->email_acudiente;
// 		}

// 		// Crear nuevos acudientes
		
// 		if (($item->grado_cursar_estudiante == null) || ($item->acudiente_email == null)
// 		)  {
// 			$errores[] = $item->codigo_estuiante;
//             $verificar = App\User::where('codigo', $item->codigo_estuiante)->first();
//             if (!$verificar) {
//                 $sysAcuciente = new App\User;
//                 $sysAcuciente->name = 'acudiente';
//                 $sysAcuciente->email = 'mail@mail.com';
//                 $sysAcuciente->codigo = $item->codigo_estuiante;
//                 $sysAcuciente->grado = $item->grado_cursar_estudiante;
//                 $sysAcuciente->name_estudiante = $item->nombre_estudiante;
//                 $sysAcuciente->password = bcrypt($item->codigo_estuiante);
//                 $sysAcuciente->save();
//             }
// 		} else {
//             $verificar = App\User::where('codigo', $item->codigo_estuiante)->first();
//             if (!$verificar) {
//                 $sysAcuciente = new App\User;
//                 $sysAcuciente->name = $item->acudiente_name;
//                 $sysAcuciente->email = $item->acudiente_email;
//                 $sysAcuciente->codigo = $item->codigo_estuiante;
//                 $sysAcuciente->grado = $item->grado_cursar_estudiante;
//                 $sysAcuciente->name_estudiante = $item->nombre_estudiante;
//                 $sysAcuciente->password = bcrypt($item->codigo_estuiante);
//                 $sysAcuciente->save();
//             }
// 		}

// 	}
//     return $errores;
// });

// Funcion para extraer los datos del estudiante con su responsable de pagos 
// public function getEstudent(){
// 	$estudiante = Estudiante::orderBy('id', 'DESC')->get();  // get estudiantes
// 	foreach ($estudiante as $item) {
// 		$mama = Madre::where('id_estudiante', $item->id)->first();
// 		$padre = Padre::where('id_estudiante', $item->id)->first();
// 		$acudiente = Acudiente::where('id_estudiante', $item->id)->first();
		
// 		// Validamor quien tiene que paga
// 		if ($mama['responsable'] == 'si') {
// 			$item->prefijo = 'madre';
// 			$item->pago = $mama;
// 		}
// 		else if ($padre['responsable'] == 'si') {
// 			$item->prefijo = 'padre';
// 			$item->pago = $padre;
// 		}
// 		else if ($acudiente['responsable'] == 'si') {
// 			$item->prefijo = 'acudiente';
// 			$item->pago = $acudiente;
// 		}
// 		else {
// 			$item->prefijo = 'acudiente';
// 			$item->pago = $acudiente;
// 		}

// 	}
// 	return $estudiante;
// }

// Funcion para extraer los datos del estudiante con su responsable de pagos 
// public function getCodeEstudent(Request $res){
// 	$estudiante = Estudiante::where('codigo_estuiante', $res->codigo_estuiante)->first();  // get estudiantes
// 	$mama = Madre::where('id_estudiante', $estudiante->id)->first();
// 	$padre = Padre::where('id_estudiante', $estudiante->id)->first();
// 	$acudiente = Acudiente::where('id_estudiante', $estudiante->id)->first();

// 	// Validamor quien tiene que paga 
// 	if ($mama['responsable'] == 'si') {
// 		$estudiante->prefijo = 'madre';
// 		$estudiante->pago = $mama;
// 	}
// 	else if ($padre['responsable'] == 'si') {
// 		$estudiante->prefijo = 'padre';
// 		$estudiante->pago = $padre;
// 	}
// 	else if ($acudiente['responsable'] == 'si') {
// 		$estudiante->prefijo = 'acudiente';
// 		$estudiante->pago = $acudiente;
// 	}
// 	else {
// 		$estudiante->prefijo = 'acudiente';
// 		$estudiante->pago = $acudiente;
// 	}


// 	return $estudiante;
// }

// public function guardarEstudiante(Request $request) {
// 	$estudiante = Estudiante::create($request->all());
// 	$item = periodoActivo::find(1);
// 	$periodo = Periodos::where('id', $item->periodo_id)->first();

// 	$consulta = Periodos::where('periodo', $periodo->periodo)->first();
// 	if ($consulta) {
// 		$inscribir = new Inscritos();
// 		$inscribir->año_id = $request->periodo;
// 		$inscribir->estudiante_id = $estudiante->id;
// 		$inscribir->grado = $estudiante->grado_cursar_estudiante;
// 		$inscribir->save();

// 		$otro =  Periodos::where('id', $request->periodo)->first();

// 		$datos6 = Estudiante::find($estudiante->id);
// 		$datos6->periodo = $otro->periodo;
// 		$datos6->save();
// 	}

// 	$date = Carbon::now();
// 	$date = $date->format('Y');
// 	$año = substr($date, 2,4);
// 	$formula = $año . "001";
// 	$formula2 = Estudiante::where('codigo_estuiante', $formula)->first();

// 	if($formula2){
// 		$proceso = Estudiante::orderBy('id', 'DESC')->offset(1)->take(1)->first();
// 		$codigo3 = $proceso->codigo_estuiante + 1;
// 		$datos44 = Estudiante::find($estudiante->id);

// 		if ($request->codigo_estuiante) {
// 			$datos44->codigo_estuiante = $request->codigo_estuiante;
// 		}else{
// 			$datos44->codigo_estuiante = $codigo3;
// 		}
// 		$datos44->save();
// 	}
// 	else{
// 		$codigo = $año . "001";
// 		$datos66 = Estudiante::find($estudiante->id);
// 		if ($request->codigo_estuiante) {
// 			$datos66->codigo_estuiante = $request->codigo_estuiante;
// 		}else{
// 			$datos66->codigo_estuiante = $codigo;
// 		}

// 		$datos66->save();
// 	}

// 	$datos = Estudiante::find($estudiante->id);
// 	// $datos->codigo_estuiante = $codigo;
// 	$datos->formulario1 = 'true';
// 	$datos->save();

// 	// Guardar los otros datos de ese estudiante
// 	$madre = new Madre();
// 	$madre->id_estudiante = $estudiante->id;
// 	$madre->save();

// 	$padre = new Padre();
// 	$padre->id_estudiante = $estudiante->id;
// 	$padre->save();

// 	$acudiente = new Acudiente();
// 	$acudiente->id_estudiante = $estudiante->id;
// 	$acudiente->save();

// 	$codeudor = new Codeudor();
// 	$codeudor->id_estudiante = $estudiante->id;
// 	$codeudor->save();

// 	$historia = new HistorialEstudiantil();
// 	$historia->id_estudiante = $estudiante->id;
// 	$historia->save();

// 	$clinica = new HistorialClinica();
// 	$clinica->id_estudiante = $estudiante->id;
// 	$clinica->save();

// 	$familia = new GrupoFamiliar();
// 	$familia->id_estudiante = $estudiante->id;
// 	$familia->save();

// 	return $estudiante;
// }