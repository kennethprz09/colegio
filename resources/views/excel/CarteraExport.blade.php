<table>
    <thead>
    <tr>
        <th>#</th>
        <th style="width: 10px">Servicio</th>
        <th style="width: 10px">Valor</th>
        <th style="width: 10px">Mora</th>
        <th style="width: 10px">Descripcion</th>
        <th style="width: 10px">Codigo</th>
        <th style="width: 10px">Creado</th>
    </tr>
    </thead>
    <tbody>
    @foreach($items as $item)
        <tr>
            <td>{{ $item->id }}</td>
            <td>{{ $item->servicio }}</td>
            <td>{{ $item->valor }}</td>
            <td>{{ $item->mora }}</td>
            <td>{{ $item->descripcion }}</td>
            <td>{{ $item->acudiente_codigo }}</td>
            <td>{{ $item->created_at->format('d-m-Y') }}</td>
        </tr>
    @endforeach
    </tbody>
</table>