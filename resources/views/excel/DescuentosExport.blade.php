<table>
    <thead>
    <tr>
        <th>#</th>
        <th style="width: 20px">Servicio</th>
        <th style="width: 20px">Porcentaje</th>
        <th style="width: 20px">codigo</th>
        <th style="width: 20px">Uso</th>
        <th style="width: 20px">codigo_estudiante</th>
        <th style="width: 20px">Activo</th>
        <th style="width: 20px">Inicia</th>
        <th style="width: 20px">Termina</th>
        <th style="width: 20px">Creado</th>
    </tr>
    </thead>
    <tbody>
    @foreach($items as $item)
        <tr>
            <td>{{ $item->id }}</td>
            <td>{{ $item->servicio }}</td>
            <td>{{ $item->porcentaje }}</td>
            <td>{{ $item->codigo }}</td>
            <td>{{ $item->uso }}</td>
            <td>{{ $item->codigo_estudiante }}</td>
            <td>{{ $item->status }}</td>
            <td>{{ $item->inicia_at->format('d-m-Y') }}</td>
            <td>{{ $item->termina_at->format('d-m-Y') }}</td>
            <td>{{ $item->created_at->format('d-m-Y') }}</td>
        </tr>
    @endforeach
    </tbody>
</table>