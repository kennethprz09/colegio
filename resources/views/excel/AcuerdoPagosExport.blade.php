<table>
    <thead>
    <tr>
        <th>#</th>
        <th style="width: 20px">Cantidad cuotas</th>
        <th style="width: 20px">Deuda por cuota</th>
        <th style="width: 20px">Deuda total</th>
        <th style="width: 20px">Dia de pago</th>
        <th style="width: 20px">Codigo</th>
        <th style="width: 20px">Creado</th>
    </tr>
    </thead>
    <tbody>
    @foreach($items as $item)
        <tr>
            <td>{{ $item->id }}</td>
            <td>{{ $item->cantidad_cuotas }}</td>
            <td>{{ $item->deuda_cuotas }}</td>
            <td>{{ $item->deuda_total }}</td>
            <td>{{ $item->fecha_at->format('d') }}</td>
            <td>{{ $item->acudiente_codigo }}</td>
            <td>{{ $item->created_at->format('d-m-Y') }}</td>
        </tr>
    @endforeach
    </tbody>
</table>