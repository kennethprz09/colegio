<table>
    <thead>
    <tr>
        <th>#</th>
        <th style="width: 20px">TransaccionId</th>
        <th style="width: 20px">Uuid</th>
        <th style="width: 25px">TransaccionConvenioId</th>
        <th style="width: 20px">Codigo</th>
        <th style="width: 20px">Valor</th>
        <th style="width: 20px">Valor_subtotal</th>
        <th style="width: 20px">Descuento</th>
        <th style="width: 20px">Cupon</th>
        <th style="width: 20px">Tramite</th>
        <th style="width: 20px">Tipo</th>
        <th style="width: 20px">Forma_pago</th>
        <th style="width: 20px">Creado</th>
    </tr>
    </thead>
    <tbody>
    @foreach($items as $item)
        <tr>
            <td>{{ $item->id }}</td>
            <td>{{ $item->transaccionId }}</td>
            <td>{{ $item->uuid }}</td>
            <td>{{ $item->transaccionConvenioId }}</td>
            <td>{{ $item->acudiente_codigo }}</td>
            <td>{{ $item->valor }}</td>
            <td>{{ $item->valor_subtotal }}</td>
            <td>{{ $item->descuento }}</td>
            <td>{{ $item->cupon }}</td>
            <td>{{ $item->_tramite }}</td>
            <td>{{ $item->tipo }}</td>
            <td>{{ $item->tipo_pago }}</td>
            <td>{{ $item->created_at->format('d-m-Y') }}</td>
        </tr>
    @endforeach
    </tbody>
</table>