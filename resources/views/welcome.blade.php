<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Colegio Católico - Pasarela</title>
    <link rel="icon" type="image/png" href="/img/logo.png">


    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/compiled.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/font-awesome.css') }}" rel="stylesheet">

    <style>
        ::-webkit-scrollbar {
            width: 8px;
            height: 10px;
        }

        ::-webkit-scrollbar-track {
            background-color: white;
        }

        ::-webkit-scrollbar-thumb {
            background-color: rgba(0, 0, 0, 0.2);
        }

        ::-webkit-scrollbar-button {
            background-color: #eee;
        }

        ::-webkit-scrollbar-corner {
            background-color: black;
            height: 10px;
        }

        .contenedor {
            padding-left: 265px;
            padding-right: 20px;
            padding-top: 68px;
        }

        .navbar {
            height: 54px !important;
            background: white;
            box-shadow: 0 2px 3px rgba(175, 175, 175, 0.3) !important;
        }

        .sider {
            overflow-x: auto;
            background: #ffffff;
            height: 91%;
            /* left: -300px; */
            top: 54px;
            width: 260px;
            /* z-index: 1300; */
            position: fixed;
            box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.2);
        }

        .sider .sider-header {
            height: 56px;
            border-bottom: 1px solid #dcdcdc;
        }

        .badge,
        .btn,
        .card:not([class*=card-outline-]),
        .chip,
        .jumbotron,
        .modal-dialog.cascading-modal .modal-c-tabs .nav-tabs,
        .modal-dialog.modal-notify .modal-header,
        .navbar,
        .pagination .active .page-link,
        .z-depth-1 {
            box-shadow: none;
        }

        .sider ul {
            list-style-type: none;
            margin: 0;
            padding: 0;
        }

        table th {
            font-weight: 500 !important;
        }

        .sider ul a {
            margin: 0;
            padding: 15px 0 15px 22px;
            color: #737373;
            display: block;
            position: relative;
            text-decoration: none;
        }

        .sider ul a:hover {
            border-right: 4px solid #e7e7e7;
            transition: 0.2s;
        }

        .sider ul li:hover {
            background: rgba(0, 0, 0, 0.04);
        }

        .sider ul a .fa,
        .fal,
        .far,
        .fas {
            font-size: 19px;
            width: 25px;
        }

        .sider ul a span {
            font-weight: 500;
            font-size: 13px;
            margin-left: 10px;
            color: #333;
        }

        .centro.px {
            box-shadow: 0 1px 20px 0 rgba(69, 90, 100, 0.08);
            background: white;
            padding: 20px;
            min-height: 574px;
        }

        .centro {
            box-shadow: 0px 0px 10px 0px rgba(185, 185, 185, 0.22);
            background: white;
            padding: 20px;
        }

        .dropdown-toggle::after {
            display: none;

        }

        .carta {
            box-shadow: 0px 0px 13px 1px rgb(222, 222, 222);
        }

        .lds-roller {
            display: inline-block;
            position: absolute;
            width: 64px;
            height: 64px;
            z-index: 3333;
            left: 0;
            right: 0;
            margin: auto;
            top: 0;
            bottom: 0;
        }

        .lds-roller div {
            animation: lds-roller 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
            transform-origin: 32px 32px;
        }

        .lds-roller div:after {
            content: " ";
            display: block;
            position: absolute;
            width: 6px;
            height: 6px;
            border-radius: 50%;
            background: #4d70ff;
            margin: -3px 0 0 -3px;
        }

        .lds-roller div:nth-child(1) {
            animation-delay: -0.036s;
        }

        .lds-roller div:nth-child(1):after {
            top: 50px;
            left: 50px;
        }

        .lds-roller div:nth-child(2) {
            animation-delay: -0.072s;
        }

        .lds-roller div:nth-child(2):after {
            top: 54px;
            left: 45px;
        }

        .lds-roller div:nth-child(3) {
            animation-delay: -0.108s;
        }

        .lds-roller div:nth-child(3):after {
            top: 57px;
            left: 39px;
        }

        .lds-roller div:nth-child(4) {
            animation-delay: -0.144s;
        }

        .lds-roller div:nth-child(4):after {
            top: 58px;
            left: 32px;
        }

        .lds-roller div:nth-child(5) {
            animation-delay: -0.18s;
        }

        .lds-roller div:nth-child(5):after {
            top: 57px;
            left: 25px;
        }

        .lds-roller div:nth-child(6) {
            animation-delay: -0.216s;
        }

        .lds-roller div:nth-child(6):after {
            top: 54px;
            left: 19px;
        }

        .lds-roller div:nth-child(7) {
            animation-delay: -0.252s;
        }

        .lds-roller div:nth-child(7):after {
            top: 50px;
            left: 14px;
        }

        .lds-roller div:nth-child(8) {
            animation-delay: -0.288s;
        }

        .lds-roller div:nth-child(8):after {
            top: 45px;
            left: 10px;
        }

        @keyframes lds-roller {
            0% {
                transform: rotate(0deg);
            }

            100% {
                transform: rotate(360deg);
            }
        }

        .swal-icon--warning__body,
        .swal-icon--warning__dot {
            position: absolute;
            left: 50%;
            background-color: #e64942;
        }

        .swal-icon--warning__body,
        .swal-icon--warning__dot {
            position: absolute;
            left: 50%;
            background-color: #e64942;
        }

        .swal-title {
            font-size: 15px !important;
        }

        .swal-text {
            font-size: 13px !important;
        }

        .swal-icon--warning {
            border-color: #fff !important;
            -webkit-animation: pulseWarning .75s infinite alternate;
            animation: pulseWarning .75s infinite alternate
        }

        .swal-icon--warning__body {
            width: 5px;
            height: 47px;
            top: 10px;
            border-radius: 2px;
            margin-left: -2px
        }

        .swal-icon--warning__body,
        .swal-icon--warning__dot {
            position: absolute;
            left: 50%;
            background-color: #e64942
        }

        .swal-icon--warning__dot {
            width: 7px;
            height: 7px;
            border-radius: 50%;
            margin-left: -4px;
            bottom: -11px
        }

        @-webkit-keyframes pulseWarning {
            0% {
                border-color: #f8d486
            }

            to {
                border-color: #f8bb86
            }
        }

        @keyframes pulseWarning {
            0% {
                border-color: #fff
            }

            to {
                border-color: #e64942
            }
        }

        .swal-button {
            color: #fff;
            border: none;
            box-shadow: none;
            border-radius: 5px;
            font-weight: 600;
            font-size: 12px;
            padding: 10px 24px;
            margin: 0;
            cursor: pointer;
        }

        .swal-footer {
            text-align: center !important;
        }

        .swal-text:first-child {
            margin-top: 45px;
            font-weight: bold;
            font-size: 18px !important;
        }

        .swal-button--cancel {
            color: rgb(255, 255, 255) !important;
            background-color: #828282 !important;
        }

        /*clases del api style*/
        .parrafo {
            font-size: 14px;
            font-weight: 400;
        }

        .pequeño-title {
            font-weight: bold;
        }

        .espacio {
            padding-top: 17px;
        }

        .doble-espacio {
            padding-top: 27px;
        }

        .centro a {
            color: #5984e8;
            font-weight: 500;
            text-decoration: none;
        }

        .centro a:hover {
            transition: 0.2s;
            color: #3b455d;
        }

        .col-6.code {
            padding-top: 63px;
            padding-left: 43px;
            padding-right: 26px;
        }

        .code .card-header {
            background: #3c4257;
            color: white;
            font-weight: bold;
            border-radius: 10px 10px 0px 0px;
        }

        .response .card-header {
            background: #f5f5f5;
            color: #333;
            font-weight: bold;
            border-radius: 10px 10px 0px 0px;
        }

        .card {
            background: #f7f7f7;
            border-radius: 12px;
        }

        .card-body {
            background: #4f566b;
            padding: 5px 0px 0px;
        }

        .numbers {
            cursor: context-menu;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        pre.line-numbers {
            position: relative;
        }

        pre.line-numbers code,
        pre.line-numbers samp {
            /*margin-left: 3em;*/
            /*border-left: 1px solid #000;*/
        }

        pre.line-numbers>div {
            display: block;
            position: absolute;
            top: 0;
            left: 0;
            height: 100%;
            border-radius: 0 0 0 3px;
            overflow: hidden;
            counter-reset: line;
        }

        pre.line-numbers>div>span {
            display: block;
            width: 2.5em;
            padding: 0 0.5em 0 0;
            text-align: right;
            color: #eee;
            overflow: hidden;
            counter-increment: line;
        }

        pre.line-numbers>div>span::before {
            content: counter(line);
        }

        pre.line-numbers>div>span:first-child {
            margin-top: 10px;
        }

        @media print {
            pre code {
                overflow-x: visible;
                white-space: pre-wrap;
            }

            pre.line-numbers div {
                display: none;
            }

            pre.line-numbers>code,
            pre.line-numbers>samp {
                margin-left: 0;
            }
        }

        pre.line-numbers>div>span:first-child {
            margin-top: 0px !important;
            color: #eee;
        }

        *:focus {
            outline: none;
        }

        .response pre.line-numbers>div>span:first-child {
            margin-top: 0px !important;
            color: #FF5722;
        }

        .response pre.line-numbers>div>span {
            display: block;
            width: 2.5em;
            padding: 0 0.5em 0 0;
            text-align: right;
            color: #FF5722;
            overflow: hidden;
            counter-increment: line;
        }

        .response .hljs-attr {
            color: #3a5858;
            padding-left: 24px;
        }

        .response .hljs,
        .hljs-subst {
            color: #616161;
        }

        .response .hljs-string {
            color: #3571ff;
        }

        .linea {
            width: 100%;
            height: 1px;
            background: #e4e4e4;
            margin-top: 48px;
            margin-bottom: 48px;
        }

        .linea-pequeña {
            width: 100%;
            height: 1px;
            background: #e4e4e4;
            margin-top: 15px;
            margin-bottom: 15px;
        }

    </style>
</head>

<body style="font-family: 'Quicksand', sans-serif;">
    <div id="app">
    </div>
    @include('componentes-panel.loader')
    <footer class="blue font-small page-footer position-fixed w-100">
        <div class="footer-copyright text-center py-1">
            <a href="https://www.doover.es" target="_blank">Desarrollado por ®Doover Network 2021</a>
        </div>
    </footer>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="/LivIconsEvo/js/LivIconsEvo.tools.js"></script>
    <script src="/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
    <script src="/LivIconsEvo/js/LivIconsEvo.min.js"></script>
    <script src="/pasarela/hmac-sha256.js"></script>
    <script src="/pasarela/enc-base64-min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"
        integrity="sha512-qTXRIMyZIFb8iQcfjXWCO8+M5Tbc38Qi5WzdPOYZHIlZpzBHG3L3by84BBBOiRGiEb7KKtAOAs5qYdUiZiQNNQ=="
        crossorigin="anonymous"></script>
    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/locale/es.min.js" integrity="sha512-L6Trpj0Q/FiqDMOD0FQ0dCzE0qYT2TFpxkIpXRSWlyPvaLNkGEMRuXoz6MC5PrtcbXtgDLAAI4VFtPvfYZXEtg==" crossorigin="anonymous"></script> --}}

    {{-- <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script> --}}
    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.11/lodash.js"></script> --}}
    {{-- <script src="{{asset('js/axios.js')}}"></script> --}}
    {{-- <script src="{{asset('js/toastr.js')}}"></script> --}}
    {{-- <script src="{{asset('js/jquery-3.2.1.min.js')}}"></script> --}}
    {{-- <script src="{{asset('js/mdb.js')}}"></script> --}}
    <script>
        // moment.js locale configuration
        // locale : spanish (es)
        // author : Julio Napurí : https://github.com/julionc
        (function(factory) {
            factory(moment);
        }(function(moment) {
            var monthsShortDot =
                'Enero_Febrero_Marzo_Abril_Mayo_Junio_Julio_Agosto_Septiembre_Octubre_Noviembre_Diciembre'.split(
                    '_'),
                monthsShort =
                'Enero_Febrero_Marzo_Abril_Mayo_Junio_Julio_Agosto_Septiembre_Octubre_Noviembre_Diciembre'.split(
                    '_');
            return moment.defineLocale('es', {
                months: 'Enero_Febrero_Marzo_Abril_Mayo_Junio_Julio_Agosto_Septiembre_Octubre_Noviembre_Diciembre'
                    .split('_'),
                monthsShort: function(m, format) {
                    if (/-MMM-/.test(format)) {
                        return monthsShort[m.month()];
                    } else {
                        return monthsShortDot[m.month()];
                    }
                },
                weekdays: 'domingo_lunes_martes_miércoles_jueves_viernes_sábado'.split('_'),
                weekdaysShort: 'dom._lun._mar._mié._jue._vie._sáb.'.split('_'),
                weekdaysMin: 'Do_Lu_Ma_Mi_Ju_Vi_Sá'.split('_'),
                longDateFormat: {
                    LT: 'H:mm',
                    LTS: 'LT:ss',
                    L: 'DD/MM/YYYY',
                    LL: 'D [de] MMMM [de] YYYY',
                    LLL: 'D [de] MMMM [de] YYYY LT',
                    LLLL: 'dddd, D [de] MMMM [de] YYYY LT'
                },
                calendar: {
                    sameDay: function() {
                        return '[hoy a la' + ((this.hours() !== 1) ? 's' : '') + '] LT';
                    },
                    nextDay: function() {
                        return '[mañana a la' + ((this.hours() !== 1) ? 's' : '') + '] LT';
                    },
                    nextWeek: function() {
                        return 'dddd [a la' + ((this.hours() !== 1) ? 's' : '') + '] LT';
                    },
                    lastDay: function() {
                        return '[ayer a la' + ((this.hours() !== 1) ? 's' : '') + '] LT';
                    },
                    lastWeek: function() {
                        return '[el] dddd [pasado a la' + ((this.hours() !== 1) ? 's' : '') + '] LT';
                    },
                    sameElse: 'L'
                },
                relativeTime: {
                    future: 'en %s',
                    past: 'hace %s',
                    s: 'unos segundos',
                    m: 'un minuto',
                    mm: '%d minutos',
                    h: 'una hora',
                    hh: '%d horas',
                    d: 'un día',
                    dd: '%d días',
                    M: 'un mes',
                    MM: '%d meses',
                    y: 'un año',
                    yy: '%d años'
                },
                ordinalParse: /\d{1,2}º/,
                ordinal: '%dº',
                week: {
                    dow: 1, // Monday is the first day of the week.
                    doy: 4 // The week that contains Jan 4th is the first week of the year.
                }
            });
        }));

        $(window).on("load", function() {
            $(".loading").fadeOut("slow");
            $(".sk-cube-grid").fadeOut("slow");
            if ($(window).width() < 992) {
                $('#sider_app').fadeOut("slow");
                $("#contenedor_app").css("padding-left", "0px");
                $("#contenedor_app").css("padding-right", "0px");
            }
            if ($(window).width() > 992) {
                $('#sider_app').fadeIn("slow");
                $("#contenedor_app").css("padding-left", "265px");
                $("#contenedor_app").css("padding-right", "20px");
            }
        });

        $(document).ready(function() {
            $(window).resize(function() {
                if ($(window).width() < 992) {
                    $('#sider_app').fadeOut("slow");
                    $("#contenedor_app").css("padding-left", "0px");
                    $("#contenedor_app").css("padding-right", "0px");
                }
                if ($(window).width() > 992) {
                    $('#sider_app').fadeIn("slow");
                    $("#contenedor_app").css("padding-left", "265px");
                    $("#contenedor_app").css("padding-right", "20px");
                }
            });
        });
    </script>
</body>

</html>