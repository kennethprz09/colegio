<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <title>Colegio Católico</title>
</head>
<body>

  <style>
    span {
      font-size: 19px !important;
    }
  </style>


  <div class="contaier" style="font-family:Open Sans,Sans-Serif;padding:10px;font-size:16px;line-height:24px;color:#3d3d3d">
    <h5 style="font-weight: 200;">
      Apreciado:
      <br>
      Padre de Familia.
      <br>
      Código de estudiante: {{$user->codigo}}
      <br>
      Saludo de Paz, Amor y Fraternidad.
      <br>
      <br>
      
      Le recordamos que el no pago a tiempo de sus obligaciones financieras, puede generar un reporte negativo en las centrales de riesgos. Recomendamos realizar su pago los cinco (05) primeros días de cada mes, evite futuros inconvenientes.
      <br>
      <br>
      
      Si usted se encuentra en deuda con sus obligaciones, le invitamos a realizar la cancelación de las mismas a través de los siguientes medios de pago:
      <br>
      <br>
      
      * Pago en Línea a través de nuestra pasarela de pago
      <br>
      (https://pasareladepagos.colegiocatolicocali.edu.co/)
      <br>
      * Directamente en el colegio con tarjeta débito o crédito.
      <br>
      *Consignación en Banco Av. Villas.

      <br>
      <br>
      Si está al día, haga caso omiso a esta comunicación. 
      
      <br>
      <br>
      Si su obligación ya se encuentra reportada en Covinoc, recuerde cancelarla directamente en la entidad.
      
      <br>
      <br>
      Cordialmente,
      <br>
      Departamento Administrativo      
    </h3>
  </div>
</body>
</html>