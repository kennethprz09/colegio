const mutations = {
	retrieveToken(state, token) {
		state.token = token
	},
	destroyToken(state) {
		state.token = null
	},
	updateSaldo(state, res) {
		state.ActiveUser.saldo = res.saldo;
		state.ActiveUser.deuda = res.deuda;
	}
}

export default mutations