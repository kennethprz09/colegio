const userDefaults = {
	id: 0,
	name: null,
	codigo: null,
	email: null,
	saldo: 0,
	deuda: 0,
	nivelacion: null,
	pension: null,
	grado: localStorage.getItem('grado') || null,
	roll: localStorage.getItem('roll') || null
}

const state = {
	ActiveUser: userDefaults,
	token: localStorage.getItem('access_token') || null
}

export default state