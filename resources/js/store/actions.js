const actions = {
	retrieveToken(context, credentials) {
	  return new Promise((resolve, reject) => {
		axios.post('/api/login', {
		  username: credentials.username,
		  password: credentials.password,
		})
		  .then(response => {
			// console.log(response)
			const token = response.data.access_token
			axios.defaults.headers.common['Authorization'] = 'Bearer ' + token
			localStorage.setItem('access_token', token)
			context.commit('retrieveToken', token)

			resolve(response)
		  })
		  .catch(error => {
			//console.log(error)
			reject(error)
		  })
	  })
	},
	destroyToken(context) {
	  if (context.getters.loggedIn){
		return new Promise((resolve, reject) => {
		  axios.post('/api/logout')
			.then(response => {
			  localStorage.removeItem('access_token')
			  context.commit('destroyToken')
			  resolve(response)
			})
			.catch(error => {
			  //console.log(error)
			  localStorage.removeItem('access_token')
			  localStorage.removeItem('roll')
			  context.commit('destroyToken')

			  reject(error)
			})
		})
	  }
	},
	updateSaldo(context) {
		if (context.getters.loggedIn){
			return new Promise((resolve, reject) => {
				axios.get('/api/user')
				.then(response => {
					context.commit('updateSaldo', response.data)
					resolve()
				})
			})
		}
	}
}

export default actions