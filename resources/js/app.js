window.$ = window.jQuery = require('jquery');
import Vue from 'vue'
import App from './App.vue'

import VueToastr2 from 'vue-toastr-2'
import 'vue-toastr-2/dist/vue-toastr-2.min.css'

window.toastr = require('toastr')
Vue.use(VueToastr2)

import swal from 'sweetalert';

// axios
import axios from "./axios.js"
Vue.prototype.$http = axios

// Vue Router
import router from './router'

// Vuex Store
import store from './store/store'

// VeeValidate
import VeeValidate from 'vee-validate'
Vue.use(VeeValidate);

// Croppa
import Croppa from 'vue-croppa';
Vue.use(Croppa);
import 'vue-croppa/dist/vue-croppa.css';

Vue.config.productionTip = false

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app')
