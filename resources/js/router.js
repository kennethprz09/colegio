import Vue from 'vue'
import Router from 'vue-router'
import store from './store/store'

Vue.use(Router)

const router = new Router({
	mode: 'history',
	base: '/',
	scrollBehavior () {return { x: 0, y: 0 }
	},
	routes: [
		{ 
			path: '/admin',
			name: 'admin',
			component: () => import('@/layouts/panel/index.vue'),
			children: [
				{
					path: 'dashboard',
					name: 'Dashboard',
					component: () => import('@/views/admin/dashboard.vue'),
					meta: {
						roll: 'admin'
					}
				},
				{
					path: 'matriculas',
					name: 'Matriculas',
					component: () => import('@/views/admin/matriculas/index.vue'),
					meta: {
						roll: 'admin'
					}
				},
				{
					path: 'extracurriculares',
					name: 'Extracurriculares',
					component: () => import('@/views/admin/extracurriculares/index.vue'),
					meta: {
						roll: 'admin'
					}
				},
				{
					path: 'pension',
					name: 'Pension',
					component: () => import('@/views/admin/pension/index.vue'),
					meta: {
						roll: 'admin'
					}
				},
				{
					path: 'cursos',
					name: 'Cursos',
					component: () => import('@/views/admin/cursos/index.vue'),
					meta: {
						roll: 'admin'
					}
				},
				{
					path: 'medias_tecnicas',
					name: 'Medias tecnicas',
					component: () => import('@/views/admin/medias_tecnicas/index.vue'),
					meta: {
						roll: 'admin'
					}
				},
				{
					path: 'certificados',
					name: 'Certificados',
					component: () => import('@/views/admin/certificados/index.vue'),
					meta: {
						roll: 'admin'
					}
				},
				{
					path: 'transporte',
					name: 'Transporte',
					component: () => import('@/views/admin/transporte/index.vue'),
					meta: {
						roll: 'admin'
					}
				},
				{
					path: 'cafeteria',
					name: 'Cafeteria',
					component: () => import('@/views/admin/cafeteria/index.vue'),
					meta: {
						roll: 'admin'
					}
				},
				{
					path: 'informe_pagos',
					name: 'Informe de pagos',
					component: () => import('@/views/admin/informe_pagos/index.vue'),
					meta: {
						roll: 'admin'
					}
				},
				{
					path: 'informe_pagos_publicos',
					name: 'Pagos publicos',
					component: () => import('@/views/admin/pagos_publicos/index.vue'),
					meta: {
						roll: 'admin'
					}
				},
				{
					path: 'historico_cartera',
					name: 'Historico de cartera',
					component: () => import('@/views/admin/historico_cartera/index.vue'),
					meta: {
						roll: 'admin'
					}
				},
				{
					path: 'historico_cartera/detalles-:acuerdoId',
					props: true,
					name: 'Detalle de cartera',
					component: () => import('@/views/admin/historico_cartera/detalles.vue'),
					meta: {
						roll: 'admin'
					}
				},
				{
					path: 'acuerdo_pagos',
					name: 'Acuerdo de pagos',
					component: () => import('@/views/admin/acuerdo_pagos/index.vue'),
					meta: {
						roll: 'admin'
					}
				},
				{
					path: 'acuerdo_pagos/detalles-:acuerdoId',
					props: true,
					name: 'Detalle de acuerdo de pago',
					component: () => import('@/views/admin/acuerdo_pagos/detalles.vue'),
					meta: {
						roll: 'admin'
					}
				},
				{
					path: 'certificados_solicitados',
					name: 'Certificados solicitados',
					component: () => import('@/views/admin/certificados_solicitados/index.vue'),
					meta: {
						roll: 'admin'
					}
				},
				{
					path: 'certificados_publicos',
					name: 'Certificados publicos',
					component: () => import('@/views/admin/certificados_publicos/index.vue'),
					meta: {
						roll: 'admin'
					}
				},
				{
					path: 'solicitudes_ingreso',
					name: 'Solicitudes de ingreso',
					component: () => import('@/views/admin/solicitudes_ingreso/index.vue'),
					meta: {
						roll: 'admin'
					}
				},
				{
					path: 'pago_matriculas',
					name: 'Pago matriculas',
					component: () => import('@/views/admin/pago_matriculas/index.vue'),
					meta: {
						roll: 'admin'
					}
				},
				{
					path: 'pago_matriculas/detalles-:matriculaId',
					props: true,
					name: 'Detalle de matricula',
					component: () => import('@/views/admin/pago_matriculas/detalles.vue'),
					meta: {
						roll: 'admin'
					}
				},
				{
					path: 'estudiantes_nivelacion',
					name: 'Estudiantes por nivelar',
					component: () => import('@/views/admin/estudiantes_compromiso/index.vue'),
					meta: {
						roll: 'admin'
					}
				},
				{
					path: 'pagos_caja',
					name: 'Pagos en caja',
					component: () => import('@/views/admin/pagos_caja/index.vue'),
					meta: {
						roll: 'admin'
					}
				},
				{
					path: 'otros_pagos',
					name: 'Otros Pagos',
					component: () => import('@/views/admin/otros_pagos/index.vue'),
					meta: {
						roll: 'admin'
					}
				},
				{
					path: 'descuentos',
					name: 'Descuentos',
					component: () => import('@/views/admin/descuentos/index.vue'),
					meta: {
						roll: 'admin'
					}
				},
				{
					path: 'docentes',
					name: 'Docentes',
					component: () => import('@/views/admin/docentes/index.vue'),
					meta: {
						roll: 'admin'
					}
				},
				{
					path: 'usuarios',
					name: 'Usuarios',
					component: () => import('@/views/admin/usuarios/index.vue'),
					meta: {
						roll: 'admin'
					}
				},
			],
			meta: {
				requiresAuth: true,
			}
		},
		{ 
			path: '/acudiente',
			name: 'acudiente',
			component: () => import('@/layouts/acudiente/index.vue'),
			children: [
				{
					path: 'perfil',
					name: 'Perfil',
					component: () => import('@/views/acudiente/profile.vue'),
					meta: {
						roll: 'acudiente'
					}
				},
				{
					path: 'inicio',
					name: 'Inicio',
					component: () => import('@/views/acudiente/dashboard.vue'),
					meta: {
						roll: 'acudiente'
					}
				},
				{
					path: 'extracurriculares',
					name: 'Suscribcion de Extracurriculares',
					component: () => import('@/views/acudiente/extracurriculares/index.vue'),
					meta: {
						roll: 'acudiente'
					}
				},
				{
					path: 'extracurriculares_activas',
					name: 'Mis extracurriculares',
					component: () => import('@/views/acudiente/extracurriculares/propios_index.vue'),
					meta: {
						roll: 'acudiente'
					}
				},
				{
					path: 'medias_tecnicas',
					name: 'Suscribcion de Medias tecnicas',
					component: () => import('@/views/acudiente/medias_tecnicas/index.vue'),
					meta: {
						roll: 'acudiente'
					}
				},
				{
					path: 'medias_tecnicas_activas',
					name: 'Mis Medias tecnicas',
					component: () => import('@/views/acudiente/medias_tecnicas/propios_index.vue'),
					meta: {
						roll: 'acudiente'
					}
				},
				{
					path: 'matricula',
					name: 'Matricula',
					component: () => import('@/views/acudiente/matricula/index.vue'),
					meta: {
						roll: 'acudiente'
					}
				},
				{
					path: 'pension',
					name: 'Pago de Pension',
					component: () => import('@/views/acudiente/pension/index.vue'),
					meta: {
						roll: 'acudiente'
					}
				},
				{
					path: 'pagos-pendientes',
					name: 'Pagos pendientes',
					component: () => import('@/views/acudiente/pagos_pendientes/index.vue'),
					meta: {
						roll: 'acudiente'
					}
				},
				{
					path: 'facturas-pagos',
					name: 'Factura de pagos',
					component: () => import('@/views/acudiente/factura_pagos/index.vue'),
					meta: {
						roll: 'acudiente'
					}
				},
				{
					path: 'documentos',
					name: 'Documentos',
					component: () => import('@/views/acudiente/documentos/index.vue'),
					meta: {
						roll: 'acudiente'
					}
				},
				{
					path: 'acuerdo_pago',
					name: 'Acuerdo de pago',
					component: () => import('@/views/acudiente/acuerdo_pago/index.vue'),
					meta: {
						roll: 'acudiente'
					}
				}
			],
			meta: {
				requiresAuth: true,
			}
		},
		{ 
			path: '',
			component: () => import('@/layouts/full/index.vue'),
			children: [
				{
					path: '/',
					name: 'login',
					component: () => import('@/views/pages/Login.vue')
				},
				{
					path: '/error-404',
					name: 'error-404',
					component: () => import('@/views/pages/Error404.vue')
				},
				{
					path: '/error-permisos',
					name: 'permisos',
					component: () => import('@/views/pages/Permisos.vue')
				},
				{
					path: '/respuestaCpv',
					name: 'respuestaCpv',
					props: true,
					component: () => import('@/views/pages/respuestaCpv.vue')
				},
			]
		},
		{
			path: '/gracias-:codigo',
			props: true,
			name: 'Gracias',
			component: () => import('@/views/pages/Gracias.vue'),
		},
		{
			path: '/solicitud-:codigounico',
			props: true,
			name: 'Solicitud',
			component: () => import('@/views/pages/StatusSolicitud.vue'),
		},
		// Redirect to 404 page, if no match found
		{
			path: '*',
			redirect: '/error-404'
		},
	],
})

router.afterEach(() => {
	const appLoading = document.getElementById('loading-bg')
	appLoading.style.display = "none";
})

router.beforeEach((to, from, next) => {
	const appLoading = document.getElementById('loading-bg')
	appLoading.style.display = "block";
	console.log(to.name)

	if (to.matched.some(record => record.meta.requiresAuth)) {
		if (!store.getters.loggedIn) {
			// next({ name: 'login' })
			next()
		} else {
			if (store.state.ActiveUser.roll == to.meta.roll) {
				next()
			} else {
				
				// next({ name: 'permisos' })
				next()
			}
		}
	} else {
		next() // make sure to always call next()!
	}
})

export default router